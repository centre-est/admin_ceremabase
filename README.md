# admin_ceremabase

## Racine

Scripts Centre-Est d'utilisation de CeremaBase
- vademecum_ceremabase.sql : aide mémoire pour les fonctions de base des usagers de la base
- vademecum_automatisation.sql : aide mémoire pour les scripts ou les fonctions d'automatisation

## Répertoire fonction_ald
Fonctions propres à l'administrateur de la base :
- a_adl_mensuel() : active les fonctions mensuelles (set_droits_public_referentiels / create_metadonnees)
- create_metadonnees() : Ajoute des vues de métadonnées de tables et d'attributs dans le schéma public et tous les schéma présents : 
- ff_maj_enforce_dims_postgis3.sql : Traitement du remplacement de CHECK ((ndims(geomloc) = 2)) par CHECK ((st_ndims(geomloc) = 2)) dans les fichiers fonciers Cerema avant de les migrer vers postgis 3.1
- ff_maj_enforce_srid_postgis3.sql : Traitement du remplacement de CHECK (public.srid(geomloc) = 2154)) par CHECK (ST_srid(geomloc) = 2154) dans les fichiers fonciers Cerema avant de les migrer vers postgis 3.1
- set_auto_tablespace_index : Affection systématique et automatique de tous les index dans CeremaBase au tablespace "index".
- set_droits_public_referentiels() : Affecte les droits par défaut à toutes les tables des schémas référentiels.
- set_tablespace_data(schema text) : Déplace tous les index présents dans un schéma en paramètre vers le tablespace data.

## Répertoire fonction_ald_delegue
Fonctions propres au administrateurs délégués de la base :
- set_tablespace_index(schema text) : Déplace tous les index présents dans un schéma en paramètre vers le tablespace index

## Répertoire fonctions
Fonctions partagées
- cree_standard_covadis_te.sql : Crée les tables conformes au standard COVADIS V1 (http://www.geoinformations.developpement-durable.gouv.fr/geostandard-transports-exceptionnels-v1-0-a3493.html) dans un schéma passé en paramètre
- creer_grille.sql : crée une grille qui contient l'ensemble de la couche en paramètre, selon un pas en paramètre aussi
- synthese_geometrie_schema : Extrait l'ensemble des informations géométriques de tous les champs géométriques présents dans un schéma passé en paramètre

## Répertoire fonctions
Fonctions partagées
- creer_grille.sql : crée une grille qui contient l'ensemble de la couche en paramètre, selon un pas en paramètre aussi

## Répertoire.ign

Ensemble de scripts SQL pour administrer les référentiels sous PostgreSQL/Postgis selon les règles de nommage prescrites par la COVADIS (http://www.geoinformations.developpement-durable.gouv.fr/covadis-r425.html)
- vademecum_ald.sql : aide mémoire pour l'administration de la base.

D'une manière générale :
- set_admin_xxxxxxxxxx : administration du référentiel qui apparait dans le nom ainsi que sa version,
- set_comment_xxxxxxxxxx : mets les commentaires sur tables et les attributs de toutes les tables d'un référentiel IGN.

Les paramètres le plus souvent :
- nom du schéma où sont placées les tables,
- emprise selon les règles de la COVADIS : 000 pour France Métro / rrr pour les Régions, ddd pour les départements,
- millesime : AAAA

Pour avoir plus de détail sur les paramètres et les taches réalisées, tout est décrit dans la première partie du corps du code. Par exemple pour ADMIN EXPRESS® de l'IGN :

[ADMIN - ADMIN_EXPRESS] - Mise en place des taches d'administration pour un millesime d'ADMIN EXPRESS® de l'IGN selon le millesime et l'emprise :

Taches réalisées :
A - Re-nommage des tables
B. Optimisation de base sur l'ensemble des fichiers
B.1 Vérification du nom du champs géométrie
B.2 Suppression des champs inutiles
B.3 Correction des erreurs sur la géométrie
B.4 Contraintes géométriques de la table
B.5 Ajout des index spatiaux et cluster
B.6 Ajout des index attributaires non existants
B.7 clés primaires sur le champs id
C. Travail à la Table
C.1 n_adm_exp_arrondissement_dpt_ddd_aaaa
C.2 n_adm_exp_chef_lieu_ddd_aaaa
C.3 n_adm_exp_commune_ddd_aaaa
C.4 n_adm_exp_departement_ddd_aaaa
C.5 n_adm_exp_epci_ddd_aaaa
C.6 n_adm_exp_region_ddd_aaaa

Tables concernées :

amélioration à faire : option nommage COG en paramètre
