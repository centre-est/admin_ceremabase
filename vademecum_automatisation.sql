----=====================================================================================
---- 	VADE-MECUM SUR LES SCRIPTS TYPES
----
----             CEREMABASE
----
---- 	Version 1 du 01/05/2020
----	c.badol
----	Finalisé oui| | / non |X|
----=====================================================================================
---- Rq : ---- pour les commentaires / -- pour les commandes optionnelles, debuger


----------------------------------------------------
---- O - Format type d'une fonction Centre-Est  ----
----------------------------------------------------
CREATE OR REPLACE FUNCTION monschema.mafonction(parametre1 text,parametre2 integer, parametre3 varchar(10) DEFAULT 'pardefaut')
 RETURNS void
 LANGUAGE plpgsql
AS $function$
 /*
[ADMIN] - Déplace tous les index présents dans un schéma en paramètre vers le tablespace index

Option :
--------
- nom du schéma

Tables concernées :
-------------------
- toutes les tables du schéma

Taches réalisées :
------------------
- Scrute toutes les tables du schéma indiqué en paramètre et leurs indexs dans une liste
- Pour tous les index de la liste leur affecte par défaut le tablespace nommé "index"
	- si dèjà le cas, passe au suivant sans perte de temps,
	- sinon le fait.

A améliorer :
-------------
vérifier l´existance du schéma

dernière MAJ :
--------------
30 avril 2020
*/
DECLARE
  object text;		---- liste des tables présentes dans le schéma
  req text;			---- requête à passer
BEGIN


END;
$function$
;


---------------------------------------------------------------------------
---- I - Une commande SQL pour indexer tous les attributs d'une table  ----
---------------------------------------------------------------------------
;DO $$
DECLARE
nomdelatable				character varying;
nomduschema					character varying;
liste_valeur 				character varying[];
attribut 					character varying;
req 						text;
BEGIN
nomdelatable := 'n_parcelle_etalab_000_2020';
nomduschema := 'r_cadastre_etalab_2020';
	FOR attribut IN
			SELECT COLUMN_NAME
				FROM INFORMATION_SCHEMA.COLUMNS
				WHERE TABLE_NAME = nomdelatable
				AND COLUMN_NAME != 'geom' AND COLUMN_NAME != 'the_geom'
		LOOP
				req := '
					DROP INDEX IF EXISTS ' || nomduschema || '.' || nomdelatable || '_' || attribut || '_idx;
					CREATE INDEX ' || nomdelatable || '_' || attribut || '_idx ON ' || nomduschema || '.' || nomdelatable || '
						USING BRIN (' || attribut || ') TABLESPACE index;
				';
				RAISE NOTICE '%', req;
				EXECUTE(req);
				COMMIT;
		END LOOP;
req := '
DROP INDEX IF EXISTS ' || nomduschema || '.' || nomdelatable || '_geom;
CREATE INDEX ' || nomdelatable || '_geom ON  '|| nomduschema || '.' || nomdelatable || '
	USING gist (geom) TABLESPACE index;
';
RAISE NOTICE '%', req;
EXECUTE(req);
COMMIT;
END $$;



----------------------------------------------------------
---- II - Une commande SQL sur tous les départements  ----
----------------------------------------------------------
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
req 						text;
BEGIN
liste_valeur := ARRAY  ['000',
						'r11','r24','r27','r28','r32','r44','r52','r53','r75','r76','r84','r93','r94',
						'001','002','003','004','005','006','007','008','009','010',
						'011','012','013','014','015','016','017','018','019','02a','02b',
						'021','022','023','024','025','026','027','028','029','030',
						'031','032','033','034','035','036','037','038','039','040',
						'041','042','043','044','045','046','047','048','049','050',
						'051','052','053','054','055','056','057','058','059','060',
						'061','062','063','064','065','066','067','068','069','070',
						'071','072','073','074','075','076','077','078','079','080',
						'081','082','083','084','085','086','087','088','089','090',
						'091','092','093','094','095','971','972','973','974','976'
						];
/*
liste_valeur := ARRAY  ['01','02','03','04','05','06','07','08','09','10',
						'11','12','13','14','15','16','17','18','19','2a','02b',
						'21','22','23','24','25','26','27','28','29','30',
						'31','32','33','34','35','36','37','38','39','40',
						'41','42','43','44','45','46','47','48','49','50',
						'51','52','53','54','55','56','57','58','59','60',
						'61','62','63','64','65','66','67','68','69','70',
						'71','72','73','74','75','76','77','78','79','80',
						'81','82','83','84','85','86','87','88','89','90',
						'91','92','93','94','95'
						];
*/

FOR i_table IN 1..array_length(liste_valeur, 1) LOOP
		req := '
				SELECT w_adl_delegue.set_comment_cadastre_etalab(''r_cadastre_etalab_2020'','''|| liste_valeur[i_table] ||''',''2020'',true);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
END LOOP;
END $$;


-----------------------------------------------------------------------
---- III - Un ALTER sur toutes les partitions d'une liste de table ----
-----------------------------------------------------------------------
DO $$
DECLARE
liste_emprise 				CHARACTER VARYING[];
liste_table 				CHARACTER VARYING[];
millesime 					CHARACTER VARYING(4);
produit_covadis				CHARACTER VARYING;
monschema					CHARACTER VARYING;
req 						text;
BEGIN
millesime := '2020';
produit_covadis := 'pepci';
monschema := 'r_parcellaire_express_' || millesime;
liste_emprise := ARRAY  ['000','r11','r24','r27','r28','r32','r44','r52','r53','r75','r76','r84','r93','r94'];
liste_table := ARRAY  [	'batiment',
						'borne_limite_propriete',
						'borne_parcelle',
						'commune',
						'feuille',
						'localisant',
						'parcelle',
						'subdivision_fiscale'
						];

FOR i_emprise IN 1..array_length(liste_emprise, 1) LOOP
	FOR i_table IN 1..array_length(liste_table, 1) LOOP
		req := '
				ALTER TABLE '|| monschema ||'.n_'|| liste_table[i_table] ||'_'|| produit_covadis ||'_'|| liste_emprise[i_emprise] ||'
						RENAME TO n_'|| liste_table[i_table] ||'_'|| produit_covadis ||'_'|| liste_emprise[i_emprise] ||'_'|| millesime ||';
			';
		RAISE NOTICE '%', req;
		--EXECUTE(req);
		--ROLLBACK;
	END LOOP;
END LOOP;
END $$
