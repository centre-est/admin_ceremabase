----=====================================================================================
---- 	VADE-MECUM SUR LES OPERATIONS TYPES
----
----             CEREMABASE
----
---- 	Version 1.4 du 04/06/2021
----	c.badol
----	Finalisé oui|X| / non | |
----=====================================================================================
---- Rq : ---- pour les commentaires / -- pour les commandes optionnelles, debuger

--------------------------------
---- I - Import des tables  ----
--------------------------------

---- I.1 dbf en ligne de commande :
-------------------------------------------
SET PGCLIENTENCODING=UTF8
SET PGHOST=localhost
SET PGPORT=5444
SET PGDATABASE=ceremabase
SET PGUSER=postgres
SET PGPASSWORD=monmotdepasse
"C:\Program Files\PostgreSQL\9.6\bin\shp2pgsql.exe" D:\_TEMP\ADD_RPG_2018\codification_cultures.dbf r_rpg.l_codification_cultures_000_2016 | "C:\Program Files\PostgreSQL\9.6\bin\psql.exe"

---- I.2 csv en ligne de commande :
-------------------------------------------
SET PGCLIENTENCODING=UTF8
SET PGPASSWORD=monmotdepasse
"C:\OSGeo4W64\bin\ogr2ogr.exe" -gt 65536 -f PostgreSQL "PG:dbname='ceremabase' host='srv41-map' port='5444' user='ctcs'" -skipfailures -lco SCHEMA=sirene_2017 C:\Users\user1\Downloads\geo-sirene_01.csv

---- I.3 Shapefile en ligne de commande :
-------------------------------------------
SET PGCLIENTENCODING=UTF8
SET PGPASSWORD=monmotdepasse
"C:\Program Files\PostgreSQL\9.6\bin\shp2pgsql.exe" -s [EPSG]:2154 -c -g geom -t 2D -W "LATIN1" \\ct69-SIGI\ref\BDTOPO\H_ADMINISTRATIF\N_COMMUNE_BDT_001.SHP travail_user.n_commune_bdt_001 | "C:\Program Files\PostgreSQL\9.6\bin\psql.exe" -h srv41-map -p 5444 -U ctcs -d ceremabase

---- I.4 MapINFO en ligne de commande :
-------------------------------------------
SET PGCLIENTENCODING=LATIN1
SET PGPASSWORD=monmotdepasse
"C:\OSGeo4W64\bin\ogr2ogr.exe" -gt 65536 -f PostgreSQL "PG:dbname=ceremabase host=srv41-map port=5444 user=ctcs" -skipfailures -lco SCHEMA=formation -a_srs EPSG:2154 -lco GEOMETRY_NAME=geom -lco DIM=2 -nlt MULTIPOLYGON C:\Users\user1\Downloads\communes-20160119-shp\communes-20160119.TAB
--> vérifier le nom d'erreur

------ I.5 Raster en ligne de commande :
-------------------------------------------

------ I.6 SQL en ligne de commande :
-------------------------------------------
SET PGCLIENTENCODING=UTF8
SET PGHOST=localhost
SET PGPORT=5444
SET PGDATABASE=ceremabase
SET PGUSER=postgres
SET PGPASSWORD=monmotdepasse
"C:\Program Files\PostgreSQL\11\bin\psql.exe" --file C:\Users\badpap\Desktop\w_bdparcellaire_2018

-------------------------------
---- II - Vérifications    ----
-------------------------------
---- Renomage de la couche :
ALTER TABLE formation.communes_20160119 RENAME TO n_commune_osm_000_2016;
--> Query returned successfully with no result in 16 msec.

---- II.1 Vérification minimale :
-----------------------------------
---- II.1.1 - Combien d'objets importés :
SELECT
	count(geom) AS nb_objets,
	sum(ST_Area(geom)) AS surface_tot,
	avg(ST_Area(geom)) AS surface_moy,
	percentile_cont(0.5) within group ( order by ST_Area(geom) ) AS surface_mediane_m2,
	min(ST_Area(geom)) AS surface_mini,
	max(ST_Area(geom)) AS surface_maxi
FROM  r_admin_express.n_adm_exp_departement_000_2016;
----> 96;548877212207.479;5717470960.49457;5986447201.53736;105345797.21505;10374525586.8078

---- II.1.2 - Test des erreurs restantes :
SELECT *,ST_IsValidDetail(geom) FROM formation.n_commune_osm_000_2016 where ST_isValid(geom)=false; --indique l'id de l'objet, et le type d'erreur
--> Total query runtime: 01:42 minutes
--> 0 ligne récupérée.

---- II.1.3 - Correction des erreurs pour des polygones : 
UPDATE ' || nom_schema || '.' || nom_table || ' SET geom=
	CASE 
		WHEN GeometryType(geom) = 'POLYGON' 		OR GeometryType(geom) = 'MULTIPOLYGON' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
		WHEN GeometryType(geom) = 'LINESTRING' 	OR GeometryType(geom) = 'MULTILINESTRING' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
		WHEN GeometryType(geom) = 'POINT' 		OR GeometryType(geom) = 'MULTIPOINT' THEN
				ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
		ELSE ST_MakeValid(geom)
	END
WHERE NOT ST_Isvalid(geom);
----Récupéré ici : http://trac.osgeo.org/postgis/ticket/1735	---------------------------------------
--> Total query runtime: 01:42 minutes
--> 0 ligne récupérée.

---- II.2 - Vérification avancées :
------------------------------------ 
---- II.2.1 - Vérification des projections et des types d'objets :
SELECT
	ST_SRID(geom) AS "ST_SRID",
	GeometryType(geom) AS "GeometryType",
	ST_GeometryType(geom) AS "ST_GeometryType",
	ST_CoordDim(geom) AS "ST_CoordDim",
	ST_NDims(geom) AS "ST_NDims",
	count(*)
FROM
	formation.n_commune_osm_000_2016
GROUP BY "ST_SRID","GeometryType","ST_GeometryType","ST_CoordDim","ST_NDims";
--> 2154;"MULTIPOLYGON";"ST_MultiPolygon";2;2;35888

---- II.2.2 - Existe-t-il des objets anormaux :
---- Géométrie vide :
SELECT * FROM formation.n_commune_osm_000_2016 WHERE ST_IsEmpty(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne récupérée.

---- Ligne simple :
SELECT * FROM gpx.ios_gpx_traces WHERE ST_IsRing(wkb_geometry);
--> Total query runtime: 1.1 secs
--> 0 ligne récupérée.

---- Points anormaux :
SELECT * FROM formation.n_commune_osm_000_2016 WHERE NOT ST_IsSimple(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne récupérée.

---- Polygones non fermés :
SELECT * FROM formation.n_commune_osm_000_2016 WHERE NOT ST_IsClosed(geom);
--> Total query runtime: 1.1 secs
--> 0 ligne récupérée.

---- II.3 - Corrections :
------------------------------------ 
---- II.3.1 - Passer de geometry(MultiPolygonZM,2154) à 
---- II.3.1.1 - geometry(MultiPolygonZ,2154)
ALTER TABLE recette_2a.n_bdt_canalisation_eau_l_ ALTER COLUMN geom TYPE geometry(MultiPolygonZ,2154) USING ST_SetSRID(ST_Force3DZ(geom),2154);
---- II.3.1.2 - geometry(MultiPolygonM,2154)
ALTER TABLE recette_2a.n_bdt_canalisation_eau_l_ ALTER COLUMN geom TYPE geometry(MultiPolygonZ,2154) USING ST_SetSRID(ST_Force3DM(geom),2154);

--------------------------------
---- III - Optimisations    ----
--------------------------------

---- III.0 - Suppression de la séquence inutile :
---------------------------------------------------
DROP SEQUENCE formation.communes_20160119_ogc_fid_seq CASCADE;
--> NOTICE:  DROP cascade sur valeur par défaut pour table formation.communes_20160119 colonne ogc_fid
--> Query returned successfully with no result in 31 msec.

---- III.1 - Mise à la COVADIS :
--------------------------------- 
---- III.1.1 - Renomage de la couche :
ALTER TABLE formation.communes_20160119 RENAME TO n_commune_osm_000_2016;
--> Query returned successfully with no result in 16 msec.

---- III.1.2 - Affectation au bon Schéma :
ALTER TABLE public.n_commune_osm_000_2016 SET SCHEMA formation;
--> Query returned successfully with no result in 16 msec.

---- III.2 - Ajout des commentaires:
-------------------------------------
---- III.2.1 - Schéma :
COMMENT ON SCHEMA formation IS 'Schéma temporaire mis en place pour la formation';
--> Query returned successfully with no result in 16 msec.

---- III.2.2 - Table :
COMMENT ON TABLE formation.n_commune_osm_000_2016 IS 'Découpage administratif communal français au 1er janvier 2016 issu d''OpenStreetMap';
--> Query returned successfully with no result in 16 msec.

---- III.2.3 - Colonne :
COMMENT ON COLUMN formation.n_commune_osm_000_2016.insee  IS 'Nombre de batimants agrégés de la DGFiP';
--> Query returned successfully with no result in 16 msec.

---- III.3 - Optimisation avec des contraintes :
-------------------------------------------------
---- III.3.0 - Géométrie Valide
ALTER TABLE formation.n_commune_osm_000_2016 ADD CONSTRAINT enforce_validite_geom CHECK (ST_IsValid(geom))

---- III.3.1 - Forcer les deux dimensions
ALTER TABLE formation.n_commune_osm_000_2016 ADD CONSTRAINT enforce_dims_geom CHECK (ST_NDims(geom)=2);
--> Query returned successfully with no result in 1.1 secs.

---- III.3.2 - Projection en EPSG:2154 :
ALTER TABLE formation.n_commune_osm_000_2016 ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)=2154);
--> Query returned successfully with no result in 1.1 secs.

---- III.3.3 - Géométrie en multi-polygones :
ALTER TABLE formation.n_commune_osm_000_2016 ADD CONSTRAINT enforce_geotype_geom CHECK (GeometryType(geom)='MULTIPOLYGON'::text OR geom IS NULL);
--> Query returned successfully with no result in 640 msec.

---- III.3.4 - Pas d'auto-intersections :
ALTER TABLE formation.n_commune_osm_000_2016 ADD CONSTRAINT enforce_no_self_intersection_geom CHECK (ST_IsSimple(geom));

---- III.3.5 - Pas de ligne sans longueur :
ALTER TABLE formation.n_commune_osm_000_2016 ADD CONSTRAINT enforce_no_zero_length_geom CHECK (ST_Length(geom) > 0);

---- III.3.6 - Pas polygone sans surface :
ALTER TABLE formation.n_commune_osm_000_2016 ADD CONSTRAINT enforce_no_zero_area_geom CHECK (ST_Area(geom) > 0);

---- III.4 - Index Attributaire et géométrique :
-------------------------------------------------
---- III.4.1 - Index attributaire :
----- old : CREATE INDEX n_commune_osm_000_2016_insee_idx ON formation.n_commune_osm_000_2016 USING btree(insee);
CREATE INDEX n_commune_osm_000_2016_insee_idx ON formation.n_commune_osm_000_2016 USING brin(insee);
--> Query returned successfully with no result in 655 msec.

---- III.4.2 - Index géométrique :
CREATE INDEX n_commune_osm_000_2016_geom_gist ON formation.n_commune_osm_000_2016 USING gist(geom);
--> Query returned successfully with no result in 1.1 secs.

---- III.4.2 - CLUSTER de l'index géométrique : regroupe dans les pages de la table, les objets géographiquement proches.
CLUSTER formation.n_commune_osm_000_2016 USING n_commune_osm_000_2016_geom_gist;
--> Query returned successfully with no result in 19.9 secs.

---- III.5 - Droit et accès aux tables :
-----------------------------------------
---- III.5.1 - Mise en place de l'usage du schéma :
GRANT USAGE ON SCHEMA formation TO public;
--> Query returned successfully with no result in 16 msec.

---- III.5.2 - Changement de propriétaire de la table :
ALTER TABLE formation.n_commune_osm_000_2016 OWNER TO ctcs;

---- III.5.3 - Attribution des droits pour le groupe public :
GRANT SELECT ON TABLE formation.n_commune_osm_000_2016 TO public;
--> Query returned successfully with no result in 32 msec.

---- III.5.4 - Attribution des droits pour le groupe ctcs :
GRANT ALL ON TABLE formation.n_commune_osm_000_2016 TO ctcs;

---- III.5.5 - Attribution des droits pour toutes les tables dans un schéma :
GRANT SELECT ON ALL TABLES IN SCHEMA foncier_sol__ff_2009 TO public;

---- III.6 - VACCUM ALL :
-------------------------------------------------
---- III.6.1 - En SQL :
VACUUM FULL;
--> Query returned successfully with no result in 03:14 minutes.

---- III.6.2 - VACCUM en ligne de commande WINDOWS :
SET PGPASSWORD=monmotdepasse
"C:\Program Files\PostgreSQL\9.6\bin\psql.exe" -h srv41-map -p 5444 -d ceremabase -U postgres -c "VACUUM FULL;"

----------------------------
---- IV - Sauvegardes   ----
----------------------------
---- IV.1 - Table telle qu'elle 
CREATE TABLE monschema.sauve_matable_jjmmaaa (LIKE monschema.matable INCLUDING ALL);
INSERT into monschema.sauve_matable_jjmmaaa SELECT * FROM monschema.matable;


--------------------------------
---- V - EXPORTS Postgis   ----
--------------------------------

---- V.1 - via un dump de SQL :
---------------------------------
---- V.1.1 - via un dump de SQL :
---------------------------------
---- V.1.1.1 - le schéma "formation" de la base "ceremabase" tout entier :
SET PGCLIENTENCODING=UTF8
set myvar=%DATE:/=%
"C:\Program Files\PostgreSQL\9.6\bin\pg_dump.exe" -h srv41-map -p 5444 -U postgres -n formation -f D:\sauve_dump_ceremabase.formation.%myvar%.sql ceremabase

--- V.1.1.2 - la seule table "n_commune_osm_000_2016" de la base "ceremabase" :
SET PGCLIENTENCODING=UTF8
set myvar=%DATE:/=%
"C:\Program Files\PostgreSQL\9.5\bin\pg_dump.exe" -h srv41-map -p 5444 -U postgres -t formation.n_commune_osm_000_2016 -f D:\sauve_dump_ceremabase.n_commune_osm_000_2016.%myvar%.sql ceremabase


---- V.1.2 - via un dump de SQL compressé :
---------------------------------------------
---- V.1.2.1 - le schéma "formation" de la base "ceremabase" tout entier :
SET PGCLIENTENCODING=UTF8
set myvar=%DATE:/=%
"C:\Program Files\PostgreSQL\9.6\bin\pg_dump.exe" -h srv41-map -p 5444 -U postgres -n formation ceremabase | "C:\Program Files\7-Zip\7z.exe" a -an -txz -si -so > D:\sauve_dump_ceremabase.formation.%myvar%.sql.xz 

--- V.1.2.2 - la seule table "n_commune_osm_000_2016" de la base "ceremabase" :
SET PGCLIENTENCODING=UTF8
set myvar=%DATE:/=%
"C:\Program Files\PostgreSQL\9.5\bin\pg_dump.exe" -h srv41-map -p 5444 -U postgres -t formation.n_commune_osm_000_2016 ceremabase | "C:\Program Files\7-Zip\7z.exe" a -an -txz -si -so > D:\sauve_dump_ceremabase.n_commune_osm_000_2016.%myvar%.sql.xz

---- V.2 - fichier à plat via OGR2OGR :
-----------------------------------------
--- V.2.1 - ShapeFile
SET PGCLIENTENCODING=UTF8
"C:\OSGeo4W64\bin\ogr2ogr.exe" -gt 65536 -a_srs EPSG:2154 -skipfailures -overwrite -dim 2 -f "ESRI Shapefile" "D:\n_commune_osm_000_2016.shp" PG:"host=srv41-map port=5444 dbname=ceremabase user=postgres" formation.n_commune_osm_000_2016

--- V.2.2 - MapINFO
SET PGCLIENTENCODING=LATIN1

---- V.3 - fichier à plat via pgsql2shp :
-----------------------------------------
--- V.3.1 - ShapeFile
SET PGPASSWORD=monmotdepasse
SET PGCLIENTENCODING=UTF8
"C:\Program Files\PostgreSQL\9.6\bin\pgsql2shp.exe" -f "D:\OCS_AUVERGNE_2011\n_occ_sol_500m2_s_003_2011.shp" -h srv41-map -p 5444 -U postgres ceremabase m_foncier_sol_n_occupation_sol.n_occ_sol_500m2_s_003_2011


-----------------------------------
---- VI - COMPRESSIONS via 7z   ----
-----------------------------------
----- Compression 7z maximale avec l'algorhytme
----- de Dmitry Shkarin's PPMdH optimisé pour le texte : -m0=PPMd

--- VI.1 - SQL :
----------------
"C:\Program Files\7-Zip\7z.exe" a -t7z D:\fichier_SQL_final.7z D:\*.sql -mmt4 -m0=PPMD -v1024m
---- -v1024m taille de l'archive à 1Go
---- -mmt4 : 4 processors

--- VI.2 - ShapeFile :
----------------------
"C:\Program Files\7-Zip\7z.exe" a -t7z D:\fichier_ShapeFile_final.7z D:\*.shp D:\*.dbf D:\*.prj D:\*.shx -mmt -slp -m0=PPMd

--- VI.3 - MapINFO :
--------------------
"C:\Program Files\7-Zip\7z.exe" a -t7z D:\fichier_MapINFO_final.7z D:\*.dat D:\*.map D:\*.id D:\*.tab -mmt -slp -m0=PPMd


-----------------------------------------
---- VII - Administration en nombre   ----
-----------------------------------------
---- Génére une liste de requêtes à exécuter

--- VII.1 - Changement de schéma :
-------------------------------
SELECT 'ALTER TABLE ' || schemaname || '.' || tablename || ' SET SCHEMA schemaderrivee;' AS requete
FROM pg_tables WHERE schemaname NOT LIKE 'schemaderrivee' AND tablename LIKE ('n_terrain_sport_bdt%'); -- AND schemaname LIKE ('schemadedepart')

--- VII.2 - Changement du propriétaire :
-------------------------------
SELECT 'ALTER TABLE ' || schemaname || '.' || tablename || ' OWNER TO admin_detc;' AS requete
FROM pg_tables WHERE schemaname LIKE 'bdtopo__a_voie_comm_route' AND NOT tableowner ='admin_detc';

--- VII.3 - Mise en place des droits de toutes les tables d'un schéma :
---------------------------------------------------------------------
SELECT 'GRANT ALL ON TABLE ' || schemaname || '.' || tablename || ' TO admin_detc;' AS requete
FROM pg_tables WHERE schemaname LIKE 'bdtopo__%';

--- VII.4 - Création d'un index géométrique pour les tables qui n'ont pas d'index dont le nom se termine par '_geom'
------------------------------------------------------------------------------------------------------------------
----- Quelles sont les tables sans index géométriques : sans nom de l'index se terminant par _gist
SELECT * FROM pg_indexes WHERE schemaname LIKE 'bdtopo__%' and indexname LIKE '%_gist'
ORDER by schemaname,tablename;
----- Mise à jour :
SELECT
	'CREATE INDEX '||tablename||'_geom_gist ON '|| schemaname || '.' || tablename || ' USING gist(geom) TABLESPACE index;' AS requete
	FROM pg_tables
	WHERE tablename NOT IN (
		SELECT tablename FROM pg_indexes WHERE schemaname LIKE 'bdtopo__%' and indexname LIKE '%_gist'
		)
		AND schemaname LIKE 'bdtopo__%'
ORDER BY schemaname,tablename;

--- VII.5 - Ajouter les cluster pour les index géométriques qui ne le sont pas :
------------------------------------------------------------------------------
SELECT 'CLUSTER '||n.nspname||'.'||t.relname||' USING '||i.relname||';' as resquete
FROM
    pg_class t,
    pg_class i,
    pg_index ix,
    pg_namespace n,
    pg_attribute a
    
WHERE
    t.oid = ix.indrelid
    AND i.oid = ix.indexrelid
    AND a.attrelid = t.oid
    AND a.attnum = ANY(ix.indkey)
    AND t.relkind = 'r'
    AND n.oid = t.relnamespace
    AND n.nspname like 'bdtopo__%'
    AND (a.attname = 'geom' OR a.attname = 'the_geom')
    AND ix.indisclustered = false
ORDER BY
    t.relname,
    i.relname;


--- VII.6 - Supprimer les sequences inutiles :
--------------------------------------------
SELECT
	'DROP SEQUENCE '||sequence_schema||'.'||sequence_name||' CASCADE;' AS requete
FROM information_schema.sequences
WHERE sequence_schema LIKE 'bdtopo__%';
