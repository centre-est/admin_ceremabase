----=====================================================================================
---- 	VADE-MECUM SUR LES GEOTRAITEMENTS TYPES
----
----             CEREMABASE
----
---- 	Version 1 du 22/07/2020
----	c.badol
----	Finalisé oui| | / non |X|
----=====================================================================================
---- Rq : ---- pour les commentaires / -- pour les commandes optionnelles, debuger


---------------------------------------------
---- O - Corrections d'erreurs poussées  ----
---------------------------------------------
---- si 
UPDATE monschema.matableacorriger SET geom=
    CASE
        WHEN GeometryType(geom) = 'POLYGON'         OR GeometryType(geom) = 'MULTIPOLYGON' THEN
                ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
        WHEN GeometryType(geom) = 'LINESTRING'     OR GeometryType(geom) = 'MULTILINESTRING' THEN
                ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
        WHEN GeometryType(geom) = 'POINT'         OR GeometryType(geom) = 'MULTIPOINT' THEN
                ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
        ELSE ST_MakeValid(geom)
    END
WHERE NOT ST_Isvalid(geom);
---- ne corrige pas la couche
---- essayer :
UPDATE monschema.matableacorriger SET geom = st_buffer(geom,0);
---- si toujours des soucis 
---- essayer mais avec legère modification des géométries, surtout un même enregistrement contient des objets supperposés
CREATE TABLE table_corrigee as 
SELECT clefprimaire, ST_Buffer(ST_Union(ST_Buffer(geom,0.001)),-0.001)::geometry('polygon',2154) as geom
FROM monschema.matableacorriger
GROUP BY clefprimaire ---- obligatoire sinon les objets qui se touchent sont fusionnés

-------------------------------
---- 10 découper une couche de 
----    polygones par rapport
----    à un autre couche
----    de polygones
-------------------------------
;DO $$
DECLARE
nomduschema 				character varying;
nomdelatable1				character varying;
nomdelatable2				character varying;
req 						text;
BEGIN
---- Les paramètres :
nomduschema :=  'w_mrc';
nomdelatable1 := 'TOT-V2';
nomdelatable2 := 'TOT-Q100-H2';

---- Le Script 
req := '
DROP TABLE IF EXISTS "' || nomduschema || '"."' || nomdelatable1 || '_recouvre_' || nomdelatable2 || '";
CREATE TABLE "' || nomduschema || '"."' || nomdelatable1 || '_recouvre_' || nomdelatable2 || '" AS 
SELECT row_number() over() as id,
    ST_Multi(
        st_collectionextract(
		st_force_collection(
		st_makevalid(
            ST_Intersection(t1.geom, t2.geom)
            )
			)
        ,3)
    )::geometry(''MULTIPOLYGON'',2154) as geom
from "' || nomduschema || '"."' || nomdelatable1 || '" as t1
     inner join "' || nomduschema || '"."' || nomdelatable2 || '" as t2 
	 on ST_Intersects(t1.geom, t2.geom) 
where not ST_IsEmpty(ST_Buffer(ST_Intersection(t1.geom, t2.geom), 0.0));
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

--------------------------------------
---- 20 - Supprimer des trous d'une
----      certaine taille dans une
----      couche de polygones à trous 
--------------------------------------
---- Astuce : Identification des scories pour définir le paramètre du surfacemaxaevider
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
-----> changer monschema.matable
            from a_dcap_actu_arretes_montagne_dhup.temp_perimetre_004
-----> changer monschema.matable            
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, st_area(ST_MULTI(st_makepolygon(st_interiorringn(geom,n))))::integer as surface_m2, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry('MULTIPOLYGON',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
order by surface_m2 DESC;
/*
pk |id_tache|num_trou|surface_m2|
---+--------+--------+----------+
 80|       1|      80|  36633861|
 74|       1|      74|  36190624|
 61|       1|      61|  21079476|
  4|       1|       4|  18419056|
105|       1|     105|  11811399|
177|       1|     177|     90411|
205|       1|     205|     77465|
193|       1|     193|     63673|
149|       1|     149|     47029|
 */

;DO $$
DECLARE
nomduschema 				character varying;
nomdelatable				character varying;
surfacemaxaevider			integer;
req 						text;
BEGIN
---- Les paramètres :
nomduschema :=  'a_dcap_actu_arretes_montagne_dhup';
nomdelatable := 'temp_perimetre_004';
surfacemaxaevider := 100000; ---- si les couches sont en coordonnnées métriques : en m2

---- Le Script 
---- Premièrement : on met le polygone sans les trous
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_sans_trous;
CREATE TABLE "' || nomduschema || '".table_sans_trous as
	SELECT ROW_NUMBER() OVER() AS id,
	ST_MULTI(st_makepolygon(st_exteriorring(st_geometryn(geom,1))))::geometry(''MULTIPOLYGON'',2154) as geom
	FROM "' || nomduschema || '"."' || nomdelatable || '";
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- Deuxièmement : On créé une TABLE que de trous de plus de 10m2 :
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous_gardes;
CREATE TABLE "' || nomduschema || '".table_de_trous_gardes AS (
WITH creation_des_anneaux as (
    WITH decompte_trou AS (
        with tache_anneaux_poly as(
            select row_number() over () as id, st_geometryN(geom,1) AS geom
            from "' || nomduschema || '"."' || nomdelatable || '"
        )
    select id, geom, ST_NumInteriorRings(geom) as nb_trou
    from tache_anneaux_poly
    )
    select row_number() over() as pk, id as id_tache, n as num_trou, ST_MULTI(st_makepolygon(st_interiorringn(geom,n)))::geometry(''MULTIPOLYGON'',2154) as geom
    from decompte_trou
    CROSS JOIN generate_series(1,nb_trou) as n
    where nb_trou > 0
)
select * from creation_des_anneaux
where st_area(geom) > ' || surfacemaxaevider || '
);
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- FINAL : On découpe la tache sans trou avec les trous gardés
req := '
DROP TABLE IF EXISTS "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous";
CREATE TABLE "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous" AS
select row_number() over() as id,
st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_difference((st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_union(t1.geom))),3))),(st_multi(st_collectionextract(st_forcecollection(st_makevalid(st_union(t2.geom))),3)))))),3))::geometry(''MultiPolygon'',2154) as geom
FROM
    "' || nomduschema || '".table_sans_trous as t1,
    "' || nomduschema || '".table_de_trous_gardes as t2
WHERE st_intersects(t1.geom,t2.geom);
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- FINAL BIS : On n'oublie pas les derniers polygones qui ne sont pas concernés par des trous
req := '
INSERT INTO "' || nomduschema || '"."' || nomdelatable || '_sans_petits_trous"
select row_number() over() as id, st_multi(t1.geom)
FROM
    "' || nomduschema || '".table_sans_trous as t1,
    "' || nomduschema || '".table_de_trous_gardes as t2
where st_intersects(t1.geom,t2.geom) IS FALSE;
';
RAISE NOTICE '%', req;
EXECUTE(req);
---- POUR FAIRE PROPRE : on supprime les tables temporaires :
req := '
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous;
DROP TABLE IF EXISTS "' || nomduschema || '".table_de_trous_gardes;
DROP TABLE IF EXISTS "' || nomduschema || '".table_sans_trous; 
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

-------------------------------
---- 30 Evider une couche par
---- une seconde couche
-------------------------------
;DO $$
DECLARE
nomduschema 				character varying;
table_decoupee				character varying;
table_decoupante				character varying;
req 						text;
BEGIN
---- Les paramètres :
nomduschema :=  'w_mrc';
table_decoupee := 'TOT-Q10-H3BIS';
table_decoupante := 'TOT-Q10-H4BIS';

---- Le Script 
---- 1°) Table temporaire découpée
req := '
DROP TABLE IF EXISTS ' || nomduschema || '.temp_decoupe;
CREATE TABLE ' || nomduschema || '.temp_decoupe as
SELECT b.id, ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(st_union(a.geom))),3))::geometry(MultiPolygon,2154) as geom_union
-- Couche à découper
FROM ' || nomduschema || '."' || table_decoupee || '" as b
-- Couche de découpe
JOIN ' || nomduschema || '."' || table_decoupante || '" as a
ON st_intersects(a.geom, b.geom)
GROUP BY b.id;
';
RAISE NOTICE '%', req;
EXECUTE(req);


---- Index pour aller plus vite
req := '
CREATE INDEX temp_decoupe1_id ON ' || nomduschema || '.temp_decoupe USING btree(id);
';
EXECUTE(req);

---- 2°) Requêt finale qui verse les polygones troués
req := '
DROP TABLE IF EXISTS ' || nomduschema || '."' || table_decoupee || '_decoupe_par_' || table_decoupante || '";
CREATE TABLE ' || nomduschema || '."' || table_decoupee || '_decoupe_par_' || table_decoupante || '" as
SELECT
	a.id,
	''' || table_decoupee || ''' as hauteurs,
	ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(st_difference(a.geom,b.geom_union))),3))::geometry(MultiPolygon,2154) as geom
FROM ' || nomduschema || '."' || table_decoupee || '" as a
LEFT JOIN ' || nomduschema || '.temp_decoupe as b
ON a.id=b.id
WHERE NOT ST_IsEmpty(st_difference(a.geom,b.geom_union));
';
RAISE NOTICE '%', req;
EXECUTE(req);

---- 3°) Requête qui verse les polygones non troués car sans intersection
req := '
INSERT INTO ' || nomduschema || '."' || table_decoupee || '_decoupe_par_' || table_decoupante || '"
WITH table_unionnee AS (
	SELECT ST_union(geom)::geometry(''MULTIPOLYGON'',2154) AS geom FROM ' || nomduschema || '."' || table_decoupante || '"
)
SELECT
	row_number() over() as id,
	''' || table_decoupee || ''' as hauteurs,
	t1.geom
FROM 
	w_mrc."' || table_decoupee || '" as t1
JOIN
	table_unionnee as t2
ON
	NOT st_intersects (t1.geom, t2.geom);
';
RAISE NOTICE '%', req;
EXECUTE(req);

---- 4°) Requête qui fusionne les polygones selon le champs 
req := '
DROP TABLE IF EXISTS ' || nomduschema || '."' || table_decoupee || '_decoupe_par_' || table_decoupante || '_fusionnee";
CREATE TABLE ' || nomduschema || '."' || table_decoupee || '_decoupe_par_' || table_decoupante || '_fusionnee" as
SELECT
	row_number() over() as id,
	hauteurs,
	ST_Union(geom)::geometry(''MULTIPOLYGON'',2154) AS geom
FROM
' || nomduschema || '."' || table_decoupee || '_decoupe_par_' || table_decoupante || '"
GROUP BY
hauteurs;
';
RAISE NOTICE '%', req;
EXECUTE(req);

---- 5°) On fait propre : on supprime la table et indexe la géométrie
req := '
DROP TABLE IF EXISTS ' || nomduschema || '.temp_decoupe;
CREATE INDEX "' || table_decoupee || '_decoupe_par_' || table_decoupante || '_fusionnee_geom_gist"
	ON ' || nomduschema || '."' || table_decoupee || '_decoupe_par_' || table_decoupante || '_fusionnee" 
	USING gist(geom);
CLUSTER ' || nomduschema || '."' || table_decoupee || '_decoupe_par_' || table_decoupante || '_fusionnee"
	USING "' || table_decoupee || '_decoupe_par_' || table_decoupante || '_fusionnee_geom_gist";
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

-------------------------------
---- 31 découper les tronçons des routes
---- en lignes élémentaires (2 point)
---- récupérer les altitudes dans le pointZ
---- calculer une pente (orientée entre le point1 et point2)
-------------------------------
;DO $$
DECLARE
nomduschema 				character varying;
table_bdtopo				character varying;
table_admin_express			character varying;
dpt							character varying(2);
req 						text;
BEGIN
---- Les paramètres :
nomduschema :=  'w_aci';
table_bdtopo := 'r_bdtopo_2020.n_troncon_de_route_bdt_fra_2020';
table_admin_express := 'r_admin_express.n_adm_exp_cog_departement_fra_2020';
dpt :='38';

req := '
drop table if exists w_aci.eclate_bdtopo_badol_038;
create table w_aci.eclate_bdtopo_badol_038 as
with requete_finale as 
	(
	with requete_de_verification as 
----- Regenere les tronçons simples
----- les uns après les autres
----- sans doublons, sans plus de 2 points
----- avec tous les champs de vérification
		(
		with ST_DumpPoints as
-------- Extraction des points de Lignes
-------- en gardant l´identifiant et le numéro d´ordre du point
			(
			with st_dump_troncons as
------------ Extraction des lignes en cas de 
------------ MultiPolylignes
				(
				with extration_bdtopo as
------------- Extraction des routes selon un département d´admin Express
------------- et selon le Champs [importance] soit le niveau d´importance de la bd topo
					(
					SELECT t1.*
					FROM ' || table_bdtopo || ' as t1
					join ' || table_admin_express || ' as t2
					on st_intersects (t1.geom, t2.geom)
					where t2.insee_dep =''38'' and (t1.importance = ''1'' or t1.importance = ''2'' or t1.importance = ''3'' or t1.importance = ''4'')
					)
				select
					row_number () over () as id,
					cleabs,
					(ST_dump(ST_Transform(geom3d,2154))).geom::geometry(LineStringZ,2154) as geom3d 
				from extration_bdtopo
				)
			SELECT id,
				cleabs,
				(ST_DumpPoints(geom3d)).path[1] as path,
				(ST_DumpPoints(geom3d)).geom::geometry(PointZ,2154) as geom3d 
			from st_dump_troncons
			)
		select distinct
			t1.id,
			t1.cleabs,
			t1.path as ordre,
			t1.geom3d::geometry(PointZ,2154) as geom3d_1,
			t2.geom3d::geometry(PointZ,2154) as geom3d_2,
			ST_Distance(t1.geom3d, t2.geom3d) as distance,
			st_z(t1.geom3d) as altitude_1,
			st_z(t2.geom3d) as altitude_2,
			(st_z(t1.geom3d)-st_z(t2.geom3d)) as hauteur,
			(st_z(t1.geom3d)-st_z(t2.geom3d))/ST_Distance(t1.geom3d, t2.geom3d)*100 as pente_pourcent
		from ST_DumpPoints as t1
		join ST_DumpPoints as t2
		on t1.id = t2.id and t1.path < t2.path and (t2.path - t1.path = 1) and ST_Distance( t1.geom3d, t2.geom3d) > 0
		order by ST_Distance( t1.geom3d, t2.geom3d)
	)
	select 	id::integer as id_badol,
			cleabs,
			ordre::integer,
			altitude_1::numeric(10,2),
			altitude_2::numeric(10,2),
			distance::numeric(10,2),
			pente_pourcent::numeric(10,2),
			ST_MakeLine(geom3d_1,geom3d_2)::geometry(LineStringZ,2154) as geom3d
	from requete_de_verification
	order by cleabs, ordre
	)
select 
 		t1.id_badol,
		t1.cleabs,
		t1.ordre::integer,
		t1.altitude_1::numeric(10,2),
		t1.altitude_2::numeric(10,2),
		t1.distance::numeric(10,2),
		t1.pente_pourcent::numeric(10,2),
		t2.nature,
		t2.nom_1_gauche,
		t2.nom_1_droite,
		t2.nom_2_gauche,
		t2.nom_2_droite,
		t2.importance,
		t2.fictif,
		t2.position_par_rapport_au_sol,
		t2.etat_de_l_objet,
		t2.date_creation,
		t2.date_modification,
		t2.date_d_apparition,
		t2.date_de_confirmation,
		t2.sources,
		t2.identifiants_sources,
		t2.precision_planimetrique,
		t2.precision_altimetrique,
		t2.nombre_de_voies,
		t2.largeur_de_chaussee,
		t2.itineraire_vert,
		t2.prive,
		t2.sens_de_circulation,
		t2.bande_cyclable,
		t2.reserve_aux_bus,
		t2.urbain,
		t2.vitesse_moyenne_vl,
		t2.acces_vehicule_leger,
		t2.acces_pieton,
		t2.periode_de_fermeture,
		t2.nature_de_la_restriction,
		t2.restriction_de_hauteur,
		t2.restriction_de_poids_total,
		t2.restriction_de_poids_par_essieu,
		t2.restriction_de_largeur,
		t2.restriction_de_longueur,
		t2.matieres_dangereuses_interdites,
		t2.borne_debut_gauche,
		t2.borne_debut_droite,
		t2.borne_fin_gauche,
		t2.borne_fin_droite,
		t2.insee_commune_gauche,
		t2.insee_commune_droite,
		t2.type_d_adressage_du_troncon,
		t2.alias_gauche,
		t2.alias_droit,
		t2.code_postal_gauche,
		t2.code_postal_droit,
		t2.date_de_mise_en_service,
		t2.identifiant_voie_1_gauche,
		t2.identifiant_voie_1_droite,
		t2.liens_vers_route_nommee,
		t2.cpx_numero,
		t2.cpx_numero_route_europeenne,
		t2.cpx_classement_administratif,
		t2.cpx_gestionnaire,
		t2.cpx_toponyme_route_nommee,
		t2.cpx_toponyme_itineraire_cyclable,
		t2.cpx_toponyme_voie_verte,
		t1.geom3d
from requete_finale as t1
join ' || table_bdtopo || ' as t2
on t1.cleabs = t2.cleabs;
--> Updated Rows	347777
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;


---------------------------------
---- 40 - Un vrai ST_Difference
---------------------------------
;DO $$
DECLARE
nomduschema 				character varying;
nomdelatable1				character varying;
nomdelatable2				character varying;
concatnomstables			character VARYING;
req 						text;
BEGIN
---- Les paramètres :
nomduschema :=  'w_mrc';
nomdelatable1 := 'TOT-Q10-H3BIS';
nomdelatable2 := 'TOT-Q10-H4BIS';

---- Le Script 
---- 1°) Table temporaire découpée
req := '
DROP TABLE IF EXISTS ' || nomduschema || '.temp_decoupe;
CREATE TABLE ' || nomduschema || '.temp_decoupe as
SELECT b.id, ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(st_union(a.geom))),3))::geometry(MultiPolygon,2154) as geom_union
-- Couche à découper
FROM ' || nomduschema || '."' || nomdelatable1 || '" as b
-- Couche de découpe
JOIN ' || nomduschema || '."' || nomdelatable2 || '" as a
ON st_intersects(a.geom, b.geom)
GROUP BY b.id;
';
RAISE NOTICE '%', req;
EXECUTE(req);


---- Index pour aller plus vite
req := '
CREATE INDEX temp_decoupe1_id ON ' || nomduschema || '.temp_decoupe USING btree(id);
';
EXECUTE(req);

---- 2°) Requêt finale qui verse les polygones troués
concatnomstables := nomdelatable1 || '_' || nomdelatable2;
--DEBUG : RAISE NOTICE '%', concatnomstables;
req := '
DROP TABLE IF EXISTS ' || nomduschema || '."' || nomdelatable1 || '_difference_' || nomdelatable2 || '";
CREATE TABLE ' || nomduschema || '."' || nomdelatable1 || '_difference_' || nomdelatable2 || '" as
SELECT
	a.id,
	''' || concatnomstables || ''' as hauteurs,
	ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(st_difference(a.geom,b.geom_union))),3))::geometry(MultiPolygon,2154) as geom
FROM ' || nomduschema || '."' || nomdelatable1 || '" as a
LEFT JOIN ' || nomduschema || '.temp_decoupe as b
ON a.id=b.id
WHERE NOT ST_IsEmpty(st_difference(a.geom,b.geom_union));
';
RAISE NOTICE '%', req;
EXECUTE(req);

---- 3°) Requête qui verse les polygones non troués de la couche1 car sans intersection 
req := '
INSERT INTO ' || nomduschema || '."' || nomdelatable1 || '_difference_' || nomdelatable2 || '"
WITH table_unionnee AS (
	SELECT ST_union(geom)::geometry(''MULTIPOLYGON'',2154) AS geom FROM ' || nomduschema || '."' || nomdelatable2 || '"
)
SELECT
	row_number() over() as id,
	''' || nomdelatable1 || ''' as hauteurs,
	t1.geom
FROM 
	w_mrc."' || nomdelatable1 || '" as t1
JOIN
	table_unionnee as t2
ON
	NOT st_intersects (t1.geom, t2.geom);
';
RAISE NOTICE '%', req;
EXECUTE(req);

---- 3bis°) Requête qui verse les polygones non troués de la couche2 car sans intersection 
req := '
INSERT INTO ' || nomduschema || '."' || nomdelatable1 || '_difference_' || nomdelatable2 || '"
WITH table_unionnee AS (
	SELECT ST_union(geom)::geometry(''MULTIPOLYGON'',2154) AS geom FROM ' || nomduschema || '."' || nomdelatable1 || '"
)
SELECT
	row_number() over() as id,
	''' || nomdelatable2 || ''' as hauteurs,
	t1.geom
FROM 
	w_mrc."' || nomdelatable2 || '" as t1
JOIN
	table_unionnee as t2
ON
	NOT st_intersects (t1.geom, t2.geom);
';
RAISE NOTICE '%', req;
EXECUTE(req);

---- 4°) Requête qui fusionne les polygones selon le champs 
req := '
DROP TABLE IF EXISTS ' || nomduschema || '."' || nomdelatable1 || '_difference_' || nomdelatable2 || '_fusionnee";
CREATE TABLE ' || nomduschema || '."' || nomdelatable1 || '_difference_' || nomdelatable2 || '_fusionnee" as
SELECT
	row_number() over() as id,
	hauteurs,
	ST_Union(geom)::geometry(''MULTIPOLYGON'',2154) AS geom
FROM
' || nomduschema || '."' || nomdelatable1 || '_difference_' || nomdelatable2 || '"
GROUP BY
hauteurs;
';
RAISE NOTICE '%', req;
EXECUTE(req);

---- 5°) On fait propre : on supprime la table temporaore et indexe les géométries
req := '
DROP TABLE IF EXISTS ' || nomduschema || '.temp_decoupe;
CREATE INDEX "' || nomdelatable1 || '_difference_' || nomdelatable2 || '_geom_gist"
	ON ' || nomduschema || '."' || nomdelatable1 || '_difference_' || nomdelatable2 || '" 
	USING gist(geom);
CLUSTER ' || nomduschema || '."' || nomdelatable1 || '_difference_' || nomdelatable2 || '"
	USING "' || nomdelatable1 || '_difference_' || nomdelatable2 || '_geom_gist";
CREATE INDEX "' || nomdelatable1 || '_difference_' || nomdelatable2 || '_fusionnee_geom_gist"
	ON ' || nomduschema || '."' || nomdelatable1 || '_difference_' || nomdelatable2 || '_fusionnee" 
	USING gist(geom);
CLUSTER ' || nomduschema || '."' || nomdelatable1 || '_difference_' || nomdelatable2 || '_fusionnee"
	USING "' || nomdelatable1 || '_difference_' || nomdelatable2 || '_fusionnee_geom_gist";
';
RAISE NOTICE '%', req;
EXECUTE(req);
END $$;

-----------------------------------------------------------
---- 51 - Partie de la commune située à l'Ouest d'une route
-----------------------------------------------------------
---- 42297     |Salvizinet 
---- Partie située à l’Ouest du C.D.8 
with resultat1 as  ---- renvoie la Bounding BOX avec le polygone découpé
	(with polygones as ---- Tous les polygones de la commune découpés par la route
		(with route as ---- Fusion des tronçons de route de la commune avec le meme numéro : ici D8
			(select st_union(t2.geom)::geometry('MULTILINESTRING',2154) as union_geom
			from r_bdtopo_2021.n_commune_bdt_042_2021 as t1
			join r_bdtopo_2021.n_troncon_de_route_bdt_042_2021 as t2
			on st_intersects (t1.geom, t2.geom)
			where t1.insee_com = '42297' and t2.numero = 'D8')
		select (ST_dump(ST_CollectionExtract(st_split(t1.geom, t2.union_geom),3))).geom::geometry('POLYGON',2154) as geom
		from r_bdtopo_2021.n_commune_bdt_042_2021 as t1
		join route as t2
		on st_intersects(t1.geom, t2.union_geom)
		where t1.insee_com = '42297')
		select st_xmin(ST_Envelope(geom)), geom from polygones)
		,
	minimal as (select min(st_xmin) as resultat from resultat1) ---- renvoye le st_xmin de l'objet le plus à l'Ouest
select t1.geom ---- Géométrie de l'objet ayant le plus à l'Ouest
from resultat1 as t1
join minimal as t2
on t1.st_xmin = t2.resultat

-----------------------------------------------------------
---- 52 - Partie de la commune située à l'Est d'une route
-----------------------------------------------------------
---- 42297     |Salvizinet 
---- Partie située à l’Est du C.D.10 
with resultat1 as 
	(with polygones as 
		(with route as 
			(select st_union(t2.geom)::geometry('MULTILINESTRING',2154) as union_geom
			from r_bdtopo_2021.n_commune_bdt_042_2021 as t1
			join r_bdtopo_2021.n_troncon_de_route_bdt_042_2021 as t2
			on st_intersects (t1.geom, t2.geom)
			where t1.insee_com = '42297' and t2.numero = 'D10')
		select (ST_dump(ST_CollectionExtract(st_split(t1.geom, t2.union_geom),3))).geom::geometry('POLYGON',2154) as geom
		from r_bdtopo_2021.n_commune_bdt_042_2021 as t1
		join route as t2
		on st_intersects(t1.geom, t2.union_geom)
		where t1.insee_com = '42297')
		select st_xmax(ST_Envelope(geom)), geom from polygones)
		,
	maximal as (select max(st_xmax) as resultat from resultat1)
select t1.geom from resultat1 as t1
join maximal as t2
on t1.st_xmax = t2.resultat

-----------------------------------------------------------
---- 60 - Plus proche requête de voisin dans PostGIS
-----------------------------------------------------------
---- https://askcodez.com/k-plus-proche-requete-de-voisin-dans-postgis.html
select zai.id,
      (SELECT  bati.id FROM p_erpv.c_batiment_s_971 as bati  
       ORDER BY ST_Distance(zai.geom, bati.geom5490)
       LIMIT 1)
from  p_erpv.d971_zai_non_bati as zai;
