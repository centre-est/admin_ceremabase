/*
[ADMIN] - Affection syst�matique et automatique de tous les index dans CeremaBase au tablespace "index".

Option :
--------
- aucune

Tables concern�es :
-------------------
- toutes les tables de tous les sch�mas

Taches r�alis�es :
------------------
Scrute tous les sch�mas de CeremaBase
- Ex�cute la fonction w_adl_delegue.set_tablespace_index("schema") qui d�place tous les index dans ce sch�ma vers le tablespace index

A am�liorer :
-------------
Pas possible de faire une fonction car out of shared memory / Indice�: You might need to increase max_locks_per_transaction
 --> obligation de faire un COMMIT apr�s chaque  SELECT w_adl_delegue.set_tablespace_index('''|| r.schemaname ||''');

derni�re MAJ :
--------------
14 mai 2020
*/
;DO $$
DECLARE
r record;			---- variable des sch�mas � parcourir
req text;			---- requ�te � passer	
n_user text;		---- utilisateur
BEGIN 

-- v�rification de l'identit� de l'op�rateur :
SELECT CURRENT_USER INTO n_user ;
IF NOT n_user = 'postgres'
THEN
        RAISE EXCEPTION E'Cette fonction ne peut �tre ex�cut�e que par le r�le d''ADL.' ;
END IF;

---- Affecte la Tablespace 'index' � tous les index
FOR r IN SELECT DISTINCT schemaname from pg_tables
LOOP	
	req := 'SELECT w_adl_delegue.set_tablespace_index('''|| r.schemaname ||''');';
	EXECUTE(req);
	RAISE NOTICE '%', req;
	COMMIT;
END LOOP;
END $$;