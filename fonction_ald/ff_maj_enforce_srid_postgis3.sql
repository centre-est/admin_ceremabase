---- Fontion srid n'existe plus
---- Quelles sont les contraintes qui posent problème :
select
		pgc.conname as constraint_name,
       ccu.table_schema as table_schema,
       ccu.table_name,
       ccu.column_name,
       pgc.consrc as definition
from pg_constraint pgc
join pg_namespace nsp on nsp.oid = pgc.connamespace
join pg_class  cls on pgc.conrelid = cls.oid
left join information_schema.constraint_column_usage ccu
          on pgc.conname = ccu.constraint_name
          and nsp.nspname = ccu.constraint_schema
where contype ='c'
AND pgc.consrc like '%.srid%'
order by ccu.table_schema, ccu.table_name;

---- Quelles sont les grands types automatisables :
select
       ccu.column_name,
       pgc.consrc as definition,
       count(*)
from pg_constraint pgc
join pg_namespace nsp on nsp.oid = pgc.connamespace
join pg_class  cls on pgc.conrelid = cls.oid
left join information_schema.constraint_column_usage ccu
          on pgc.conname = ccu.constraint_name
          and nsp.nspname = ccu.constraint_schema
where contype ='c'
AND pgc.consrc like '%.srid%'
group by ccu.column_name, pgc.consrc 
order by ccu.column_name, pgc.consrc;
/*
column_name|definition                   |count|
-----------|-----------------------------|-----|
geomloc    |(public.srid(geomloc) = 2154)| 6072|
geompar    |(public.srid(geompar) = 2154)| 1518|
*/

---- Génération du script de mise à jour :
(
select distinct
		'ALTER TABLE ' || ccu.table_schema || '.' || ccu.table_name || ' DROP CONSTRAINT ' || pgc.conname || ';--' as requete
from pg_constraint pgc
join pg_namespace nsp on nsp.oid = pgc.connamespace
join pg_class  cls on pgc.conrelid = cls.oid
left join information_schema.constraint_column_usage ccu
          on pgc.conname = ccu.constraint_name
          and nsp.nspname = ccu.constraint_schema
where contype ='c'
and pgc.consrc like '(public.srid(geomloc) = 2154)'
order by requete
)
UNION ALL
(
select distinct
		'ALTER TABLE ' || ccu.table_schema || '.' || ccu.table_name || ' ADD CONSTRAINT ' || pgc.conname || ' CHECK (St_Srid(geomloc) = 2154);--' as requete
from pg_constraint pgc
join pg_namespace nsp on nsp.oid = pgc.connamespace
join pg_class  cls on pgc.conrelid = cls.oid
left join information_schema.constraint_column_usage ccu
          on pgc.conname = ccu.constraint_name
          and nsp.nspname = ccu.constraint_schema
where contype ='c'
AND pgc.consrc = '(public.srid(geomloc) = 2154)'
order by requete
)
union all 
(
select distinct
		'ALTER TABLE ' || ccu.table_schema || '.' || ccu.table_name || ' DROP CONSTRAINT ' || pgc.conname || ';--' as requete
from pg_constraint pgc
join pg_namespace nsp on nsp.oid = pgc.connamespace
join pg_class  cls on pgc.conrelid = cls.oid
left join information_schema.constraint_column_usage ccu
          on pgc.conname = ccu.constraint_name
          and nsp.nspname = ccu.constraint_schema
where contype ='c'
AND pgc.consrc = '(public.srid(geompar) = 2154)'
order by requete
)
UNION ALL
(
select distinct
		'ALTER TABLE ' || ccu.table_schema || '.' || ccu.table_name || ' ADD CONSTRAINT ' || pgc.conname || ' CHECK (St_Srid(geompar) = 2154);--' as requete
from pg_constraint pgc
join pg_namespace nsp on nsp.oid = pgc.connamespace
join pg_class  cls on pgc.conrelid = cls.oid
left join information_schema.constraint_column_usage ccu
          on pgc.conname = ccu.constraint_name
          and nsp.nspname = ccu.constraint_schema
where contype ='c'
AND pgc.consrc = '(public.srid(geompar) = 2154)'
order by requete
);
