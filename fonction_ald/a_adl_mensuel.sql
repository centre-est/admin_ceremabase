CREATE OR REPLACE FUNCTION w_adl.a_adl_mensuel()
 RETURNS void
 LANGUAGE plpgsql
AS $function$
DECLARE
r record; 
BEGIN 

	EXECUTE('SELECT w_adl.set_droits_public_referentiels();');
	--EXECUTE('SELECT w_adl.set_auto_tablespace_index();'); -- ne fonctionne pas : out of shared memory / Indice�: You might need to increase max_locks_per_transaction
	EXECUTE('SELECT w_adl.create_metadonnees();');
END; 
$function$
;

COMMENT ON FUNCTION w_adl.a_adl_mensuel() IS 'Fonction mensuelle d�ALD :
- met toutes les donn�es dans tablespace data,
- met tous les index dans dans tablespace index,
- remet � jour les tables de m�tadonn�es de sch�ma et global
';
