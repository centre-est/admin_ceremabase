CREATE OR REPLACE FUNCTION w_adl.set_creer_metadonnees()
 RETURNS void
 LANGUAGE plpgsql
AS $function$
 /*
[ADMIN] - Mise en place dans les schémas d'une vue des commentaires sur les tables et d'une vue des commentaires sur les attributs

A] Ajoute  dans le schéma public :
 _metadonnees_toutes_tables
 _metadonnees_tout_attributs
 
B] Ajoute automatiquement deux vues de métadonnées dans tous les schéma présents : 
 _metadonnees_attributs : une vue des commentaires des attributs de toutes les tables du schéma
 _metadonnees_tables : une vue des commentaires sur toutes les tables du schéma

Paramètres :
--------
aucun

Tables concernées :
-------------------
- toutes les tables de tous les schémas sauf ceux commançants par "pg_" et le schéma "public"

Taches réalisées :
------------------
A] crée dans le schéma public deux ressources :
	A.1] _metadonnees_toutes_tables : vue des métadonnées de toutes les tables et vue de CeremaBata
	A.2] _metadonnees_tout_attributs : table des métadonnées detous les attributs de CeremaBase
	A.3] affecte à ces deux tables des droits public + commentaires table + champs
B] Scrute toutes les schémas et les place dans une liste de schéma et pour l´ensemble des schémas de cette liste :
	B.1] crée une vue des métadonnées de tables
	B.2] crée une vue des métadonnées des attributs
	B.3] affecte à ces deux tables des droits public  + commentaires table + champs

A améliorer :
-------------
vérifier si ca fonctionne sans :
GRANT SELECT ON TABLE pg_catalog.pg_tables TO consultation;
GRANT SELECT ON TABLE pg_catalog.pg_class TO consultation;
GRANT SELECT ON TABLE pg_catalog.pg_namespace TO consultation;
GRANT SELECT ON TABLE pg_catalog.pg_attribute TO consultation;
GRANT SELECT ON TABLE pg_catalog.pg_description TO consultation;

Les tables et vues de métadonnées ainsi crées sont référencées par elles-mêmes :
- Léger intérêt pour le compte consultation
- intérêt pour les chargés d'étude ?

CREATE OR REPLACE VIEW modifie-t-il son oid ?

dernière MAJ :
--------------
11/05/2020
*/
DECLARE
r record;				---- liste des ressources inventoriées
req text;				---- requête à passer
nom_postgres varchar;	---- nom à afficher à la place de postgres pour le propriétaire
BEGIN 
----	A.1] crée la vue public._metadonnees_toutes_tables
	req :='
		DROP VIEW IF EXISTS public._metadonnees_toutes_tables;
		CREATE VIEW public._metadonnees_toutes_tables AS 
		SELECT
				row_number() over ()::integer aS id,
				n.nspname::varchar AS nom_schema,
		    		c.relname::varchar AS nom_table,
		    		obj_description(c.oid, ''pg_class''::name)::text AS description_table,
				CASE
					WHEN pg_get_userbyid(c.relowner) = ''postgres'' THEN ''ALD DterCE''
					ELSE pg_get_userbyid(c.relowner)
				END::varchar AS a_contacter,
				CASE
					WHEN c.relkind = ''r'' THEN ''table''
					WHEN c.relkind = ''v'' THEN ''vue''
					WHEN c.relkind = ''m'' THEN ''vue matérialisée''
					WHEN c.relkind = ''p'' THEN ''table paritionnée''
					WHEN c.relkind = ''f'' THEN ''table étrangère''
				END::varchar AS type_table    		
		   FROM pg_class c
		     LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
		     --LEFT JOIN pg_attribute a ON c.oid = a.attrelid AND a.attnum = 0 AND NOT a.attisdropped
		     --LEFT JOIN pg_stat_all_tables s ON c.oid = s.relid
		  WHERE n.nspname !~~ ''pg_%''::TEXT AND c.relkind IN (''r'',''v'',''m'',''p'',''f'')
		  GROUP BY n.nspname, c.relowner, c.relkind, c.relname, c.oid
		  ORDER BY n.nspname, c.relname;
		';
	RAISE NOTICE '%', req;
	EXECUTE(req);	
---- A.2] crée la vue public._metadonnees_tout_attributs
	req :='
		DROP TABLE IF EXISTS public._metadonnees_tout_attributs;
		CREATE TABLE public._metadonnees_tout_attributs AS
		SELECT
			row_number() over ()::integer aS id,
			i.table_schema::varchar as nom_schema,
			i.table_name::varchar as nom_table,
			i.column_name::varchar as nom_champ,
			d.description::varchar as description_champ,
			i.data_type::varchar as type_attribut
		FROM
			pg_class c
			LEFT JOIN pg_namespace n
					ON n.oid = c.relnamespace
			LEFT JOIN pg_attribute a
					ON c.oid = a.attrelid AND a.attnum > 0 AND NOT a.attisdropped,
			information_schema.columns i,
			pg_description d
		WHERE
			d.classoid = ''1259''::oid AND d.objoid = c.oid AND
			a.attnum = d.objsubid AND i.table_schema::name = n.nspname AND
			i.table_name::name = c.relname AND i.column_name::name = a.attname AND
			i.ordinal_position::integer = a.attnum AND i.table_schema::text NOT LIKE ''pg_%''::text
		  --GROUP BY n.nspname, i.table_schema, i.table_name, i.column_name, c.relname, a.attname, i.data_type, d.description, c.oid
		 ORDER BY
		 n.nspname, i.table_schema, i.table_name,i.column_name;
		';
	RAISE NOTICE '%', req;
	EXECUTE(req);
---- A.3]  Ajout des commentaires et des droits
	req :='
		GRANT SELECT ON TABLE public._metadonnees_toutes_tables TO public;
		COMMENT ON VIEW public._metadonnees_toutes_tables IS ''Vue des commentaires sur toutes les tables de CeremaBase.'';
		COMMENT ON COLUMN  public._metadonnees_toutes_tables.id IS ''Identifiant nécessaire à QGIS.'';
		COMMENT ON COLUMN  public._metadonnees_toutes_tables.nom_schema IS ''Nom du schéma qui heberge la table.'';
		COMMENT ON COLUMN  public._metadonnees_toutes_tables.nom_table IS ''Nom de la table.'';
		COMMENT ON COLUMN  public._metadonnees_toutes_tables.description_table IS ''Commentaire permettant la description de la table.'';	
		COMMENT ON COLUMN  public._metadonnees_toutes_tables.a_contacter IS ''Référent de la table à contacter pour toute utilisation.'';
		COMMENT ON COLUMN  public._metadonnees_toutes_tables.type_table IS ''TYPE de la table : table, paritionnée, étrangère, vue, matérialisée.'';

		 GRANT SELECT ON TABLE public._metadonnees_tout_attributs TO public;
		 COMMENT ON TABLE public._metadonnees_tout_attributs IS ''Vue des commentaires des attributs de toutes les tables de CeremaBase.'';
		 COMMENT ON COLUMN  public._metadonnees_tout_attributs.id IS ''Identifiant nécessaire à QGIS.'';
		 COMMENT ON COLUMN  public._metadonnees_tout_attributs.nom_table IS ''Nom de la table.'';
		 COMMENT ON COLUMN  public._metadonnees_tout_attributs.nom_champ IS ''Nom du champs d´attributs.'';
		 COMMENT ON COLUMN  public._metadonnees_tout_attributs.description_champ IS ''Commentaire permettant la description du champs d´attributs.'';
		 COMMENT ON COLUMN  public._metadonnees_tout_attributs.type_attribut IS ''Type du champs d´attributs.'';
		';
	RAISE NOTICE '%', req;
	EXECUTE(req);
---- B] Ajout automatique des deux vues de métadonnées dans tous les schéma présents 
FOR r IN SELECT DISTINCT schemaname from pg_tables WHERE
	(schemaname NOT LIKE 'pg_%' AND schemaname NOT LIKE 'public')
LOOP
---- B.1] Vue des métadonnées de tables
	req :='
		--DROP VIEW IF EXISTS ' || r.schemaname || '._metadonnees_tables;
		CREATE OR REPLACE VIEW ' || r.schemaname || '._metadonnees_tables AS 
		SELECT
				row_number() over ()::integer aS id,
		    	c.relname::varchar AS nom_table,
		    	obj_description(c.oid, ''pg_class''::name)::text AS description_table,
				CASE
					WHEN pg_get_userbyid(c.relowner) = ''postgres'' THEN ''ALD DterCE''
					ELSE pg_get_userbyid(c.relowner)
				END::varchar AS a_contacter,
				CASE
					WHEN c.relkind = ''r'' THEN ''table''
					WHEN c.relkind = ''v'' THEN ''vue''
					WHEN c.relkind = ''m'' THEN ''vue matérialisée''
					WHEN c.relkind = ''p'' THEN ''table paritionnée''
					WHEN c.relkind = ''f'' THEN ''table étrangère''
				END::varchar AS type_table    		
		   FROM pg_class c
		     LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
		  WHERE n.nspname ~~ ''' || r.schemaname || '''::TEXT AND c.relkind IN (''r'',''v'',''m'',''p'',''f'')
		  GROUP BY c.relowner, c.relkind, c.relname, c.oid
		  ORDER BY c.relname;
		';
	RAISE NOTICE '%', req;
	EXECUTE(req);
---- B.2] Vue des métadonnées d´attributs
	req :='
		--DROP VIEW IF EXISTS ' || r.schemaname || '._metadonnees_attributs;
		CREATE OR REPLACE VIEW ' || r.schemaname || '._metadonnees_attributs AS
		WITH mytables AS (
		         SELECT pg_tables.tablename,
		            pg_tables.schemaname
		           FROM pg_tables
		          WHERE (pg_tables.schemaname IN ( SELECT schemata.schema_name
		                   FROM information_schema.schemata
		                  WHERE schemata.schema_name = ''' || r.schemaname || '''))
				ORDER BY pg_tables.tablename
		        )
		 SELECT row_number() over ()::integer aS id,
		    mytables.tablename::varchar AS nom_table,
		    a.attname::varchar AS nom_champ,
		    d.description::varchar AS description_champ,
		    t.typname::varchar AS type_attribut
		   FROM mytables
		     JOIN pg_class c ON mytables.tablename = c.relname
		     JOIN pg_namespace n ON c.relnamespace = n.oid AND mytables.schemaname = n.nspname
		     JOIN pg_attribute a ON a.attrelid = c.oid
		     JOIN pg_description d ON d.objoid = c.oid AND d.objsubid = a.attnum
		     JOIN pg_type t ON t.oid = a.atttypid
		  ORDER BY mytables.tablename, a.attname;
		';
	RAISE NOTICE '%', req;
	EXECUTE(req);
---- B.3]  Ajout des commentaires et des droits
	req :='
		GRANT SELECT ON TABLE ' || r.schemaname || '._metadonnees_tables TO PUBLIC;
		COMMENT ON VIEW ' || r.schemaname || '._metadonnees_tables IS ''Vue des commentaires sur toutes les tables du schéma ' || r.schemaname || '.'';
		COMMENT ON COLUMN  ' || r.schemaname || '._metadonnees_tables.id IS ''Identifiant nécessaire à QGIS.'';
		COMMENT ON COLUMN  ' || r.schemaname || '._metadonnees_tables.nom_table IS ''Nom de la table.'';
		COMMENT ON COLUMN  ' || r.schemaname || '._metadonnees_tables.description_table IS ''Commentaire permettant la description de la table.'';		
		COMMENT ON COLUMN  ' || r.schemaname || '._metadonnees_tables.a_contacter IS ''Référent de la table à contacter pour toute utilisation.'';
		COMMENT ON COLUMN  ' || r.schemaname || '._metadonnees_tables.type_table IS ''TYPE de la table : table, paritionnée, étrangère, vue, matérialisée.'';

		GRANT SELECT ON TABLE ' || r.schemaname || '._metadonnees_attributs TO PUBLIC;
		COMMENT ON VIEW ' || r.schemaname || '._metadonnees_attributs IS ''Vue des commentaires des attributs de toutes les tables du schéma ' || r.schemaname || '.'';
		COMMENT ON COLUMN  ' || r.schemaname || '._metadonnees_attributs.id IS ''Identifiant nécessaire à QGIS.'';
		COMMENT ON COLUMN  ' || r.schemaname || '._metadonnees_attributs.nom_table IS ''Nom de la table.'';
		COMMENT ON COLUMN  ' || r.schemaname || '._metadonnees_attributs.nom_champ IS ''Nom du champs d´attributs.'';
		COMMENT ON COLUMN  ' || r.schemaname || '._metadonnees_attributs.description_champ IS ''Commentaire permettant la description du champs d´attributs.'';
		COMMENT ON COLUMN  ' || r.schemaname || '._metadonnees_attributs.type_attribut IS ''Type du champs d´attributs.'';
		';
	RAISE NOTICE '%', req;
	EXECUTE(req);
END LOOP;
END; 
$function$
;