CREATE OR REPLACE FUNCTION w_adl.set_tablespace_data(schema text)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
/*
[ADMIN] - D�place tous les index pr�sents dans un sch�ma en param�tre vers le tablespace data

Option :
--------
- nom du sch�ma

Tables concern�es :
-------------------
- toutes les tables du sch�ma pass� en param�tre

Taches r�alis�es :
------------------
- Scrute toutes les tables du sch�ma indiqu� en param�tre et leurs indexs dans une liste
- Pour tous les index de la liste leur affecte par d�faut le tablespace nomm� "data"
	- si d�j� le cas, passe au suivant sans perte de temps,
	- sinon le fait.

A am�liorer :
-------------
v�rifier l�existance du sch�ma

derni�re MAJ :
--------------
30 avril 2020
*/
DECLARE
  object text;		---- liste des tables pr�sentes dans le sch�ma
  req text;		---- requ�te � passer
BEGIN
    FOR object IN 
	SELECT pg_namespace.nspname||'.'||pg_class.relname  FROM pg_class, pg_namespace
	WHERE pg_namespace.nspname = schema AND pg_class.relnamespace  = pg_namespace.oid and pg_class.relkind = 'i' AND pg_class.relname
	not like 'pg_%'
    LOOP
		req := 'ALTER TABLE IF EXISTS ' || object || ' SET TABLESPACE data;';
		EXECUTE(req);
		RAISE NOTICE '%', req;
    END LOOP;
END;
$function$
;

COMMENT ON FUNCTION w_adl.set_tablespace_data(schema text) IS
'[ADMIN] - D�place tous les index pr�sents dans un sch�ma en param�tre vers le tablespace data

Option :
--------
- nom du sch�ma

Tables concern�es :
-------------------
- toutes les tables du sch�ma pass� en param�tre

Taches r�alis�es :
------------------
- Scrute toutes les tables du sch�ma indiqu� en param�tre et leurs indexs dans une liste
- Pour tous les index de la liste leur affecte par d�faut le tablespace nomm� "data"
	- si d�j� le cas, passe au suivant sans perte de temps,
	- sinon le fait.

A am�liorer :
-------------
v�rifier l�existance du sch�ma

derni�re MAJ :
--------------
30 avril 2020';