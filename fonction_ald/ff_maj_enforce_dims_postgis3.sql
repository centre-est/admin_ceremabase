---- Fontion ndims n'existe plus
---- Quelles sont les contraintes qui posent problème :
select
		pgc.conname as constraint_name,
       ccu.table_schema as table_schema,
       ccu.table_name,
       ccu.column_name,
       pgc.consrc as definition
from pg_constraint pgc
join pg_namespace nsp on nsp.oid = pgc.connamespace
join pg_class  cls on pgc.conrelid = cls.oid
left join information_schema.constraint_column_usage ccu
          on pgc.conname = ccu.constraint_name
          and nsp.nspname = ccu.constraint_schema
where contype ='c'
AND pgc.consrc like '%ndims%' and pgc.consrc not like '%st_ndims%'
order by ccu.table_schema, ccu.table_name;

---- Quelles sont les grands types automatisables :
select
       ccu.column_name,
       pgc.consrc as definition
from pg_constraint pgc
join pg_namespace nsp on nsp.oid = pgc.connamespace
join pg_class  cls on pgc.conrelid = cls.oid
left join information_schema.constraint_column_usage ccu
          on pgc.conname = ccu.constraint_name
          and nsp.nspname = ccu.constraint_schema
where contype ='c'
AND pgc.consrc like '%ndims%' and pgc.consrc not like '%st_ndims%'
group by ccu.column_name, pgc.consrc 
order by ccu.column_name, pgc.consrc;
/*
"geomloc"	"(public.ndims(geomloc) = 2)"
"geompar"	"(public.ndims(geompar) = 2)"
*/

---- Génération du script de mise à jour :
(
select distinct
		'ALTER TABLE ' || ccu.table_schema || '.' || ccu.table_name || ' DROP CONSTRAINT ' || pgc.conname || ';--' as requete
from pg_constraint pgc
join pg_namespace nsp on nsp.oid = pgc.connamespace
join pg_class  cls on pgc.conrelid = cls.oid
left join information_schema.constraint_column_usage ccu
          on pgc.conname = ccu.constraint_name
          and nsp.nspname = ccu.constraint_schema,
          count(*)
where contype ='c'
AND pgc.consrc = '(public.ndims(geomloc) = 2)'
order by requete
)
UNION ALL
(
select distinct
		'ALTER TABLE ' || ccu.table_schema || '.' || ccu.table_name || ' ADD CONSTRAINT ' || pgc.conname || ' CHECK (St_Ndims(geomloc) = 2);--' as requete
from pg_constraint pgc
join pg_namespace nsp on nsp.oid = pgc.connamespace
join pg_class  cls on pgc.conrelid = cls.oid
left join information_schema.constraint_column_usage ccu
          on pgc.conname = ccu.constraint_name
          and nsp.nspname = ccu.constraint_schema
where contype ='c'
AND pgc.consrc = '(public.ndims(geomloc) = 2)'
order by requete
)
union all 
(
select distinct
		'ALTER TABLE ' || ccu.table_schema || '.' || ccu.table_name || ' DROP CONSTRAINT ' || pgc.conname || ';--' as requete
from pg_constraint pgc
join pg_namespace nsp on nsp.oid = pgc.connamespace
join pg_class  cls on pgc.conrelid = cls.oid
left join information_schema.constraint_column_usage ccu
          on pgc.conname = ccu.constraint_name
          and nsp.nspname = ccu.constraint_schema
where contype ='c'
AND pgc.consrc = '(public.ndims(geompar) = 2)'
order by requete
)
UNION ALL
(
select distinct
		'ALTER TABLE ' || ccu.table_schema || '.' || ccu.table_name || ' ADD CONSTRAINT ' || pgc.conname || ' CHECK (St_Ndims(geompar) = 2);--' as requete
from pg_constraint pgc
join pg_namespace nsp on nsp.oid = pgc.connamespace
join pg_class  cls on pgc.conrelid = cls.oid
left join information_schema.constraint_column_usage ccu
          on pgc.conname = ccu.constraint_name
          and nsp.nspname = ccu.constraint_schema
where contype ='c'
AND pgc.consrc = '(public.ndims(geompar) = 2)'
order by requete
);
