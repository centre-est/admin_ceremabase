CREATE OR REPLACE FUNCTION w_adl.set_droits_public_referentiels()
 RETURNS void
 LANGUAGE plpgsql
AS $function$
 /*
[ADMIN] - Affecte les droits par d�faut � toutes les tables des sch�mas r�f�rentiels

Option :
--------
aucun

Tables concern�es :
-------------------
- toutes les tables des sch�mas commen�ant par 'r_'

Taches r�alis�es :
------------------
- Scrute toutes les sch�mas commen�ant par 'r_' et les place dans une liste de sch�ma
- Pour l'ensemble des sch�mas de cette liste
	- affecte un USAGE au r�le 'public'
	- affecte � toutes ses tables une possibilit� au r�le 'public'
	- rend le r�le 'postgres' proprit�taire du sch�ma
- Scrute toutes les tables des sch�mas commen�ant par 'r_' et donc le r�le 'public' n'est pas propri�taire et les place dans une liste de table
- Pour l'ensemble des tables de cette liste
		- rend le r�le 'postgres' proprit�taire de la table

A am�liorer :
-------------
v�rifier l�existance du sch�ma / travailler sur les vues, les scripts etc...

derni�re MAJ :
--------------
30 avril 2020
*/
DECLARE
r record;		---- liste des ressources inventori�es
req text;		---- requ�te � passer	
BEGIN 

FOR r IN SELECT DISTINCT schemaname from pg_tables where (schemaname LIKE 'r_%')
LOOP 
	req :='
		GRANT USAGE ON SCHEMA ' || r.schemaname || ' TO public;
		GRANT SELECT ON ALL TABLES IN SCHEMA ' || r.schemaname || ' TO public;
		ALTER SCHEMA ' || r.schemaname || ' OWNER TO postgres;
		';
	EXECUTE(req);
	RAISE NOTICE '%', req;
END LOOP;

FOR r IN SELECT schemaname, tablename FROM pg_tables WHERE schemaname LIKE 'r_%' AND tableowner NOT LIKE 'postgres' 
LOOP 
	req :='ALTER TABLE ' || r.schemaname || '.' || r.tablename || ' OWNER TO postgres;';
	EXECUTE(req);
	RAISE NOTICE '%', req;
END LOOP;

END; 
$function$
;

COMMENT ON FUNCTION w_adl.set_droits_public_referentiels() IS
'[ADMIN] - Affecte les droits par d�faut � toutes les tables des sch�mas r�f�rentiels

Option :
--------
aucun

Tables concern�es :
-------------------
- toutes les tables des sch�mas commen�ant par "r_"

Taches r�alis�es :
------------------
- Scrute toutes les sch�mas commen�ant par "r_" et les place dans une liste de sch�ma
- Pour l�ensemble des sch�mas de cette liste
	- affecte un USAGE au r�le "public"
	- affecte � toutes ses tables une possibilit� au r�le "public"
	- rend le r�le "postgres" proprit�taire du sch�ma
- Scrute toutes les tables des sch�mas commen�ant par "r_" et donc le r�le "public" n�est pas propri�taire et les place dans une liste de table
- Pour l�ensemble des tables de cette liste
		- rend le r�le "postgres" proprit�taire de la table

A am�liorer :
-------------
v�rifier l�existance du sch�ma / travailler sur les vues, les scripts etc...

derni�re MAJ :
--------------
30 avril 2020';