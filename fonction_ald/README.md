# admin_ceremabase

## Répertoire fonction_ald
Fonctions propres à l'administrateur de la base :
- a_adl_mensuel() : active les fonctions mensuelles (set_droits_public_referentiels / create_metadonnees)
- create_metadonnees() : Ajoute des vues de métadonnées de tables et d'attributs dans le schéma public et tous les schéma présents : 
- set_auto_tablespace_index : Affection systématique et automatique de tous les index dans CeremaBase au tablespace "index".
- set_droits_public_referentiels() : Affecte les droits par défaut à toutes les tables des schémas référentiels.
- set_tablespace_data(schema text) : Déplace tous les index présents dans un schéma en paramètre vers le tablespace data.
