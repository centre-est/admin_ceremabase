---- R�cup�r� ici : 
---- https://www.enterprisedb.com/postgres-tutorials/how-run-hierarchical-queries-oracle-and-postgresql

---- Table h�rarchique :
drop table if exists table_hierarchique;
create table table_hierarchique(  

  organisation_no int,  
  type_organisation char(15),  
  organisation    char(50),  
  no_orga_sup     int
);
insert into table_hierarchique values(10,'Internationale','ONU',null);
insert into table_hierarchique values(11, 'Internationale', 'UE', 10);
insert into table_hierarchique values(12, 'Internationale', 'ALENA', 10);
insert into table_hierarchique values(13, 'Internationale', 'COMEWEALTH', 10);
insert into table_hierarchique values(14, 'Pays', 'FRANCE', 11);
insert into table_hierarchique values(15, 'Pays', 'ALLEMAGNE',11);
insert into table_hierarchique values(16, 'R�gion', 'AUVERGNE-RHONE-ALPES', 14);
insert into table_hierarchique values(17 ,'R�gion', 'BADE-WUNTENBERG', 15);
insert into table_hierarchique values(18, 'D�partement', 'ISERE', 16);
insert into table_hierarchique values(19, 'D�partement', 'SAVOIE', 16);
insert into table_hierarchique values(20, 'EPCI', 'CAPI', 18);
insert into table_hierarchique values(21, 'Pays', 'USA', 12);
insert into table_hierarchique values(22, 'Pays', 'MEXIQUE', 12);
insert into table_hierarchique values(23, 'Etat', 'CALIFORNIE', 21);
insert into table_hierarchique values(24, 'Etat', 'NEW-YORK', 21);
insert into table_hierarchique values(25, 'EPCI', 'METROPOLE SAVOIE', 19);
insert into table_hierarchique values(26, 'EPCI', 'Vallons de la Tour', 18);
insert into table_hierarchique values(27, 'Commune', 'La-Tour-du-Pin', 26);
insert into table_hierarchique values(28, 'Commune', 'Saint-Clair-de-la-Tour', 26);
insert into table_hierarchique values(29, 'Commune', 'La-Chapelle-de-la-Tour', 26);
insert into table_hierarchique values(30, 'Hameau', 'Le Serpentin', 28);
insert into table_hierarchique values(31, 'Commune', 'Chamb�ry', 25);
insert into table_hierarchique values(32, 'Commune', 'Bassens', 25);
insert into table_hierarchique values(33, 'Commune', 'Cognin', 25);

---- Impossible sous PostgreSQL
SELECT  organisation_no, id_organisation, organisation,level
FROM table_hierarchique
CONNECT BY PRIOR organisation_no = no_orga_sup
START WITH no_orga_sup IS NULL
order by level ;

---- Deux alternatives :
---- Avec une CTE recursive :
WITH RECURSIVE cte AS (                                                                                                                                                          
SELECT organisation_no, organisation, type_organisation, no_orga_sup, 1 AS level
FROM   table_hierarchique                                                                                                                                                                     
where no_orga_sup is null                                                                                                                                                            
UNION  ALL                                                                                                                                                                               
SELECT e.organisation_no, e.organisation, e.type_organisation, e.no_orga_sup, c.level + 1  FROM  cte c                                                                                                                                                                            
JOIN   table_hierarchique e ON e.no_orga_sup = c.organisation_no                                                                                  
)                                                                                                                                                                                     
SELECT *                                                                                                                                                                                 
FROM cte;
/*
organisation_no|organisation                                      |type_organisation|no_orga_sup|level|
---------------|--------------------------------------------------|-----------------|-----------|-----|
             10|ONU                                               |Internationale   |           |    1|
             11|UE                                                |Internationale   |         10|    2|
             12|ALENA                                             |Internationale   |         10|    2|
             13|COMEWEALTH                                        |Internationale   |         10|    2|
             14|FRANCE                                            |Pays             |         11|    3|
             15|ALLEMAGNE                                         |Pays             |         11|    3|
             21|USA                                               |Pays             |         12|    3|
             22|MEXIQUE                                           |Pays             |         12|    3|
             16|AUVERGNE-RHONE-ALPES                              |R�gion           |         14|    4|
             17|BADE-WUNTENBERG                                   |R�gion           |         15|    4|
             23|CALIFORNIE                                        |Etat             |         21|    4|
             24|NEW-YORK                                          |Etat             |         21|    4|
             18|ISERE                                             |D�partement      |         16|    5|
             19|SAVOIE                                            |D�partement      |         16|    5|
             20|CAPI                                              |EPCI             |         18|    6|
             25|METROPOLE SAVOIE                                  |EPCI             |         19|    6|
             26|Vallons de la Tour                                |EPCI             |         18|    6|
             27|La-Tour-du-Pin                                    |Commune          |         26|    7|
             28|Saint-Clair-de-la-Tour                            |Commune          |         26|    7|
             29|La-Chapelle-de-la-Tour                            |Commune          |         26|    7|
             31|Chamb�ry                                          |Commune          |         25|    7|
             32|Bassens                                           |Commune          |         25|    7|
             33|Cognin                                            |Commune          |         25|    7|
             30|Le Serpentin                                      |Hameau           |         28|    8|
 */
---- Avec tablefunc
CREATE EXTENSION tablefunc;
SELECT * FROM connectby('table_hierarchique', 'organisation_no', 'no_orga_sup', '10', 0, '->') AS t(organisation_no int, no_orga_sup int, level int, ord text) order by organisation_no;
/*
organisation_no|no_orga_sup|level|ord                           |
---------------|-----------|-----|------------------------------|
             10|           |    0|10                            |
             11|         10|    1|10->11                        |
             12|         10|    1|10->12                        |
             13|         10|    1|10->13                        |
             14|         11|    2|10->11->14                    |
             15|         11|    2|10->11->15                    |
             16|         14|    3|10->11->14->16                |
             17|         15|    3|10->11->15->17                |
             18|         16|    4|10->11->14->16->18            |
             19|         16|    4|10->11->14->16->19            |
             20|         18|    5|10->11->14->16->18->20        |
             21|         12|    2|10->12->21                    |
             22|         12|    2|10->12->22                    |
             23|         21|    3|10->12->21->23                |
             24|         21|    3|10->12->21->24                |
             25|         19|    5|10->11->14->16->19->25        |
             26|         18|    5|10->11->14->16->18->26        |
             27|         26|    6|10->11->14->16->18->26->27    |
             28|         26|    6|10->11->14->16->18->26->28    |
             29|         26|    6|10->11->14->16->18->26->29    |
             30|         28|    7|10->11->14->16->18->26->28->30|
             31|         25|    6|10->11->14->16->19->25->31    |
             32|         25|    6|10->11->14->16->19->25->32    |
             33|         25|    6|10->11->14->16->19->25->33    |
 */

---- Plus lisible
with cte as (
			SELECT *
			FROM connectby('table_hierarchique', 'organisation_no', 'no_orga_sup', '10', 0, '->')
			AS t(organisation_no int, no_orga_sup int, level int, ord text) order by organisation_no
)
select t1.organisation_no, t2.organisation, t1.level, t2.type_organisation, t1.ord
FROM cte as t1
join table_hierarchique as t2
on t1.organisation_no = t2.organisation_no
order by level;
/*
organisation_no|organisation                                      |level|type_organisation|ord                           |
---------------|--------------------------------------------------|-----|-----------------|------------------------------|
             10|ONU                                               |    0|Internationale   |10                            |
             11|UE                                                |    1|Internationale   |10->11                        |
             12|ALENA                                             |    1|Internationale   |10->12                        |
             13|COMEWEALTH                                        |    1|Internationale   |10->13                        |
             22|MEXIQUE                                           |    2|Pays             |10->12->22                    |
             14|FRANCE                                            |    2|Pays             |10->11->14                    |
             15|ALLEMAGNE                                         |    2|Pays             |10->11->15                    |
             21|USA                                               |    2|Pays             |10->12->21                    |
             16|AUVERGNE-RHONE-ALPES                              |    3|R�gion           |10->11->14->16                |
             23|CALIFORNIE                                        |    3|Etat             |10->12->21->23                |
             17|BADE-WUNTENBERG                                   |    3|R�gion           |10->11->15->17                |
             24|NEW-YORK                                          |    3|Etat             |10->12->21->24                |
             18|ISERE                                             |    4|D�partement      |10->11->14->16->18            |
             19|SAVOIE                                            |    4|D�partement      |10->11->14->16->19            |
             20|CAPI                                              |    5|EPCI             |10->11->14->16->18->20        |
             26|Vallons de la Tour                                |    5|EPCI             |10->11->14->16->18->26        |
             25|METROPOLE SAVOIE                                  |    5|EPCI             |10->11->14->16->19->25        |
             33|Cognin                                            |    6|Commune          |10->11->14->16->19->25->33    |
             31|Chamb�ry                                          |    6|Commune          |10->11->14->16->19->25->31    |
             32|Bassens                                           |    6|Commune          |10->11->14->16->19->25->32    |
             29|La-Chapelle-de-la-Tour                            |    6|Commune          |10->11->14->16->18->26->29    |
             27|La-Tour-du-Pin                                    |    6|Commune          |10->11->14->16->18->26->27    |
             28|Saint-Clair-de-la-Tour                            |    6|Commune          |10->11->14->16->18->26->28    |
             30|Le Serpentin                                      |    7|Hameau           |10->11->14->16->18->26->28->30|
 */

---- Arbre diff�rent
with cte as (
			SELECT *
			FROM connectby('table_hierarchique', 'organisation_no', 'no_orga_sup', '10', 0, '->')
			AS t(organisation_no int, no_orga_sup int, level int, ord text) order by organisation_no
)
select t1.organisation_no, t2.organisation, t1.level, t2.type_organisation, t1.ord
FROM cte as t1
join table_hierarchique as t2
on t1.organisation_no = t2.organisation_no
order by ord;
/*
organisation_no|organisation                                      |level|type_organisation|ord                           |
---------------|--------------------------------------------------|-----|-----------------|------------------------------|
             10|ONU                                               |    0|Internationale   |10                            |
             11|UE                                                |    1|Internationale   |10->11                        |
             14|FRANCE                                            |    2|Pays             |10->11->14                    |
             16|AUVERGNE-RHONE-ALPES                              |    3|R�gion           |10->11->14->16                |
             18|ISERE                                             |    4|D�partement      |10->11->14->16->18            |
             20|CAPI                                              |    5|EPCI             |10->11->14->16->18->20        |
             26|Vallons de la Tour                                |    5|EPCI             |10->11->14->16->18->26        |
             27|La-Tour-du-Pin                                    |    6|Commune          |10->11->14->16->18->26->27    |
             28|Saint-Clair-de-la-Tour                            |    6|Commune          |10->11->14->16->18->26->28    |
             30|Le Serpentin                                      |    7|Hameau           |10->11->14->16->18->26->28->30|
             29|La-Chapelle-de-la-Tour                            |    6|Commune          |10->11->14->16->18->26->29    |
             19|SAVOIE                                            |    4|D�partement      |10->11->14->16->19            |
             25|METROPOLE SAVOIE                                  |    5|EPCI             |10->11->14->16->19->25        |
             31|Chamb�ry                                          |    6|Commune          |10->11->14->16->19->25->31    |
             32|Bassens                                           |    6|Commune          |10->11->14->16->19->25->32    |
             33|Cognin                                            |    6|Commune          |10->11->14->16->19->25->33    |
             15|ALLEMAGNE                                         |    2|Pays             |10->11->15                    |
             17|BADE-WUNTENBERG                                   |    3|R�gion           |10->11->15->17                |
             12|ALENA                                             |    1|Internationale   |10->12                        |
             21|USA                                               |    2|Pays             |10->12->21                    |
             23|CALIFORNIE                                        |    3|Etat             |10->12->21->23                |
             24|NEW-YORK                                          |    3|Etat             |10->12->21->24                |
             22|MEXIQUE                                           |    2|Pays             |10->12->22                    |
             13|COMEWEALTH                                        |    1|Internationale   |10->13                        |

