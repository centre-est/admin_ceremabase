#!/bin/bash 
#Sauvegarde du Vendredi
currentDate=`date +"%d_%m_20%y"`
echo '- pg_dumpall -'
fichier=$(pg_dumpall --port=5444 -v -g --file=/mnt/Save_base/03_sauve_vendredi/03_pg_dumpall_srv41_$currentDate.sql 1>/mnt/Save_base/00_log/03_pg_dumpall_srv41_$currentDate.log 2>&1)
echo '- sauve ceremabase -'
fichier=$(pg_dump -v -F d --port=5444 -j 10 --file=/mnt/Save_base/03_sauve_vendredi/03_pg_dump_ceremabase_$currentDate ceremabase 1>/mnt/Save_base/00_log/03_pg_dump_ceremabase_$currentDate.log 2>&1)
echo '- sauve diagnostic_envsante -'
fichier=$(pg_dump -v -F d --port=5445 -j 10 --file=/mnt/Save_base/03_sauve_vendredi/03_pg_dump_diagnostic_envsante_$currentDate diagnostic_envsante 1>/mnt/Save_base/00_log/03_pg_dump_diagnostic_envsante_$currentDate.log 2>&1)
echo '- sauve restore_plamade_2020 -'
fichier=$(pg_dump -v -F d --port=5445 -j 10 --file=/mnt/Save_base/03_sauve_vendredi/03_pg_dump_restore_plamade_2020_$currentDate restore_plamade_2020 1>/mnt/Save_base/00_log/03_pg_dump_restore_plamade_2020_$currentDate.log 2>&1)
echo '- sauve ceremabase_archives -'
fichier=$(pg_dump -v -F d --port=5445 -j 10 --file=/mnt/Save_base/03_sauve_vendredi/03_pg_dump_ceremabase_archives_$currentDate ceremabase_archives 1>/mnt/Save_base/00_log/03_pg_dump_ceremabase_archives_$currentDate.log 2>&1)
