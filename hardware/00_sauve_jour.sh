#!/bin/bash
#Sauvegarde Quotidienne
#On stocke la date de départ de la sauvegarde
#Sinon le repertoire de versement change à minuit et n'existe
currentDate=`date +"%d_%m_20%y"`
#DEBUG echo $currentDate
#Création du Répertoire ad'hoc
fichier=$(mkdir /mnt/Save_base/01_sauve_jour_$currentDate)
echo '--------------'
echo '- pg_dumpall'
fichier=$(pg_dumpall -p 5444 -v -g --file="/mnt/Save_base/01_sauve_jour_$currentDate/pg_dumpall_srv41_$currentDate.sql" 1>/mnt/Save_base/00_log/00_pg_dumpall_srv41_$currentDate.log 2>&1)
echo '-----------------------------------------'
echo '- Schéma Affaire / Production / Travail -'
#Generation de la liste des schémas
fichier=$(ogr2ogr -f CSV /mnt/Save_base/01_sauve_jour_$currentDate/liste_table.csv PG:"host='localhost' port=5444 dbname='ceremabase' user='administrateur' password='se6rg67x'" -sql "SELECT schema_name FROM information_schema.schemata WHERE schema_name ilike 'a\_%' or schema_name ilike 'p\_%' or schema_name ilike 'w\_%' ORDER BY schema_name;")
# Boucle sur la liste des schémas
for line in $(cat /mnt/Save_base/01_sauve_jour_$currentDate/liste_table.csv)
do 
echo '-----------------------------------------'
echo $line
fichier=$(pg_dump -v --format=t --port="5444" --schema="$line" ceremabase --file="/mnt/Save_base/01_sauve_jour_"$currentDate"/"$line"_"$currentDate".dump" 1>/mnt/Save_base/00_log/00_pg_dump_"$line"_"$currentDate".log 2>&1)
done
