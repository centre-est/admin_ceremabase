----=====================================================================================
---- 	VADE-MECUM SUR LES OPERATIONS TYPES DES ALD
----
----             CEREMABASE
----
---- 	Version 1 du 20/04/2020
----	c.badol
----	Finalisé oui| | / non |X|
----=====================================================================================
---- Rq : ---- pour les commentaires / -- pour les commandes optionnelles, debuger


-----------------------------------------
---- VI - Administration en nombre   ----
-----------------------------------------
---- Génére une liste de requêtes à exécuter

--- V.1 - Changement de schéma :
-------------------------------
SELECT 'ALTER TABLE ' || schemaname || '.' || tablename || ' SET SCHEMA schemaderrivee;' AS requete
FROM pg_tables WHERE schemaname NOT LIKE 'schemaderrivee' AND tablename LIKE ('n_terrain_sport_bdt%'); -- AND schemaname LIKE ('schemadedepart')

--- V.2 - Changement du propriétaire :
-------------------------------
SELECT 'ALTER TABLE ' || schemaname || '.' || tablename || ' OWNER TO admin_detc;' AS requete
FROM pg_tables WHERE schemaname LIKE 'bdtopo__a_voie_comm_route' AND NOT tableowner ='admin_detc';

--- V.3 - Mise en place des droits de toutes les tables d'un schéma :
---------------------------------------------------------------------
SELECT 'GRANT ALL ON TABLE ' || schemaname || '.' || tablename || ' TO admin_detc;' AS requete
FROM pg_tables WHERE schemaname LIKE 'bdtopo__%';

--- V.4 - Création d'un index géométrique pour les tables qui n'ont pas d'index dont le nom se termine par '_geom'
------------------------------------------------------------------------------------------------------------------
----- Quelles sont les tables sans index géométriques : sans nom de l'index se terminant par _gist
SELECT * FROM pg_indexes WHERE schemaname LIKE 'bdtopo__%' and indexname LIKE '%_gist'
ORDER by schemaname,tablename;
----- Mise à jour :
SELECT
	'CREATE INDEX '||tablename||'_geom_gist ON '|| schemaname || '.' || tablename || ' USING gist(geom) TABLESPACE index;' AS requete
	FROM pg_tables
	WHERE tablename NOT IN (
		SELECT tablename FROM pg_indexes WHERE schemaname LIKE 'bdtopo__%' and indexname LIKE '%_gist'
		)
		AND schemaname LIKE 'bdtopo__%'
ORDER BY schemaname,tablename;

--- V.5 - Ajouter les cluster pour les index géométriques qui ne le sont pas :
------------------------------------------------------------------------------
SELECT 'CLUSTER '||n.nspname||'.'||t.relname||' USING '||i.relname||';' as resquete
FROM
    pg_class t,
    pg_class i,
    pg_index ix,
    pg_namespace n,
    pg_attribute a
    
WHERE
    t.oid = ix.indrelid
    AND i.oid = ix.indexrelid
    AND a.attrelid = t.oid
    AND a.attnum = ANY(ix.indkey)
    AND t.relkind = 'r'
    AND n.oid = t.relnamespace
    AND n.nspname like 'bdtopo__%'
    AND (a.attname = 'geom' OR a.attname = 'the_geom')
    AND ix.indisclustered = false
ORDER BY
    t.relname,
    i.relname;


--- V.6 - Supprimer les sequences inutiles :
--------------------------------------------
SELECT
	'DROP SEQUENCE '||sequence_schema||'.'||sequence_name||' CASCADE;' AS requete
FROM information_schema.sequences
WHERE sequence_schema LIKE 'bdtopo__%';
