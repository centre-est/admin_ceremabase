DO $$
/*
[ADMIN - ADMIN_EXPRESS COG] - Mise en place des commentaires pour un millesime d´ADMIN EXPRESS® V3 Code Officiel Géographique de l´IGN selon le millesime et l´emprise :

Paramètres :
- nom du schéma où se trouvent les tables,
- emprise sur 3 caractères selon la COVADIS ddd : 
	- 'fra' : France Entière
	- '000' : France Métropolitaine
	- 'rrr' : Numéro INSEE de la Région : 'r84' pour Avergne-Rhône-Alpes
	- 'ddd' : Numéro INSEE du département : '038' pour l'Isère
	--> non pris en compte si COVADIS = false,
- millesime selon COVADIS : aaaa pour l'année du millesime ou null si pas de millesime
				non pris en compte si COVADIS = false,
- COVADIS : nommage des tble selon la COVADIS : oui : true / non : false,

Tables concernées :
n_adm_exp_cog_arrondissement_dpt_ddd_aaaa
n_adm_exp_cog_arrondissement_municipal_ddd_aaaa
n_adm_exp_cog_canton_ddd_aaaa
n_adm_exp_cog_chflieu_arrondissement_municipal_ddd_aaaa
n_adm_exp_cog_chflieu_commune_associee_ou_deleguee_ddd_aaaa
n_adm_exp_cog_chflieu_commune_ddd_aaaa
n_adm_exp_cog_collectivite_territoriale_ddd_aaaa
n_adm_exp_cog_commune_associee_ou_deleguee_ddd_aaaa
n_adm_exp_cog_commune_ddd_aaaa
n_adm_exp_cog_departement_ddd_aaaa
n_adm_exp_cog_epci_ddd_aaaa
n_adm_exp_cog_region_ddd_aaaa

amélioration à faire : aucune

dernière MAJ : 09/07/2021
*/

DECLARE
---- déclaration variables  --
req 						text;					-- Variable pour faire passer les requêtes
liste_valeur				character varying[][4];	-- Toutes les tables
nb_valeur					integer;				-- Nombre de tables --> normalement XX
nom_schema 					character varying;		-- Nom du schéma de travail
nom_table 					character varying;		-- Nom de la table en text
champs						character varying;		-- Nom du champs de la table en text;
commentaires 				character varying;		-- Texte du commentaire à inserer
millesime 					character varying(4);	-- Année du référentiel
emprise						character varying(3);	-- code COVADIS : ddd ou rrr ou 000 ou fra
covadis 					boolean;				-- Paramètre pour stocker le type de nommage

begin
---- PARAMETRES OBLIGATOIRES :
nom_schema := 'w_adl_delegue';
millesime := '2021';
emprise := 'fra';
covadis := 'true';

---- A] Commentaires des tables
---- Liste des valeurs à passer :
---- ARRAY['nom de la table telle que livrées par IGN', 'Commentaires à passer']
---- récupéré ici https://geoservices.ign.fr/ressources_documentaires/Espace_documentaire/BASES_VECTORIELLES/ADMIN_EXPRESS/DC_DL_ADMIN_EXPRESS_3-0.pdf
liste_valeur := ARRAY[
ARRAY['ARRONDISSEMENT','L’arrondissement est une subdivision du département. Depuis le redécoupage cantonal lié aux élections départementales  de  mars  2015,  l’arrondissement  n’est  plus  un  regroupement  de  cantons  mais  de  communes.'],
ARRAY['ARRONDISSEMENT_MUNICIPAL','Subdivision territoriale des communes de Lyon, Marseille et Paris. Il  ne  faut  pas  confondre  l’ARRONDISSEMENT_MUNICIPAL,  subdivision  de  Paris,  de  Lyon  et  de  Marseille, avec l’ARRONDISSEMENT, subdivision du département.'],
ARRAY['CANTON','Les  cantons  du  territoires’appuient  sur  les  cantons  au  sens  INSEE  (appelés  également pseudo-cantons) et non les cantons électoraux.'],
ARRAY['CHFLIEU_ARRONDISSEMENT_MUNICIPAL','Emplacement de la mairie de l’arrondissement municipal. Dans certains cas, le chef-lieu n’est pas dans l’arrondissement municipal.'],
ARRAY['CHFLIEU_COMMUNE','Centre de la zone d’habitat dans laquelle se trouve la mairie de la commune. Dans certains cas, le chef-lieu n’est pas dans la commune.'],
ARRAY['CHFLIEU_COMMUNE_ASSOCIEE_OU_DELEGUEE','Emplacement de la mairie de la commune associée ou déléguée. Dans certains cas, le chef-lieu n’est pas dans l’entité rattachée.'],
ARRAY['COLLECTIVITE_TERRITORIALE','Collectivité territoriale correspondant à l’échelon "départemental", comprenant les collectivités territoriales départementales, les collectivités territoriales uniques et les collectivités territoriales à statut particulier. Une collectivité territoriale est une personne morale de droit public qui exerce sur son territoire certaines compétences qui lui sont dévolues par l’État. Pour certains territoires, l’échelon régional et départemental sont regroupés : il s’agit des «collectivités uniques».'],
ARRAY['COMMUNE','Plus petite subdivision administrative du territoire, administrée par un maire, des adjoints et un conseil municipal.'],
ARRAY['COMMUNE_ASSOCIEE_OU_DELEGUEE','Ancienne commune ayant perdu son statut de collectivité territoriale en fusionnant avec d’autres communes, mais ayant gardé son territoire et certaines spécificités comme un maire délégué ou une mairie annexe.'],
ARRAY['DEPARTEMENT','Département au sens INSEE.'],
ARRAY['EPCI','Les établissements publics de coopération intercommunale (EPCI) sont des regroupements de communes ayant pour objet l’élaboration de « projets communs de développement au sein de périmètres de solidarité».  Ils sont soumis à des règles communes, homogènes et comparables à celles de collectivités locales(source INSEE).'],
ARRAY['REGION','Région au sens INSEE.']
];
nb_valeur := array_length(liste_valeur, 1);

FOR i_table IN 1..nb_valeur LOOP
---- Récupération des champs
---- Nom de la table
	select
		case
			when COVADIS is false then 
				lower(liste_valeur[i_table][1])
			else
				case
					when millesime is not null then
						'n_adm_exp_cog_' || lower(liste_valeur[i_table][1]) || '_' || emprise || '_' || millesime
					else
						'n_adm_exp_cog_' || lower(liste_valeur[i_table][1]) || '_' || emprise
				end
		end
		 into nom_table;
---- Nom du commentaire	
	SELECT liste_valeur[i_table][2] into commentaires;

---- Execution de la requete
	IF EXISTS (SELECT relname FROM pg_class where relname='' || nom_table ) then
		req := '
				COMMENT ON TABLE ' || nom_schema || '.' || nom_table || ' IS ''' || commentaires || ''';
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
	else
		req := '
				La table ' || nom_schema || '.' || nom_table || ' n´est pas présente.
				';
		RAISE NOTICE '%', req;
	END IF;

END LOOP;

---- B] Commentaires des attributs
---- Liste des valeurs à passer :
---- ARRAY['nom de la table telle que livrées par IGN','nom du champs dans la livraison shapefile', 'Commentaires à passer']
---- récupéré ici https://geoservices.ign.fr/ressources_documentaires/Espace_documentaire/BASES_VECTORIELLES/ADMIN_EXPRESS/DC_DL_ADMIN_EXPRESS_3-0.pdf
liste_valeur := ARRAY[
ARRAY['ARRONDISSEMENT','ID','Identifiant de l’arrondissment départemental.'],
ARRAY['ARRONDISSEMENT','INSEE_ARR','Code INSEE de l’arrondissment départemental.'],
ARRAY['ARRONDISSEMENT','INSEE_DEP','Code INSEE du département.'],	
ARRAY['ARRONDISSEMENT','INSEE_REG','Code INSEE de la région.'],
ARRAY['ARRONDISSEMENT','geom','Champs contenant la géométrie de l´objet (polygones).'],

ARRAY['ARRONDISSEMENT_MUNICIPAL','ID','Identifiant de l’arrondissement municipal.'],
ARRAY['ARRONDISSEMENT_MUNICIPAL','NOM','Nom de l’arrondissement municipal.'],
ARRAY['ARRONDISSEMENT_MUNICIPAL','NOM_M','Nom de l’arrondissement municipal en majuscules.'],
ARRAY['ARRONDISSEMENT_MUNICIPAL','INSEE_ARM','Code INSEE de l’arrondissement municipal.'],
ARRAY['ARRONDISSEMENT_MUNICIPAL','INSEE_COM','Code INSEE de la commune de rattachement.'],
ARRAY['ARRONDISSEMENT_MUNICIPAL','POPULATION','Population de l’arrondissement municipal.'],
ARRAY['ARRONDISSEMENT_MUNICIPAL','geom','Champs contenant la géométrie de l´objet (polygones).'],

ARRAY['CANTON','ID','Identifiant du canton.'],
ARRAY['CANTON','INSEE_CAN','Code INSEE du canton.'],
ARRAY['CANTON','INSEE_ARR','Code INSEE de l’arrondissement.'],
ARRAY['CANTON','INSEE_DEP','Code INSEE du département.'],
ARRAY['CANTON','INSEE_REG','Code INSEE de la région.'],
ARRAY['CANTON','geom','Champs contenant la géométrie de l´objet (polygones).'],

ARRAY['CHFLIEU_ARRONDISSEMENT_MUNICIPAL','ID','Identifiant du chef-lieu de l’arrondissement municipal.'],
ARRAY['CHFLIEU_ARRONDISSEMENT_MUNICIPAL','NOM','Nom du chef-lieu de l’arrondissement municipal.'],
ARRAY['CHFLIEU_ARRONDISSEMENT_MUNICIPAL','ID_COM','Identifiantde l’arrondissement municipal.'],
ARRAY['CHFLIEU_ARRONDISSEMENT_MUNICIPAL','geom','Champs contenant la géométrie de l´objet (points).'],

ARRAY['CHFLIEU_COMMUNE','ID','Identifiant du chef-lieu de commune.'],
ARRAY['CHFLIEU_COMMUNE','NOM','Nom du chef-lieu de commune.'],
ARRAY['CHFLIEU_COMMUNE','ID_COM','Identifiant de la commune.'],
ARRAY['CHFLIEU_COMMUNE','geom','Champs contenant la géométrie de l´objet (points).'],

ARRAY['CHFLIEU_COMMUNE_ASSOCIEE_OU_DELEGUEE','ID','Identifiant du chef-lieu de la commune associée ou déléguée.'],
ARRAY['CHFLIEU_COMMUNE_ASSOCIEE_OU_DELEGUEE','NOM','Nom du chef-lieu de la commune associée ou déléguée.'],
ARRAY['CHFLIEU_COMMUNE_ASSOCIEE_OU_DELEGUEE','ID_COM','Identifiant de la commune associée ou déléguée.'],
ARRAY['CHFLIEU_COMMUNE_ASSOCIEE_OU_DELEGUEE','geom','Champs contenant la géométrie de l´objet (points).'],

ARRAY['COLLECTIVITE_TERRITORIALE','ID','Identifiantde la collectivité territoriale.'],
ARRAY['COLLECTIVITE_TERRITORIALE','NOM','Nom de la collectivité territoriale.'],
ARRAY['COLLECTIVITE_TERRITORIALE','NOM_M','Nom de la collectivité territoriale en majuscules.'],
ARRAY['COLLECTIVITE_TERRITORIALE','INSEE_COL','Codification INSEE de la collectivité territoriale.'],
ARRAY['COLLECTIVITE_TERRITORIALE','geom','Champs contenant la géométrie de l´objet (polygones).'],

ARRAY['COMMUNE','ID','Identifiant de la commune.'],
ARRAY['COMMUNE','NOM','Nom de la commune.'],
ARRAY['COMMUNE','NOM_M','Nom de la commune en majuscules.'],
ARRAY['COMMUNE','INSEE_COM','Code INSEE de la commune.'],
ARRAY['COMMUNE','STATUT','Statut administratif.'],
ARRAY['COMMUNE','POPULATION','Population de la commune.'],
ARRAY['COMMUNE','INSEE_CAN','Code INSEE du canton.'],
ARRAY['COMMUNE','INSEE_ARR','Code INSEE de l’arrondissement.'],
ARRAY['COMMUNE','INSEE_DEP','Code INSEE du département.'],
ARRAY['COMMUNE','INSEE_REG','Code INSEE de la région.'],
ARRAY['COMMUNE','SIREN_EPCI','Code SIREN des EPCI.'],
ARRAY['COMMUNE','geom','Champs contenant la géométrie de l´objet (polygones).'],

ARRAY['COMMUNE_ASSOCIEE_OU_DELEGUEE','ID','Identifiant de la commune associée ou déléguée.'],
ARRAY['COMMUNE_ASSOCIEE_OU_DELEGUEE','NOM','Nom de la commune associée ou déléguée.'],
ARRAY['COMMUNE_ASSOCIEE_OU_DELEGUEE','NOM_M','Nom de la commune associée ou déléguée en majuscules.'],
ARRAY['COMMUNE_ASSOCIEE_OU_DELEGUEE','INSEE_CAD','Code INSEE de la commune associée ou déléguée.'],
ARRAY['COMMUNE_ASSOCIEE_OU_DELEGUEE','INSEE_COM','Code INSEE de la commune de rattachement.'],
ARRAY['COMMUNE_ASSOCIEE_OU_DELEGUEE','NATURE','Nature de la commune associée ou déléguée.'],
ARRAY['COMMUNE_ASSOCIEE_OU_DELEGUEE','POPULATION','Population de l’entité.'],
ARRAY['COMMUNE_ASSOCIEE_OU_DELEGUEE','geom','Champs contenant la géométrie de l´objet (polygones).'],

ARRAY['DEPARTEMENT','ID','Identifiant du département.'],
ARRAY['DEPARTEMENT','NOM_M','Nom du département en majuscules.'],
ARRAY['DEPARTEMENT','NOM','Nom du département.'],
ARRAY['DEPARTEMENT','INSEE_DEP','Code INSEE du département.'],
ARRAY['DEPARTEMENT','INSEE_REG','Code INSEE de la région.'],
ARRAY['DEPARTEMENT','geom','Champs contenant la géométrie de l´objet (polygones).'],

ARRAY['EPCI','ID','Identifiant de l’EPCI.'],
ARRAY['EPCI','CODE_SIREN','Code SIREN de l´entité administrative.'],
ARRAY['EPCI','NOM','Nom de l’EPCI.'],
ARRAY['EPCI','NATURE','Nature de l’EPCI.'],
ARRAY['EPCI','geom','Champs contenant la géométrie de l´objet (polygones).'],

ARRAY['REGION','ID','Identifiant de la région.'],
ARRAY['REGION','NOM_M','Nom de la région en majuscules.'],
ARRAY['REGION','NOM','Nom de la région.'],
ARRAY['REGION','INSEE_REG','Code INSEE de la région.'],
ARRAY['REGION','geom','Champs contenant la géométrie de l´objet (polygones).']

];
nb_valeur := array_length(liste_valeur, 1);

FOR i_table IN 1..nb_valeur LOOP
---- Récupération des champs
---- Nom de la table
	select
		case
			when COVADIS is false then 
				lower(liste_valeur[i_table][1])
			else
				case
					when millesime is not null then
						'n_adm_exp_cog_' || lower(liste_valeur[i_table][1]) || '_' || emprise || '_' || millesime
					else
						'n_adm_exp_cog_' || lower(liste_valeur[i_table][1]) || '_' || emprise
				end
		end
		 into nom_table;
---- Nom du champs à commenter		
	SELECT lower(liste_valeur[i_table][2])	into champs;

---- Nom du commentaire	
	SELECT liste_valeur[i_table][3] into commentaires;
/*-- debug
RAISE NOTICE '%', i_table;
RAISE NOTICE '%', nom_table;
RAISE NOTICE '%', champs;
RAISE NOTICE '%', commentaires;
-- debug */

---- Execution de la requete
	IF EXISTS (SELECT relname FROM pg_class where relname='' || nom_table ) then
		req := '
				COMMENT ON COLUMN ' || nom_schema || '.' || nom_table || '.' || champs || ' IS ''' || commentaires || ''';
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
	else
		req := '
				La table ' || nom_schema || '.' || nom_table || ' n´est pas présente pour le champs ' || champs || '.
				';
		RAISE NOTICE '%', req;
	END IF;

END LOOP; 

END $$;
