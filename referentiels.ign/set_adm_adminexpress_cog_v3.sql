---- A.2] Administration dans PostGIS :
CREATE OR REPLACE FUNCTION r_admin_express.set_adm_adminexpress_cog(nom_schema character varying, emprise character, millesime character)
 RETURNS text
 LANGUAGE plpgsql
AS $function$/*
[ADMIN - ADMIN_EXPRESS COG] - Mise en place des taches dadministration pour un millesime dADMIN EXPRESS V3 Code Officiel Gographique de lIGN selon le millesime et lemprise :

Taches ralises :
A. Re-nommage des tables selon COVADIS

B. Optimisation de base sur lensemble des fichiers
B.1 Vérification du nom du champs géomtérie
B.2 Suppression des champs inutiles
B.3 Correction des erreurs sur la géomtérie
B.4 Contraintes géomtériques de la table
B.5 Ajout des index spatiaux et cluster
B.6 Ajout des index attributaires non existants
B.7 Clés primaires sur le champs id

Tables concernées :
n_adm_exp_cog_arrondissement_ddd_aaaa
n_adm_exp_cog_arrondissement_municipal_ddd_aaaa
n_adm_exp_cog_canton_ddd_aaaa
n_adm_exp_cog_chflieu_arrondissement_municipal_ddd_aaaa
n_adm_exp_cog_chflieu_commune_associee_ou_deleguee_ddd_aaaa
n_adm_exp_cog_chflieu_commune_ddd_aaaa
n_adm_exp_cog_collectivite_territoriale_ddd_aaaa
n_adm_exp_cog_commune_associee_ou_deleguee_ddd_aaaa
n_adm_exp_cog_commune_ddd_aaaa
n_adm_exp_cog_departement_ddd_aaaa
n_adm_exp_cog_epci_ddd_aaaa
n_adm_exp_cog_region_ddd_aaaa

dernire MAJ : 09/07/2021
*/
DECLARE
---- dclaration variables  --

object 		text; 			-- Liste des objets pour executer une boucle
req		text;			-- requte  passer	
attribut 	text; 			-- Liste des attributs de la table


BEGIN

---- A - Re-nommage des tables :
---- A.1 - Nom non conforme :
req :='
	ALTER TABLE IF EXISTS ' || nom_schema || '."ARRONDISSEMENT" RENAME TO n_adm_exp_cog_arrondissement_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '.arrondissement RENAME TO n_adm_exp_cog_arrondissement_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."ARRONDISSEMENT_MUNICIPAL" RENAME TO n_adm_exp_cog_arrondissement_municipal_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '.arrondissement_municipal RENAME TO n_adm_exp_cog_arrondissement_municipal_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."CANTON" RENAME TO n_adm_exp_cog_canton_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '.canton RENAME TO n_adm_exp_cog_canton_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."CHFLIEU_ARRONDISSEMENT_MUNICIPAL" RENAME TO n_adm_exp_cog_chflieu_arrondissement_municipal_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."CHFLIEU_COMMUNE" RENAME TO n_adm_exp_cog_chflieu_commune_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."CHFLIEU_COMMUNE_ASSOCIEE_OU_DELEGUEE" RENAME TO n_adm_exp_cog_chflieu_commune_associee_ou_deleguee_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."COMMUNE_ASSOCIEE_OU_DELEGUEE" RENAME TO n_adm_exp_cog_commune_associee_ou_deleguee_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."COMMUNE" RENAME TO n_adm_exp_cog_commune_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."COLLECTIVITE_TERRITORIALE" RENAME TO n_adm_exp_cog_collectivite_territoriale_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '.commune RENAME TO n_adm_exp_cog_commune_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."DEPARTEMENT" RENAME TO n_adm_exp_cog_departement_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '.departement RENAME TO n_adm_exp_cog_departement_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."EPCI" RENAME TO n_adm_exp_cog_epci_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '.epci RENAME TO n_adm_exp_cog_epci_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."REGION" RENAME TO n_adm_exp_cog_region_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '.region RENAME TO n_adm_exp_cog_region_'  || emprise || '_'  || millesime  || ';
';
RAISE NOTICE '%', req;
EXECUTE(req);



---- B. Optimisation de base sur l'ensemble des fichiers
FOR object IN 
SELECT tablename::text from pg_tables where (schemaname LIKE nom_schema) AND left(tablename,14) = 'n_adm_exp_cog_' AND right(tablename,9) = '_' || emprise || '_' || millesime
LOOP

---- B.1 Vrification du nom du champs gomtrie si un seul champs gomtrique dans la table
	SELECT f_geometry_column FROM public.geometry_columns WHERE f_table_schema =nom_schema AND f_table_name = object AND (
		select count(f_geometry_column) FROM public.geometry_columns WHERE f_table_schema =nom_schema AND f_table_name = object
		) = 1
	INTO attribut;
		IF attribut = 'geom'
		THEN
			req := '
				La table ' || nom_schema || '.' || object || '  un nom de gomtrie conforme
			';
		RAISE NOTICE '%', req;
		ELSE
			req :='
				ALTER TABLE ' || nom_schema || '.' || object || ' RENAME ' || attribut  || ' TO geom;
			 ';
			RAISE NOTICE '%', req;
			EXECUTE(req);
		END IF;

---- B.2 Suppression des champs inutiles et des squences correspondantes
---- champs gid (shp2pgsql) / champs ogc_fid (ogr2ogr) / id_0 (glisser/dplacer de QGIS)
	req := '
				ALTER TABLE ' || nom_schema || '.' || object || ' DROP COLUMN IF EXISTS gid;
				ALTER TABLE ' || nom_schema || '.' || object || ' DROP COLUMN IF EXISTS ogc_fid;
				ALTER TABLE ' || nom_schema || '.' || object || ' DROP COLUMN IF EXISTS id_0;
				';
	RAISE NOTICE '%', req;
	EXECUTE(req);

---- B.3 Correction des erreurs sur la gomtrie
---- selon cette mthode : http://www.geoinformations.developpement-durable.gouv.fr/verification-et-corrections-des-geometries-a3522.html
	req := '
				UPDATE ' || nom_schema || '.' || object || ' SET geom=
					CASE 
						WHEN GeometryType(geom) = ''POLYGON'' 		OR GeometryType(geom) = ''MULTIPOLYGON'' THEN
								ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
						WHEN GeometryType(geom) = ''LINESTRING'' 	OR GeometryType(geom) = ''MULTILINESTRING'' THEN
								ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
						WHEN GeometryType(geom) = ''POINT'' 		OR GeometryType(geom) = ''MULTIPOINT'' THEN
								ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
						ELSE ST_MakeValid(geom)
					END
				WHERE NOT ST_Isvalid(geom);
				';
	RAISE NOTICE '%', req;
	EXECUTE(req);

---- B.4 Contraintes gomtriques de la table
---- B.4.1 Ajout des contraintes sur le champs gomtrie: 
	req := '
				ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS enforce_dims_geom;
				ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_dims_geom CHECK (ST_NDims(geom)=2);
				ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS enforce_srid_geom;
				ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)=4326);
				';
	RAISE NOTICE '%', req;
	EXECUTE(req);
---- B.4.2 CHECK (geometrytype(geom) :
	SELECT type FROM public.geometry_columns WHERE f_table_schema = nom_schema AND f_table_name = object INTO attribut;
		IF 	attribut = 'POLYGON' 			THEN req := '
						ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''POLYGON''::text OR geom IS NULL);
					';
			ELSEIF attribut = 'MULTIPOLYGON' 	THEN req := '
						ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTIPOLYGON''::text OR geom IS NULL);
					';
			ELSEIF attribut = 'LINESTRING' 		THEN req := '
						ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''LINESTRING''::text OR geom IS NULL);
					';
			ELSEIF attribut = 'MULTILINESTRING' 	THEN req := '
						ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTILINESTRING''::text OR geom IS NULL);
					';
			ELSEIF attribut = 'POINT' 		THEN req := '
						ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''POINT''::text OR geom IS NULL);
					';
			ELSEIF attribut = 'MULTIPOINT' 		THEN req := '
						ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTIPOINT''::text OR geom IS NULL);
					';
			ELSE 						req := 'SELECT current_time;';
		END IF;
		RAISE NOTICE '%', req;
		EXECUTE(req);
	

---- B.5 Ajout des index spatiaux et cluster
	req := '
		DROP INDEX IF EXISTS ' || nom_schema || '.' || object || '_geom_gist;
		DROP INDEX IF EXISTS ' || nom_schema || '.' || substring(object from 15 for char_length(object)-23) || '_idx;
		CREATE INDEX ' || object || '_geom_gist ON ' || nom_schema || '.' || object || ' USING gist (geom) TABLESPACE index;
		ALTER TABLE ' || nom_schema || '.' || object || ' CLUSTER ON ' || object || '_geom_gist;
	';
	RAISE NOTICE '%', req;
	EXECUTE(req);

---- B.6 Ajout des index attributaires non existants
	FOR attribut IN
		SELECT COLUMN_NAME
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_NAME = object
			AND COLUMN_NAME != 'geom' AND COLUMN_NAME != 'the_geom'
	LOOP
			req := '
				DROP INDEX IF EXISTS ' || nom_schema || '.' || object || '_' || attribut || '_idx;
				CREATE INDEX ' || object || '_' || attribut || '_idx ON ' || nom_schema || '.' || object || ' USING btree (' || attribut || ') TABLESPACE index;
			';
			RAISE NOTICE '%', req;
			EXECUTE(req);
	END LOOP;

---- B.7 cls primaires sur le champs id
			
	req := '
			ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS ' || object || '_id_pkey;
			ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS ' || UPPER(substring(object from 15 for char_length(object)-23)) || '_pkey;
			ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS ' || LOWER(substring(object from 15 for char_length(object)-23)) || '_pkey;
			ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT ' || object || '_id_pkey PRIMARY KEY (id);
	';
	EXECUTE(req);
	RAISE NOTICE '%', req;

END LOOP;

RETURN current_time;
END;
$function$
;

ALTER FUNCTION r_admin_express.set_adm_adminexpress_COG(character varying, character, character)
  OWNER TO postgres;
COMMENT ON FUNCTION r_admin_express.set_adm_adminexpress_COG(character varying, character, character) IS
'[ADMIN - ADMIN_EXPRESS COG] - Mise en place des taches dadministration pour un millesime dADMIN EXPRESS V3 Code Officiel Gographique de lIGN selon le millesime et lemprise :

Taches ralises :
A. Re-nommage des tables selon COVADIS

B. Optimisation de base sur lensemble des fichiers
B.1 Vrification du nom du champs gomtrie
B.2 Suppression des champs inutiles
B.3 Correction des erreurs sur la gomtrie
B.4 Contraintes gomtriques de la table
B.5 Ajout des index spatiaux et cluster
B.6 Ajout des index attributaires non existants
B.7 cls primaires sur le champs id

tables concernées :
n_adm_exp_cog_arrondissement_ddd_aaaa
n_adm_exp_cog_arrondissement_municipal_ddd_aaaa
n_adm_exp_cog_canton_ddd_aaaa
n_adm_exp_cog_chflieu_arrondissement_municipal_ddd_aaaa
n_adm_exp_cog_chflieu_commune_associee_ou_deleguee_ddd_aaaa
n_adm_exp_cog_chflieu_commune_ddd_aaaa
n_adm_exp_cog_collectivite_territoriale_ddd_aaaa
n_adm_exp_cog_commune_associee_ou_deleguee_ddd_aaaa
n_adm_exp_cog_commune_ddd_aaaa
n_adm_exp_cog_departement_ddd_aaaa
n_adm_exp_cog_epci_ddd_aaaa
n_adm_exp_cog_region_ddd_aaaa

dernire MAJ : 09/07/2021';
