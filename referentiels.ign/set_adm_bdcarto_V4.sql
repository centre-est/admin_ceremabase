CREATE OR REPLACE FUNCTION w_adl_delegue.set_adm_bdcarto_v4(emprise character varying, millesime character varying, projection integer DEFAULT 2154)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/*
[ADMIN - BDCARTO V4] - Administration d´un millésime de la BDCARTO© V4 issue du ShapeFile; une fois son import réalisé

Taches réalisées :
---- C. Optimisation de toutes les tables
---- C.1 Suppression du champs gid créée et de la séquence correspondante
---- C.2 Vérification du nom du champs géométrique
---- C.3 Correction des erreurs sur la géométrie
---- C.4 Ajout des contraintes
---- C.4.1 Ajout des contraintes sur le champs géométrie
---- C.4.2 CHECK (geometrytype(geom)
---- C.5 Ajout de la clef primaire
---- C.5.1 Suppression de l´ancienne si existante
---- C.5.1 Création de la clé primaire selon IGN
---- C.6 Ajout des index spatiaux
---- C.7 Ajout des index attributaires non existants

---- Les commentaires sont renvoyés à une autre fonction

Tables concernées :
					'acces_equipement',
					'aerodrome',
					'arrondissement_departemental',
					'cimetiere',
					'commune',
					'communication_restreinte',
					'construction_elevee',
					'departement',
					'digue',
					'enceinte_militaire',
					'equipement_routier',
					'etablissement',
					'franchissement',
					'itineraire',
					'laisse',
					'liaison_maritime',
					'lien_franchissement',
					'ligne_electrique',
					'limite_administrative',
					'massif_boise',
					'metro_aerien',
					'noeud_ferre',
					'noeud_routier',
					'piste_aerodrome',
					'point_remarquable_relief',
					'ponctuel_hydrographique',
					'region',
					'surface_hydrographique',
					'transport_cable',
					'troncon_hydrographique',
					'troncon_route',
					'troncon_voie_ferree',
					'zone_activite',
					'zone_habitat_mairie',
					'zone_hydrographique_texture',
					'zone_occupation_sol',
					'zone_reglementee_touristique'

amélioration à faire :
---- A Create Schema : vérification que le schéma n'existe pas et le créer
---- C.5.1 Ajout de la clef primaire sauf si doublon d?identifiant notamment n_troncon_cours_eau_bdt

dernière MAJ : 05/12/2021
*/

declare
nom_schema 			character varying;		-- Schéma du référentiel en text
nom_table 			character varying;		-- nom de la table en text
referentiel			character varying;		-- Nom du référentiel tel qu'il apparait dans le schema
covadis				character varying;		-- Tri-gramme du référentiel pour un nommage COVADIS
req 				text;
veriftable 			character varying;
tb_toutestables		character varying[];	-- Toutes les tables
nb_toutestables 	integer;				-- Nombre de tables --> normalement XX
attribut 			character varying; 		-- Liste des attributs de la table
typegeometrie 		text; 					-- "GeometryType" de la table

BEGIN
referentiel := 'bdcarto';
covadis 	:= 'bdc';

nom_schema 	:= 'r_' || referentiel || '_' || millesime;


---- Référencement des tables à traiter
--DEBUG tb_toutestables := array['erp'];
tb_toutestables := array[
					'acces_equipement',
					'aerodrome',
					'arrondissement_departemental',
					'cimetiere',
					'commune',
					'communication_restreinte',
					'construction_elevee',
					'departement',
					'digue',
					'enceinte_militaire',
					'equipement_routier',
					'etablissement',
					'franchissement',
					'itineraire',
					'laisse',
					'liaison_maritime',
					'lien_franchissement',
					'ligne_electrique',
					'limite_administrative',
					'massif_boise',
					'metro_aerien',
					'noeud_ferre',
					'noeud_routier',
					'piste_aerodrome',
					'point_remarquable_relief',
					'ponctuel_hydrographique',
					'region',
					'surface_hydrographique',
					'transport_cable',
					'troncon_hydrographique',
					'troncon_route',
					'troncon_voie_ferree',
					'zone_activite',
					'zone_habitat_mairie',
					'zone_hydrographique_texture',
					'zone_occupation_sol',
					'zone_reglementee_touristique'
		];
nb_toutestables := array_length(tb_toutestables, 1);

---- C. Optimisation de toutes les tables
FOR i_table IN 1..nb_toutestables loop

	nom_table:='n_' || tb_toutestables[i_table] || '_' || covadis|| '_' || emprise || '_' || millesime;
	
	SELECT tablename FROM pg_tables WHERE schemaname = nom_schema AND tablename = nom_table INTO veriftable;
	IF LEFT(veriftable,length (nom_table)) = nom_table
	then
---- C.1 Suppression du champs gid créée et de la séquence correspondante
	req := '
				ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP COLUMN IF EXISTS gid;
				ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP COLUMN IF EXISTS ogc_fid;
				ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP COLUMN IF EXISTS id_0;
		';
		RAISE NOTICE '%', req;
		EXECUTE(req);

---- C.2 Vérification du nom du champs géométrique
		SELECT f_geometry_column FROM public.geometry_columns WHERE f_table_schema = nom_schema AND f_table_name = nom_table AND (
		select count(f_geometry_column) FROM public.geometry_columns WHERE f_table_schema = nom_schema AND f_table_name = nom_table
		) = 1
		INTO attribut;
		IF attribut = 'geom'
		THEN
			req := '
				La table ' || nom_schema || '.' || nom_table || ' à un nom de géométrie conforme
			';
		RAISE NOTICE '%', req;
		ELSE
			req :='
				ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' RENAME ' || attribut  || ' TO geom;
			 ';
			RAISE NOTICE '%', req;
			EXECUTE(req);
		END IF;
---- C.3 Correction des erreurs sur la géométrie
---- selon cette méthode : http://www.geoinformations.developpement-durable.gouv.fr/verification-et-corrections-des-geometries-a3522.html
	req := '
				UPDATE ' || nom_schema || '.' || nom_table || ' SET geom=
					CASE 
						WHEN GeometryType(geom) = ''POLYGON'' 		OR GeometryType(geom) = ''MULTIPOLYGON'' THEN
								ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
						WHEN GeometryType(geom) = ''LINESTRING'' 	OR GeometryType(geom) = ''MULTILINESTRING'' THEN
								ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
						WHEN GeometryType(geom) = ''POINT'' 		OR GeometryType(geom) = ''MULTIPOINT'' THEN
								ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
						ELSE ST_MakeValid(geom)
					END
				WHERE NOT ST_Isvalid(geom);
				';
	RAISE NOTICE '%', req;
	EXECUTE(req);
---- C.4 Ajout des contraintes
---- C.4.1 Ajout des contraintes sur le champs géométrie
	req := '
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_dims_geom;
		--ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_dims_geom CHECK (ST_NDims(geom)=2);
		--ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_srid_geom;
		--ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)=' || projection || ');
		--ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_no_self_intersection_geom;
		--ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_no_self_intersection_geom CHECK (ST_IsSimple(geom));
	';
	RAISE NOTICE '%', req;
	EXECUTE(req);
---- C.4.2 CHECK (geometrytype(geom)
---- C.4.2.1 Création de la table pour lister les géométries disponible
	req := '
		CREATE TABLE public.a_supprimer AS (
			SELECT GeometryType(geom) AS geomtype
			FROM ' || nom_schema || '.' || nom_table || ' group by geomtype
		);
	';
	RAISE NOTICE '%', req;
	EXECUTE(req);

---- C.4.2.2 CHECK (geometrytype(geom)
	SELECT type FROM public.geometry_columns WHERE f_table_schema = nom_schema AND f_table_name = nom_table INTO attribut;
		IF attribut = 'GEOMETRY' then SELECT geomtype FROM public.a_supprimer LIMIT 1 INTO typegeometrie;
					IF 	typegeometrie = 'POLYGON' 			THEN req := '
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''POLYGON''::text);-- OR geom IS NULL);
							';
					ELSEIF typegeometrie = 'MULTIPOLYGON' 	THEN req := '
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTIPOLYGON''::text);-- OR geom IS NULL);
							';
					ELSEIF typegeometrie = 'LINESTRING' 		THEN req := '
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''LINESTRING''::text);-- OR geom IS NULL);
							';
					ELSEIF typegeometrie = 'MULTILINESTRING' 	THEN req := '
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTILINESTRING''::text);-- OR geom IS NULL);
							';
					ELSEIF typegeometrie = 'POINT' 		THEN req := '
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''POINT''::text);-- OR geom IS NULL);
							';
					ELSEIF typegeometrie = 'MULTIPOINT' 		THEN req := '
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTIPOINT''::text);-- OR geom IS NULL);
							';
					else req := 'La valeur attribut est <<' || attribut || '>> et la valeur typegeometrie est <<' || typegeometrie || '>> ';
				END IF;	
			ELSIF attribut = 'POLYGON' 			THEN req := '
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''POLYGON''::text);-- OR geom IS NULL);
					';
			ELSIF attribut = 'MULTIPOLYGON' 	THEN req := '
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTIPOLYGON''::text);-- OR geom IS NULL);
					';
			ELSIF attribut = 'LINESTRING' 		THEN req := '
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''LINESTRING''::text);-- OR geom IS NULL);
					';
			ELSIF attribut = 'MULTILINESTRING' 	THEN req := '
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTILINESTRING''::text);-- OR geom IS NULL);
					';
			ELSIF attribut = 'POINT' 		THEN req := '
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''POINT''::text);-- OR geom IS NULL);
					';
			ELSIF attribut = 'MULTIPOINT' 		THEN req := '
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTIPOINT''::text);-- OR geom IS NULL);
					';
		else req := 'La valeur attribut est <<' || attribut || '>> ';
		END IF;
		RAISE NOTICE '%', req;
		EXECUTE(req);
---- C.4.2.3
		req := '
			DROP TABLE public.a_supprimer;
		';
		RAISE NOTICE '%', req;
		EXECUTE(req);

	---- C.5 Ajout de la clef primaire
---- C.5.1 Suppression de l'ancienne si existante
		select t1.conname from pg_constraint as t1, pg_class as t2
		where t2.relname = nom_table and t1.contype = 'p' and t1.conrelid = t2.oid
		into attribut;
		IF attribut is NULL
		THEN
			req := '
				La table ' || nom_schema || '.' || nom_table || ' n´a pas de clé primaire.
			';
		RAISE NOTICE '%', req;
		ELSE
			req :='
				ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS ' || attribut  || ';
			 ';
			RAISE NOTICE '%', req;
			EXECUTE(req);

		END IF;

---- C.5.1 Création de la clé primaire selon IGN
			select left(nom_table,12) into attribut;
			IF attribut != 'n_toponymie_'
			then
				req := '
					ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT ' || nom_table || '_pkey PRIMARY KEY (id);
				';		
			else
				req := '
					ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT ' || nom_table || '_pkey PRIMARY KEY (cleabs);
					select current_time;
				';					
			end if;
			--RAISE NOTICE '%', req;
			--EXECUTE(req);

---- C.6 Ajout des index spatiaux
			req := '
				DROP INDEX IF EXISTS ' || nom_schema || '.' || nom_table || '_geom_gist;
				CREATE INDEX ' || nom_table || '_geom_gist ON ' || nom_schema || '.' || nom_table || ' USING gist (geom) TABLESPACE index;
        		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' CLUSTER ON ' || nom_table || '_geom_gist;
			';
			RAISE NOTICE '%', req;
			EXECUTE(req);

		---- C.7 Ajout des index attributaires non existants
			FOR attribut IN
				SELECT COLUMN_NAME
					FROM INFORMATION_SCHEMA.COLUMNS
					WHERE TABLE_NAME = nom_table AND TABLE_SCHEMA = nom_schema
					AND COLUMN_NAME != 'geom' AND COLUMN_NAME != 'the_geom'
			LOOP
					req := '
						DROP INDEX IF EXISTS ' || nom_schema || '.' || nom_table || '_' || attribut || '_idx;
						CREATE INDEX ' || nom_table || '_' || attribut || '_idx ON ' || nom_schema || '.' || nom_table || ' USING btree (' || attribut || ') TABLESPACE index;
					';
					RAISE NOTICE '%', req;
					EXECUTE(req);
			END LOOP;				

---- C.99 Fin de la boucle
	ELSE
	req :='La table ' || nom_schema || '.' || nom_table || ' n est pas présente';
	RAISE NOTICE '%', req;

	END IF;
END LOOP; 	

RETURN current_time;
END; 
$function$
;

COMMENT ON FUNCTION w_adl_delegue.set_adm_bdcarto_v4(varchar,varchar,int4) IS '[ADMIN - BDCARTO V4] - Administration d´un millésime de la BDCARTO© V4 issue du ShapeFile; une fois son import réalisé

Taches réalisées :
---- C. Optimisation de toutes les tables
---- C.1 Suppression du champs gid créée et de la séquence correspondante
---- C.2 Vérification du nom du champs géométrique
---- C.3 Correction des erreurs sur la géométrie
---- C.4 Ajout des contraintes
---- C.4.1 Ajout des contraintes sur le champs géométrie
---- C.4.2 CHECK (geometrytype(geom)
---- C.5 Ajout de la clef primaire
---- C.5.1 Suppression de l´ancienne si existante
---- C.5.1 Création de la clé primaire selon IGN
---- C.6 Ajout des index spatiaux
---- C.7 Ajout des index attributaires non existants

---- Les commentaires sont renvoyés à une autre fonction

Tables concernées :
					''acces_equipement'',
					''aerodrome'',
					''arrondissement_departemental'',
					''cimetiere'',
					''commune'',
					''communication_restreinte'',
					''construction_elevee'',
					''departement'',
					''digue'',
					''enceinte_militaire'',
					''equipement_routier'',
					''etablissement'',
					''franchissement'',
					''itineraire'',
					''laisse'',
					''liaison_maritime'',
					''lien_franchissement'',
					''ligne_electrique'',
					''limite_administrative'',
					''massif_boise'',
					''metro_aerien'',
					''noeud_ferre'',
					''noeud_routier'',
					''piste_aerodrome'',
					''point_remarquable_relief'',
					''ponctuel_hydrographique'',
					''region'',
					''surface_hydrographique'',
					''transport_cable'',
					''troncon_hydrographique'',
					''troncon_route'',
					''troncon_voie_ferree'',
					''zone_activite'',
					''zone_habitat_mairie'',
					''zone_hydrographique_texture'',
					''zone_occupation_sol'',
					''zone_reglementee_touristique''

amélioration à faire :
---- A Create Schema : vérification que le schéma n''existe pas et le créer
---- C.5.1 Ajout de la clef primaire sauf si doublon d?identifiant notamment n_troncon_cours_eau_bdt

dernière MAJ : 05/12/2021';
