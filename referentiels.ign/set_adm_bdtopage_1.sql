CREATE OR REPLACE FUNCTION r_bdtopage.set_adm_bdtopage_1(nom_schema character varying, emprise character, millesime character)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/*
[ADMIN - BDTOPAGE] - Mise en place des taches d�administration pour un millesime de la BDTOPAGE� du Sandre et de l�IGN selon le millesime et l�emprise :

Taches r�alis�es :
A - Re-nommage des tables

B. Optimisation de base sur l�ensemble des fichiers
B.1 V�rification du nom du champs g�om�trie
B.2 Suppression des champs inutiles
B.3 Correction des erreurs sur la g�om�trie
B.4 Contraintes g�om�triques de la table
B.5 Ajout des index spatiaux et cluster
B.6 Ajout des index attributaires non existants
B.7 cl�s primaires sur le champs id

C. Travail � la Table
#AFAIRE
C.1 n_topage_bassin_hydro_ddd_aaaa
C.2 n_topage_bassin_versant_topo_ddd_aaaa
C.3 n_topage_cours_eau_ddd_aaaa
C.4 n_topage_limite_terre_mer_ddd_aaaa
C.5 n_topage_noeud_hydro_ddd_aaaa
C.6 n_topage_plan_eau_ddd_aaaa
C.7 n_topage_surface_elementaire_ddd_aaaa
C.8 n_topage_troncon_hydro_ddd_aaaa


Tables concern�es :
n_topage_bassin_hydro_ddd_aaaa
n_topage_bassin_versant_topo_ddd_aaaa
n_topage_cours_eau_ddd_aaaa
n_topage_limite_terre_mer_ddd_aaaa
n_topage_noeud_hydro_ddd_aaaa
n_topage_plan_eau_ddd_aaaa
n_topage_surface_elementaire_ddd_aaaa
n_topage_troncon_hydro_ddd_aaaa

derni�re MAJ : 18/03/2021
*/
DECLARE
---- d�claration variables  --

object 		text; 			-- Liste des objets pour executer une boucle
req		text;			-- requ�te � passer	
attribut 	text; 			-- Liste des attributs de la table


BEGIN

---- A - Re-nommage des tables :
---- A.1 - Nom non conforme :
req :='
	ALTER TABLE IF EXISTS ' || nom_schema || '."BassinHydrographique" RENAME TO n_topage_bassin_hydro_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."BassinVersantTopographique_FXX" RENAME TO n_topage_bassin_versant_topo_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."CoursEau_FXX" RENAME TO n_topage_cours_eau_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."LimiteTerreMer_FXX" RENAME TO n_topage_limite_terre_mer_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."NoeudHydrographique_FXX" RENAME TO n_topage_noeud_hydro_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."PlanEau_FXX" RENAME TO n_topage_plan_eau_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."SurfaceElementaire_FXX" RENAME TO n_topage_surface_elementaire_'  || emprise || '_'  || millesime  || ';
	ALTER TABLE IF EXISTS ' || nom_schema || '."TronconHydrographique_FXX" RENAME TO n_topage_troncon_hydro_'  || emprise || '_'  || millesime  || ';
';
RAISE NOTICE '%', req;
EXECUTE(req);


---- B. Optimisation de base sur l'ensemble des fichiers
FOR object IN 
SELECT tablename::text from pg_tables where (schemaname LIKE nom_schema) AND left(tablename,9) = 'n_topage_' AND right(tablename,9) = '_' || emprise || '_' || millesime
LOOP

---- B.1 V�rification du nom du champs g�om�trie si un seul champs g�om�trique dans la table
	SELECT f_geometry_column FROM public.geometry_columns WHERE f_table_schema =nom_schema AND f_table_name = object AND (
		select count(f_geometry_column) FROM public.geometry_columns WHERE f_table_schema =nom_schema AND f_table_name = object
		) = 1
	INTO attribut;
		IF attribut = 'geom'
		THEN
			req := '
				La table ' || nom_schema || '.' || object || ' � un nom de g�om�trie conforme
			';
		RAISE NOTICE '%', req;
		ELSE
			req :='
				ALTER TABLE ' || nom_schema || '.' || object || ' RENAME ' || attribut  || ' TO geom;
			 ';
			RAISE NOTICE '%', req;
			EXECUTE(req);
		END IF;

---- B.2 Suppression des champs inutiles et des s�quences correspondantes
---- champs gid (shp2pgsql) / champs ogc_fid (ogr2ogr) / id_0 (glisser/d�placer de QGIS)
	req := '
				ALTER TABLE ' || nom_schema || '.' || object || ' DROP COLUMN IF EXISTS gid;
				ALTER TABLE ' || nom_schema || '.' || object || ' DROP COLUMN IF EXISTS ogc_fid;
				ALTER TABLE ' || nom_schema || '.' || object || ' DROP COLUMN IF EXISTS id_0;
				';
	RAISE NOTICE '%', req;
	--EXECUTE(req);

---- B.3 Correction des erreurs sur la g�om�trie
---- selon cette m�thode : http://www.geoinformations.developpement-durable.gouv.fr/verification-et-corrections-des-geometries-a3522.html
	req := '
				UPDATE ' || nom_schema || '.' || object || ' SET geom=
					CASE 
						WHEN GeometryType(geom) = ''POLYGON'' 		OR GeometryType(geom) = ''MULTIPOLYGON'' THEN
								ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
						WHEN GeometryType(geom) = ''LINESTRING'' 	OR GeometryType(geom) = ''MULTILINESTRING'' THEN
								ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
						WHEN GeometryType(geom) = ''POINT'' 		OR GeometryType(geom) = ''MULTIPOINT'' THEN
								ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
						ELSE ST_MakeValid(geom)
					END
				WHERE NOT ST_Isvalid(geom);
				';
	RAISE NOTICE '%', req;
	EXECUTE(req);

---- B.4 Contraintes g�om�triques de la table
---- B.4.1 Ajout des contraintes sur le champs g�om�trie: 
	req := '
				ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS enforce_dims_geom;
				---- A Faire table par table : ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_dims_geom CHECK (ST_NDims(geom)=2);
				ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS enforce_srid_geom;
				ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)=2154);
				';
	RAISE NOTICE '%', req;
	EXECUTE(req);
---- B.4.2 CHECK (geometrytype(geom) :
	SELECT type FROM public.geometry_columns WHERE f_table_schema = nom_schema AND f_table_name = object INTO attribut;
		IF 	attribut = 'POLYGON' 			THEN req := '
						ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''POLYGON''::text OR geom IS NULL);
					';
			ELSEIF attribut = 'MULTIPOLYGON' 	THEN req := '
						ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTIPOLYGON''::text OR geom IS NULL);
					';
			ELSEIF attribut = 'LINESTRING' 		THEN req := '
						ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''LINESTRING''::text OR geom IS NULL);
					';
			ELSEIF attribut = 'MULTILINESTRING' 	THEN req := '
						ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTILINESTRING''::text OR geom IS NULL);
					';
			ELSEIF attribut = 'POINT' 		THEN req := '
						ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''POINT''::text OR geom IS NULL);
					';
			ELSEIF attribut = 'MULTIPOINT' 		THEN req := '
						ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTIPOINT''::text OR geom IS NULL);
					';
			ELSE 						req := 'SELECT current_time;';
		END IF;
		RAISE NOTICE '%', req;
		EXECUTE(req);
	

---- B.5 Ajout des index spatiaux et cluster
	req := '
		DROP INDEX IF EXISTS ' || nom_schema || '.' || object || '_geom_gist;
		CREATE INDEX ' || object || '_geom_gist ON ' || nom_schema || '.' || object || ' USING gist (geom) TABLESPACE index;
		ALTER TABLE ' || nom_schema || '.' || object || ' CLUSTER ON ' || object || '_geom_gist;
	';
	RAISE NOTICE '%', req;
	EXECUTE(req);

---- B.6 Ajout des index attributaires non existants
	FOR attribut IN
		SELECT COLUMN_NAME
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_NAME = object
			AND COLUMN_NAME != 'geom' AND COLUMN_NAME != 'the_geom'
	LOOP
			req := '
				DROP INDEX IF EXISTS ' || nom_schema || '.' || object || '_' || attribut || '_idx;
				CREATE INDEX ' || object || '_' || attribut || '_idx ON ' || nom_schema || '.' || object || ' USING btree (' || attribut || ') TABLESPACE index;
			';
			RAISE NOTICE '%', req;
			EXECUTE(req);
	END LOOP;

---- B.7 cl�s primaires sur le champs id
			
	req := '
			ALTER TABLE ' || nom_schema || '.' || object || ' DROP CONSTRAINT IF EXISTS ' || object || '_id_pkey;
			ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT ' || object || '_id_pkey PRIMARY KEY (id);
	';
	EXECUTE(req);
	RAISE NOTICE '%', req;

END LOOP;
----#AFAIRE
/*
---- C. Travail � la Table
----------------------------------------
---- C.1 n_adm_exp_arrondissement_dpt_ddd_aaaa
----------------------------------------
---- A Faire table par table : ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_dims_geom CHECK (ST_NDims(geom)=2);
---- M�tadonn�es :
req := '
-- Table
	COMMENT ON TABLE '||nom_schema||'.n_adm_exp_arrondissement_dpt_'|| emprise ||'_'|| millesime ||' IS ''Arrondissement d�partementaux de la base ADMIN Express de l�IGN de '|| millesime ||''';
-- Colonnes
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_arrondissement_dpt_'|| emprise ||'_'|| millesime ||'.id IS ''Identifiant unique de l�arrondissement'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_arrondissement_dpt_'|| emprise ||'_'|| millesime ||'.insee_arr IS ''Num�ro INSEE de l�arrondissement'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_arrondissement_dpt_'|| emprise ||'_'|| millesime ||'.insee_dep IS ''Num�ro INSEE du d�partement auquel appartient l�arrondissement'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_arrondissement_dpt_'|| emprise ||'_'|| millesime ||'.insee_reg IS ''Num�ro INSEE de la r�gion contenant l�arrondissement'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_arrondissement_dpt_'|| emprise ||'_'|| millesime ||'.geom IS ''Surfacique 2D'';
';
RAISE NOTICE '%', req;
EXECUTE(req);

----------------------------------------
---- C.2 n_adm_exp_chef_lieu_ddd_aaaa
----------------------------------------
---- A Faire table par table : ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_dims_geom CHECK (ST_NDims(geom)=2);
---- M�tadonn�es :
req := '
-- Table
	COMMENT ON TABLE '||nom_schema||'.n_adm_exp_chef_lieu_'|| emprise ||'_'|| millesime ||' IS ''Chef-lieu de la base ADMIN Express de l�IGN de '|| millesime ||' :
  
Centre de  la  zone  d�habitat  dans  laquelle  se  trouve  la  mairie  de  la  commune.  
Dans certains cas, le chef-lieu n�est pas dans la commune.'';
-- Colonnes
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_chef_lieu_'|| emprise ||'_'|| millesime ||'.id IS ''Identifiant unique du chef-lieu de commune'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_chef_lieu_'|| emprise ||'_'|| millesime ||'.nom_chf IS ''D�nomination du chef-lieu de commune, parfois diff�rente de la d�nomination de la commune'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_chef_lieu_'|| emprise ||'_'|| millesime ||'.statut IS ''Statut du chef-lieu : Capitale d��tat / Pr�fecture de r�gion / Pr�fecture / Sous-pr�fecture / Commune simple'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_chef_lieu_'|| emprise ||'_'|| millesime ||'.insee_com IS ''Num�ro INSEE de la commune.
Il s�agit de la valeur de l�attribut INSEE_COM de la commune � se rapporte le chef-lieu'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_chef_lieu_'|| emprise ||'_'|| millesime ||'.geom IS ''Surfacique 2D'';
';
RAISE NOTICE '%', req;
EXECUTE(req);

----------------------------------------
---- C.3 n_adm_exp_commune_ddd_aaaa
----------------------------------------
---- A Faire table par table : ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_dims_geom CHECK (ST_NDims(geom)=2);
---- M�tadonn�es :
req := '
-- Table
	COMMENT ON TABLE '||nom_schema||'.n_adm_exp_commune_'|| emprise ||'_'|| millesime ||' IS ''Commune de la base ADMIN Express de l�IGN de '|| millesime ||' :

	Plus petite subdivision administrative du territoire, administr�e par un maire, des adjoints et un conseil municipal'';
-- Colonnes
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_commune_'|| emprise ||'_'|| millesime ||'.id IS ''Identifiant de la commune'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_commune_'|| emprise ||'_'|| millesime ||'.nom_com IS ''Nom de la commune'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_commune_'|| emprise ||'_'|| millesime ||'.nom_com_m IS ''Nom de la commune en majuscules'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_commune_'|| emprise ||'_'|| millesime ||'.insee_com IS ''Num�ro INSEE de la commune'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_commune_'|| emprise ||'_'|| millesime ||'.statut IS ''Statut administratif'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_commune_'|| emprise ||'_'|| millesime ||'.population IS ''Population de la commune'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_commune_'|| emprise ||'_'|| millesime ||'.insee_arr IS ''Num�ro INSEE de l�arrondissement'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_commune_'|| emprise ||'_'|| millesime ||'.nom_dep IS ''Nom du d�partement'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_commune_'|| emprise ||'_'|| millesime ||'.insee_dep IS ''Num�ro INSEE du d�partement'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_commune_'|| emprise ||'_'|| millesime ||'.nom_reg IS ''Nom de la r�gion'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_commune_'|| emprise ||'_'|| millesime ||'.insee_reg IS ''Num�ro INSEE de la r�gion'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_commune_'|| emprise ||'_'|| millesime ||'.code_epci IS ''Code de l�EPCI'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_commune_'|| emprise ||'_'|| millesime ||'.geom IS ''Surfacique 2D'';
';
RAISE NOTICE '%', req;
EXECUTE(req);

----------------------------------------
---- C.4 n_adm_exp_departement_ddd_aaaa
----------------------------------------
---- A Faire table par table : ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_dims_geom CHECK (ST_NDims(geom)=2);
---- M�tadonn�es :
req := '
-- Table
	COMMENT ON TABLE '||nom_schema||'.n_adm_exp_departement_'|| emprise ||'_'|| millesime ||'  IS ''D�partement au sens INSEE de la base ADMIN Express de l�IGN de '|| millesime ||''';
-- Colonnes
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_departement_'|| emprise ||'_'|| millesime ||'.id IS ''Identifiant du d�partement'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_departement_'|| emprise ||'_'|| millesime ||'.nom_dep IS ''Nom du d�partement'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_departement_'|| emprise ||'_'|| millesime ||'.insee_dep IS ''Num�ro INSEE du d�partement'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_departement_'|| emprise ||'_'|| millesime ||'.insee_reg IS ''Num�ro INSEE de la r�gion'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_departement_'|| emprise ||'_'|| millesime ||'.geom IS ''Surfacique 2D'';
';
RAISE NOTICE '%', req;
EXECUTE(req);

----------------------------------------
---- C.5 n_adm_exp_epci_ddd_aaaa
----------------------------------------
---- A Faire table par table : ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_dims_geom CHECK (ST_NDims(geom)=2);
---- M�tadonn�es :
req := '
-- Table
	COMMENT ON TABLE '||nom_schema||'.n_adm_exp_epci_'|| emprise ||'_'|| millesime ||'  IS ''EPCI de la base ADMIN Express de l�IGN de '|| millesime ||' :
	  Les �tablissements publics de coop�ration intercommunale sont  des  
	regroupements   de   communes   ayant   pour   objet   l��laboration
	de projets communs de d�veloppement au sein de p�rim�tres de  solidarit�.
	Ils sont soumis � des r�gles communes, homog�nes et comparables � celles
	de collectivit�s locales'';
-- Colonnes
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_epci_'|| emprise ||'_'|| millesime ||'.id IS ''Identifiant de l�EPCI'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_epci_'|| emprise ||'_'|| millesime ||'.code_epci IS ''Code de l�EPCI'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_epci_'|| emprise ||'_'|| millesime ||'.nom_epci IS ''Nom de l�EPCI'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_epci_'|| emprise ||'_'|| millesime ||'.type_epci IS ''Type de l�EPCI'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_epci_'|| emprise ||'_'|| millesime ||'.geom IS ''Surfacique 2D'';
';
RAISE NOTICE '%', req;
EXECUTE(req);

----------------------------------------
---- C.6 n_adm_exp_region_ddd_aaaa
----------------------------------------
---- A Faire table par table : ALTER TABLE ' || nom_schema || '.' || object || ' ADD CONSTRAINT enforce_dims_geom CHECK (ST_NDims(geom)=2);
---- M�tadonn�es :
req := '
-- Table
	COMMENT ON TABLE '||nom_schema||'.n_adm_exp_region_'|| emprise ||'_'|| millesime ||' IS ''R�gion au sens INSEE de la base ADMIN Express de l�IGN de '|| millesime ||''';
-- Colonnes
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_region_'|| emprise ||'_'|| millesime ||'.id IS ''Identifiant de la r�gion'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_region_'|| emprise ||'_'|| millesime ||'.nom_reg IS ''Nom de la r�gion'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_region_'|| emprise ||'_'|| millesime ||'.insee_reg IS ''Num�ro INSEE de la r�gion'';
	COMMENT ON COLUMN '||nom_schema||'.n_adm_exp_region_'|| emprise ||'_'|| millesime ||'.geom IS ''Surfacique 2D'';
';
RAISE NOTICE '%', req;
EXECUTE(req);
*/
RETURN current_time;
END;
$function$
;

COMMENT ON FUNCTION r_bdtopage.set_adm_bdtopage_1(varchar,bpchar,bpchar) IS '[ADMIN - BDTOPAGE] - Mise en place des taches d�administration pour un millesime de la BDTOPAGE� du Sandre et de l�IGN selon le millesime et l�emprise :

Taches r�alis�es :
A - Re-nommage des tables

B. Optimisation de base sur l�ensemble des fichiers
B.1 V�rification du nom du champs g�om�trie
B.2 Suppression des champs inutiles
B.3 Correction des erreurs sur la g�om�trie
B.4 Contraintes g�om�triques de la table
B.5 Ajout des index spatiaux et cluster
B.6 Ajout des index attributaires non existants
B.7 cl�s primaires sur le champs id

C. Travail � la Table
#AFAIRE
C.1 n_topage_bassin_hydro_ddd_aaaa
C.2 n_topage_bassin_versant_topo_ddd_aaaa
C.3 n_topage_cours_eau_ddd_aaaa
C.4 n_topage_limite_terre_mer_ddd_aaaa
C.5 n_topage_noeud_hydro_ddd_aaaa
C.6 n_topage_plan_eau_ddd_aaaa
C.7 n_topage_surface_elementaire_ddd_aaaa
C.8 n_topage_troncon_hydro_ddd_aaaa


Tables concern�es :
n_topage_bassin_hydro_ddd_aaaa
n_topage_bassin_versant_topo_ddd_aaaa
n_topage_cours_eau_ddd_aaaa
n_topage_limite_terre_mer_ddd_aaaa
n_topage_noeud_hydro_ddd_aaaa
n_topage_plan_eau_ddd_aaaa
n_topage_surface_elementaire_ddd_aaaa
n_topage_troncon_hydro_ddd_aaaa

derni�re MAJ : 18/03/2021';
