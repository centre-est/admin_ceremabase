CREATE OR REPLACE FUNCTION w_adl_delegue.set_comment_bdcarto_v4(livraison character DEFAULT 'shp'::bpchar, emprise character DEFAULT '000'::bpchar, millesime character DEFAULT NULL::bpchar, covadis boolean DEFAULT true)
 RETURNS text
 LANGUAGE plpgsql
AS $function$

/*
[ADMIN - BDCARTO] - Mise en place des commentaires

Options :
- format de livraison de l'IGN :
	- 'shp' = shapefile
	- 'sql' = dump postgis
- emprise sur 3 caractères selon la COVADIS ddd : 
	- 'fra' : France Entière
	- '000' : France Métropolitaine
	- 'rrr' : Numéro INSEE de la Région : 'r84' pour Avergne-Rhône-Alpes
	- 'ddd' : Numéro INSEE du département : '038' pour l'Isère
				non pris en compte si COVADIS = false
- millesime selon COVADIS : aaaa pour l'année du millesime ou null si pas de millesime
				non pris en compte si COVADIS = false
- COVADIS : nommage des tble selon la COVADIS : oui : true / non false

Tables concernées :
					'acces_equipement',
					'aerodrome',
					'arrondissement_departemental',
					'cimetiere',
					'commune',
					'communication_restreinte',
					'construction_elevee',
					'departement',
					'digue',
					'enceinte_militaire',
					'equipement_routier',
					'etablissement',
					'franchissement',
					'itineraire',
					'laisse'
					'lien_franchissement',
					'ligne_electrique',
					'limite_administrative',
					'massif_boise',
					'metro_aerien',
					'noeud_ferre',
					'noeud_routier',
					'piste_aerodrome',
					'point_remarquable_relief',
					'ponctuel_hydrographique',
					'region',
					'surface_hydrographique',
					'transport_cable',
					'troncon_hydrographique',
					'troncon_route',
					'troncon_voie_ferree',
					'zone_activite',
					'zone_habitat_mairie',
					'zone_hydrographique_texture',
					'zone_occupation_sol',
					'zone_reglementee_touristique'

amélioration à faire :


dernière MAJ : 05/12/2021
*/

DECLARE
nom_schema 					character varying;		-- Schéma du référentiel en text
nom_table 					character varying;		-- nom de la table en text
referentiel					character varying;		-- Nom du référentiel tel qu'il apparait dans le schema
trigramecovadis				character varying;		-- Tri-gramme du référentiel pour un nommage COVADIS
champs						character varying;		-- nom de la table en text;
commentaires 				character varying;		-- nom de la table en text
req 						text;
veriftable 					character varying;
liste_valeur				character varying[][4];	-- Toutes les tables
--liste_sous_valeur			character varying[3];	-- Toutes les tables
nb_valeur					integer;				-- Nombre de tables --> normalement XX
attribut 					character varying; 		-- Liste des attributs de la table
--typegeometrie 				text; 					-- "GeometryType" de la table

begin

referentiel := 'bdcarto';
trigramecovadis 	:= 'bdc';
nom_schema 	:= 'r_' || referentiel || '_' || millesime;	

---- A] Commentaires des tables
---- Liste des valeurs à passer :
---- ARRAY['nom de la table telle que livrées par IGN','nom du champs dans le livraison SQL','nom du champs dans la livraison shapefile', 'Commentaires à passer']
---- récupéré ici http://professionnels.ign.fr/doc/Structure_Nouvelle_BDTopo%20%285%29.xlsx
liste_valeur := ARRAY[
				ARRAY['acces_equipement','Cette classe traduit la relation « Un tronçon de route permet d’accéder à un équipement routier ».'],
				ARRAY['aerodrome','Aérodromes, aéroports et hydrobases.'],
				ARRAY['arrondissement_departemental','L’arrondissement est une subdivision du département. Depuis le redécoupage cantonal lié aux élections départementales de mars 2015, l’arrondissement n’est plus un regroupement de cantons mais de communes.'],
				ARRAY['cimetiere','Cimetière.'],
				ARRAY['commune','Plus petite subdivision du territoire, administrée par un maire, des adjoints et un conseil municipal.'],
				ARRAY['communication_restreinte','Cette table permet d’exprimer le fait que la communication entre un tronçon routier dit initial et un tronçon routier dit final soit impossible ou soumise à certaines restrictions de poids et/ou de hauteur.'],
				ARRAY['construction_elevee','Construction remarquable par sa hauteur.'],
				ARRAY['departement','Département au sens INSEE.'],
				ARRAY['digue','Digue.'],
				ARRAY['enceinte_militaire','Terrains militaires, champs de tirs et forts.'],
				ARRAY['equipement_routier','Equipement routier : aires de repos et de service, gare de péage, tunnel routier.'],
				ARRAY['etablissement','Etablissement public ou administratif. Ces établissements sont des bâtiments ou ensembles de bâtiments ayant pour fonction l’équipement technique, administratif, éducatif et sanitaire du territoire.'],
				ARRAY['franchissement','Lieu où plusieurs tronçons des réseaux routier, ferré ou hydrographique s’intersectent, sans qu’il n’y ait communication entre ces tronçons.'],
				ARRAY['itineraire','Un itinéraire routier est un ensemble de parcours continu empruntant des tronçons de route, chemin ou sentier et identifié par un toponyme et/ou un numéro (par exemple pour les routes européennes).'],
				ARRAY['laisse','En bord de mer, limite des plus hautes et des plus basses eaux.'],
				ARRAY['liaison_maritime','Liaison maritime ou ligne de bac reliant deux embarcadères.'],
				ARRAY['lien_franchissement','.'],
				ARRAY['ligne_electrique','Partie ou totalité d’une ligne de transport d’énergie électrique, homogène quant au voltage et au type de tracé.'],
				ARRAY['limite_administrative','Portion continue de contour de commune.'],
				ARRAY['massif_boise','Point repérant un massif boisé.'],
				ARRAY['metro_aerien','Métropolitain aérien.'],
				ARRAY['noeud_ferre','Un nœud ferré correspond à un embranchement, à un équipement (gare, etc.) ou à un changement de valeur d’attribut sur un tronçon de voie ferrée. C’est une extrémité d’un tronçon de voie ferrée.'],
				ARRAY['noeud_routier','Nœuds routiers et carrefours complexes. Un nœud routier est une extrémité de tronçon de route. Il traduit donc une modification des conditions de circulation, une intersection, un obstacle ou un changement de valeur d’attribut.'],
				ARRAY['piste_aerodrome','Axes des pistes d’aérodrome.'],
				ARRAY['point_remarquable_relief','Point remarquable du relief. Elément caractéristique en termes de relief (cap, col, etc.).'],
				ARRAY['ponctuel_hydrographique','Nœud hydrographique et point d’eau isolé. Cette classe regroupe les nœuds hydrographiques et les points d’eau isolés. Un nœud hydrographique correspond à une modification de l’écoulement de l’eau. C’est une extrémité d’un tronçon hydrographique.'],
				ARRAY['region','Région au sens INSEE.'],
				ARRAY['surface_hydrographique','Surface repérant et décrivant une zone couverte d’eau.'],
				ARRAY['transport_cable','Transport par câble : équipements de sport d’hiver (hormis les funiculaires) destinés au transport des skieurs, équipements de loisirs d’été en montagne, câbles transporteurs à usage privé ou industriel.'],
				ARRAY['troncon_hydrographique','Tronçon hydrographique et cours d’eau. Portion connexe de rivière, de ruisseau ou de canal, homogène pour les attributs qu’elle porte et les relations la mettant en jeu. Un tronçon correspond à l’axe du lit du cours d’eau.'],
				ARRAY['troncon_route','Portion connexe de route, de chemin, de piste cyclable ou de sentier, homogène pour les relations la mettant en jeu, et pour les attributs qu’elle porte.'],
				ARRAY['troncon_voie_ferree','Tronçon de voie ferrée et ligne de chemin de fer. Un tronçon de voie ferrée est une portion connexe de voie ferrée, homogène pour les attributs qu’elle porte.'],
				ARRAY['zone_activite','Point représentant une zone d’activité.'],
				ARRAY['zone_habitat_mairie','Point représentant une zone d’habitat centré sur la mairie.'],
				ARRAY['zone_hydrographique_texture','Zone plate au drainage complexe dans laquelle circule un ensemble de portions de cours d’eau formant un entrelacs de bras d’égale importance.'],
				ARRAY['zone_occupation_sol','Le territoire est partitionné en zones connexes et de nature homogène. Chaque zone est donc localisée et possède une nature. Tout point du territoire a été interprété lors de la saisie, et appartient à une zone et une seule.'],
				ARRAY['zone_reglementee_touristique','Zone réglementée par l’administration et d’intérêt touristique.']
];
nb_valeur := array_length(liste_valeur, 1);

FOR i_table IN 1..nb_valeur LOOP
---- Récupération des champs
---- Nom de la table
	select
		case
			when COVADIS is false then 
				lower(liste_valeur[i_table][1])
			else
				case
					when millesime is not null then
						'n_' || lower(liste_valeur[i_table][1]) || '_' || trigramecovadis || '_' || emprise || '_' || millesime
					else
						'n_' || lower(liste_valeur[i_table][1]) || '_' || trigramecovadis || '_' || emprise
				end
		end
		 into nom_table;
---- Nom du commentaire	
	SELECT liste_valeur[i_table][2] into commentaires;

---- Execution de la requete
	IF EXISTS (SELECT relname FROM pg_class where relname='' || nom_table ) then
		req := '
				COMMENT ON TABLE ' || nom_schema || '.' || nom_table || ' IS ''' || commentaires || ''';
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
	else
		req := '
				La table ' || nom_schema || '.' || nom_table || ' n´est pas présente.
				';
		RAISE NOTICE '%', req;
	END IF;

END LOOP;

---- B] Commentaires des attributs
---- Liste des valeurs à passer :
---- ARRAY['nom de la table telle que livrées par IGN','nom du champs dans le livraison SQL','nom du champs dans la livraison shapefile', 'Commentaires à passer']
---- récupéré ici http://professionnels.ign.fr/doc/Structure_Nouvelle_BDTopo%20%285%29.xlsx
liste_valeur := ARRAY[
				--- ARRAY['nom de la table telle que livrées par IGN','nom du champs dans le livraison SQL','nom du champs dans la livraison shapefile', 'Commentaires à passer']
				ARRAY['ARRONDISSEMENT_DEPARTEMENTAL','ID','ID','Identifiant de l’arrondissement. Cet identifiant est unique. À partir de l’édition 2019, de nouveaux identifiants sont utilisés..'],
				ARRAY['ARRONDISSEMENT_DEPARTEMENTAL','INSEE_ARR','INSEE_ARR','Numéro INSEE de l’arrondissement.'],
				ARRAY['ARRONDISSEMENT_DEPARTEMENTAL','INSEE_DEP','INSEE_DEP','Numéro INSEE du département auquel appartient l’arrondissement.'],
				ARRAY['ARRONDISSEMENT_DEPARTEMENTAL','INSEE_REG','INSEE_REG','Numéro INSEE de la région contenant l’arrondissement.'],
				
				ARRAY['COMMUNE','ID','ID','Identifiant de la commune. Cet identifiant est unique. À partir de l’édition 2019, de nouveaux identifiants sont utilisés.'],
				ARRAY['COMMUNE','NOM_COM','NOM_COM','Nom INSEE de la commune en minuscules.'],
				ARRAY['COMMUNE','NOM_COM_M','NOM_COM_M','Nom INSEE de la commune en majuscules.'],
				ARRAY['COMMUNE','INSEE_COM','INSEE_COM','Numéro INSEE de la commune. Une commune nouvelle résultant d’un regroupement de communes préexistantes se voit attribuer le code INSEE de l’ancienne commune désignée comme chef-lieu par l’arrêté préfectoral qui l’institue. En conséquence une commune change de code INSEE si un arrêté préfectoral modifie son chef-lieu.'],
				ARRAY['COMMUNE','STATUT','STATUT','Statut administratif de la commune.'],
				ARRAY['COMMUNE','X_COM','X_COM','Abscisse d’un point à l’intérieur de la commune (en mètre, dans le système légal de référence correspondant.'],
				ARRAY['COMMUNE','Y_COM','Y_COM','Ordonnée d’un point à l’intérieur de la commune (en mètre, dans le système légal de référence correspondant.'],
				ARRAY['COMMUNE','SUPERFICIE','SUPERFICIE','Superficie de la commune en hectares.'],
				ARRAY['COMMUNE','POPULATION','POPULATION','Population municipale de la commune en nombre d’habitants (au sens de l’INSEE). La population au 1 janvier de l’année « n » correspond à la population légale millésimée de l’année « n-3 ». Elles ont été calculées conformément aux concepts définis dans le décret n° 2003-485 signé le 5 juin 2003.'],
				ARRAY['COMMUNE','INSEE_ARR','INSEE_ARR','Numéro INSEE de l’arrondissement contenant la commune.'],
				ARRAY['COMMUNE','NOM_DEP','NOM_DEP','Nom INSEE du département auquel appartient la commune.'],
				ARRAY['COMMUNE','INSEE_DEP','INSEE_DEP','Numéro INSEE du département auquel appartient la commune.'],
				ARRAY['COMMUNE','NOM_REG','NOM_REG','Nom INSEE de la région à laquelle appartient la commune.'],
				ARRAY['COMMUNE','INSEE_REG','INSEE_REG','Numéro INSEE de la région contenant la commune.'],
				
				ARRAY['DEPARTEMENT','ID','ID','Identifiant du département. Cet identifiant est unique. À partir de l’édition 2019, de nouveaux identifiants sont utilisés.'],
				ARRAY['DEPARTEMENT','NOM_DEP','NOM_DEP','Nom INSEE du département.'],
				ARRAY['DEPARTEMENT','INSEE_DEP','INSEE_DEP','Numéro INSEE du département.'],
				ARRAY['DEPARTEMENT','X_DEP','X_DEP','Abscisse d’un point à l’intérieur du département (en mètre, dans le système légal de référence correspondant.'],
				ARRAY['DEPARTEMENT','Y_DEP','Y_DEP','Ordonnée d’un point à l’intérieur du département (en mètre, dans le système légal de référence correspondant.'],
				ARRAY['DEPARTEMENT','INSEE_REG','INSEE_REG','Numéro INSEE de la région contenant le département.'],
				
				ARRAY['LIMITE_ADMINISTRATIVE','ID','ID','Identifiant de la limite administrative. Cet identifiant est unique. À partir de l’édition 2019, de nouveaux identifiants sont utilisés.'],
				ARRAY['LIMITE_ADMINISTRATIVE','NATURE','NATURE','Nature de la limite administrative.'],
				ARRAY['LIMITE_ADMINISTRATIVE','PRECISION','PRECISION','Précision de la localisation.'],
				
				ARRAY['REGION','ID','ID','Identifiant de la région. Cet identifiant est unique. À partir de l’édition 2019, de nouveaux identifiants sont utilisés.'],
				ARRAY['REGION','NOM_REG','NOM_REG','Nom INSEE de la région.'],
				ARRAY['REGION','INSEE_REG','INSEE_REG','Numéro INSEE de la région.'],
				ARRAY['REGION','X_REG','X_REG','Abscisse d’un point à l’intérieur de la région (en mètre, dans le système légal de référence correspondant.'],
				ARRAY['REGION','Y_REG','Y_REG','Ordonnée d’un point à l’intérieur de la région (en mètre, dans le système légal de référence correspondant.'],
				
				ARRAY['AERODROME','ID','ID','Identifiant de l’aérodrome. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['AERODROME','NATURE','NATURE','Nature de l’aérodrome.'],
				ARRAY['AERODROME','DESSERTE','DESSERTE','Desserte de l’aérodrome.'],
				ARRAY['AERODROME','TOPONYME','TOPONYME','Toponyme éventuellement associé à l’aérodrome.'],
				
				ARRAY['CIMETIERE','ID','ID','Identifiant du cimetière. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['CIMETIERE','NATURE','NATURE','Nature du cimetière.'],
				ARRAY['CIMETIERE','TOPONYME','TOPONYME','Toponyme éventuellement associé au cimetière.'],
				
				ARRAY['CONSTRUCTION_ELEVEE','ID','ID','Identifiant de la construction élevée. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['CONSTRUCTION_ELEVEE','NATURE','NATURE','Nature de la construction élevée.'],
				
				ARRAY['DIGUE','ID','ID','Identifiant de la digue. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['DIGUE','NATURE','NATURE','Nature de la digue.'],
				
				ARRAY['ENCEINTE_MILITAIRE','ID','ID','Identifiant de l’enceinte militaire. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['ENCEINTE_MILITAIRE','NATURE','NATURE','Nature de l’enceinte militaire.'],
				ARRAY['ENCEINTE_MILITAIRE','TOPONYME','TOPONYME','Toponyme éventuellement associé à l’enceinte militaire.'],
				
				ARRAY['LIGNE_ELECTRIQUE','ID','ID','Identifiant de la ligne électrique. Cet identifiant est unique.'],
				ARRAY['LIGNE_ELECTRIQUE','TENSION','TENSION','Tension de construction de la ligne.'],
				
				ARRAY['METRO_AERIEN','ID','ID','Identifiant du métro aérien. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				
				ARRAY['PISTE_AERODROME','ID','ID','Identifiant de la piste d’aérodrome. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				
				ARRAY['TRANSPORT_CABLE','ID','ID','Identifiant du transport par câble. Cet identifiant est unique. À partir de l’édition 2019, de nouveaux identifiants sont utilisés.'],
				ARRAY['TRANSPORT_CABLE','NATURE','NATURE','Nature du transport par câble.'],
				ARRAY['TRANSPORT_CABLE','TOPONYME','TOPONYME','Toponyme éventuellement associé au transport par câble.'],
				
				ARRAY['ZONE_OCCUPATION_SOL','ID','ID','Identifiant de la zone d’occupation du sol. Cet identifiant est unique. À partir de l’édition 2019, de nouveaux identifiants sont utilisés.'],
				ARRAY['ZONE_OCCUPATION_SOL','NATURE','NATURE','Nature de la zone d’occupation du sol.'],
				
				ARRAY['LAISSE','ID','ID','Identifiant de la laisse. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['LAISSE','NATURE','NATURE','Nature de la laisse.'],
				
				ARRAY['PONCTUEL_HYDROGRAPHIQUE','ID','ID','Identifiant du ponctuel hydrographique. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['PONCTUEL_HYDROGRAPHIQUE','TYPE','TYPE','Type de ponctuel hydrographique.'],
				ARRAY['PONCTUEL_HYDROGRAPHIQUE','NATURE','NATURE','Nature du ponctuel hydrographique.'],
				ARRAY['PONCTUEL_HYDROGRAPHIQUE','TOPONYME','TOPONYME','Toponyme éventuellement associé au ponctuel hydrographique.'],
				ARRAY['PONCTUEL_HYDROGRAPHIQUE','COTE','COTE','Altitude du ponctuel hydrographique (en mètres).'],
				
				ARRAY['SURFACE_HYDROGRAPHIQUE','ID','ID','Identifiant de la surface hydrographique. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['SURFACE_HYDROGRAPHIQUE','NATURE','NATURE','Nature de la surface hydrographique.'],
				ARRAY['SURFACE_HYDROGRAPHIQUE','TOPONYME','TOPONYME','Toponyme éventuellement associé à la surface hydrographique.'],
				
				ARRAY['TRONCON_HYDROGRAPHIQUE','ID','ID','Identifiant du tronçon hydrographique. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['TRONCON_HYDROGRAPHIQUE','ETAT','ETAT','État du tronçon hydrographique.'],
				ARRAY['TRONCON_HYDROGRAPHIQUE','LARGEUR','LARGEUR','Largeur du tronçon hydrographique.'],
				ARRAY['TRONCON_HYDROGRAPHIQUE','NATURE','NATURE','Nature du tronçon hydrographique.'],
				ARRAY['TRONCON_HYDROGRAPHIQUE','NAVIGABLE','NAVIGABLE','Navigabilité du tronçon hydrographique.'],
				ARRAY['TRONCON_HYDROGRAPHIQUE','POS_SOL','POS_SOL','Position du tronçon hydrographique par rapport au sol.'],
				ARRAY['TRONCON_HYDROGRAPHIQUE','TOPONYME','TOPONYME','Toponyme éventuellement associé au tronçon hydrographique.'],
				ARRAY['TRONCON_HYDROGRAPHIQUE','SENS','SENS','Sens d’écoulement des eaux le long du tronçon.'],
				ARRAY['TRONCON_HYDROGRAPHIQUE','CLASSE','CLASSE','Hiérarchie décroissante entre les cours d’eau. Cette hiérarchie est basée sur les cours d’eau BD Carthage. On entend par « embouchure logique » une interruption du réseau formé par les cours d’eau naturels : mer, puits, etc.'],
				
				ARRAY['ZONE_HYDROGRAPHIQUE_TEXTURE','ID','ID','Identifiant de la zone d’hydrographie de texture. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['ZONE_HYDROGRAPHIQUE_TEXTURE','TOPONYME','TOPONYME','Toponyme éventuellement associé à la zone d’hydrographie de texture.'],
				
				ARRAY['NOEUD_FERRE','ID','ID','Identifiant du nœud ferré. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['NOEUD_FERRE','NATURE','NATURE','Nature du nœud ferré.'],
				ARRAY['NOEUD_FERRE','TOPONYME','TOPONYME','Toponyme éventuellement associé au nœud ferré.'],
				
				ARRAY['TRONCON_VOIE_FERREE','ID','ID','Identifiant du tronçon de voie ferrée. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['TRONCON_VOIE_FERREE','NATURE','NATURE','Nature du tronçon de voie ferrée.'],
				ARRAY['TRONCON_VOIE_FERREE','ENERGIE','ENERGIE','Énergie de propulsion.'],
				ARRAY['TRONCON_VOIE_FERREE','NB_VOIES','NB_VOIES','Nombre de voies principales du tronçon.'],
				ARRAY['TRONCON_VOIE_FERREE','LARGEUR','LARGEUR','Largeur du tronçon de voie ferrée.'],
				ARRAY['TRONCON_VOIE_FERREE','POS_SOL','POS_SOL','Position du tronçon de voie ferrée par rapport au sol.'],
				ARRAY['TRONCON_VOIE_FERREE','CLASSEMENT','CLASSEMENT','Classement du tronçon de voie ferrée.'],
				ARRAY['TRONCON_VOIE_FERREE','TOPONYME','TOPONYME','Toponyme éventuellement associé au tronçon de voie ferrée. Seuls les noms des ponts, viaducs ou tunnels sont portés par les tronçons.'],
				ARRAY['TRONCON_VOIE_FERREE','TOURISTIQ','TOURISTIQ','Éventuel caractère touristique de la ligne de chemin de fer à laquelle ce tronçon de voie ferrée peut appartenir.'],
				ARRAY['TRONCON_VOIE_FERREE','TOPO_LIGNE','TOPO_LIGNE','Éventuel toponyme de la ligne de chemin de fer à laquelle le tronçon de voie ferrée peut appartenir.'],
				
				ARRAY['ACCES_EQUIPEMENT','ID','ID','Identifiant de l’accès équipement. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['ACCES_EQUIPEMENT','ID_EQUIPMT','ID_EQUIPMT','Identifiant de l’équipement routier.'],
				ARRAY['ACCES_EQUIPEMENT','ID_TRONCON','ID_TRONCON','Identifiant du tronçon routier permettant d’accéder à l’équipement.'],
				ARRAY['ACCES_EQUIPEMENT','COTE','COTE','Côté du tronçon par lequel on accède à l’équipement. La gauche et la droite s’entendent par rapport à l’orientation du tronçon orienté dans le sens des arcs qui le composent.'],
				
				ARRAY['COMMUNICATION_RESTREINTE','ID','ID','Identifiant de la communication restreinte. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['COMMUNICATION_RESTREINTE','ID_NOEUD','ID_NOEUD','Identifiant du nœud routier concerné par la relation de communication restreinte.'],
				ARRAY['COMMUNICATION_RESTREINTE','ID_TRO_INI','ID_TRO_INI','Identifiant du tronçon routier initial concerné par la relation de communication restreinte.'],
				ARRAY['COMMUNICATION_RESTREINTE','ID_TRO_FIN','ID_TRO_FIN','Identifiant du tronçon routier final concerné par la relation de communication restreinte.'],
				ARRAY['COMMUNICATION_RESTREINTE','INTERDIT','INTERDIT','Type de restriction.'],
				ARRAY['COMMUNICATION_RESTREINTE','REST_POIDS','REST_POIDS','Poids maximum autorisé en tonne.'],
				ARRAY['COMMUNICATION_RESTREINTE','REST_HAUT','REST_HAUT','Hauteur maximale autorisée en mètre.'],
				
				ARRAY['EQUIPEMENT_ROUTIER','ID','ID','Identifiant de l’équipement routier. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['EQUIPEMENT_ROUTIER','NATURE','NATURE','Nature de l’équipement routier.'],
				ARRAY['EQUIPEMENT_ROUTIER','TOPONYME','TOPONYME','Toponyme éventuellement associé à l’équipement routier.'],
				
				ARRAY['FRANCHISSEMENT','ID','ID','Identifiant du franchissement.'],
				ARRAY['FRANCHISSEMENT','TOPONYME','TOPONYME','Toponyme éventuellement associé au franchissement.'],
				ARRAY['FRANCHISSEMENT','COTE','COTE','Altitude en mètre du point haut du franchissement.'],
				ARRAY['FRANCHISSEMENT','TYPE_TRON','TYPE_TRON','Type de tronçon qui franchit.'],
				ARRAY['FRANCHISSEMENT','ID_TRONCON','ID_TRONCON','Identifiant du tronçon qui traverse (tronçon de route, de voie ferrée, ou hydrographique).'],
				ARRAY['FRANCHISSEMENT','MODE','MODE','Mode de franchissement. Renseigne sur la nature de l’ouvrage d’art réalisant le franchissement physique pour le tronçon.'],
				ARRAY['FRANCHISSEMENT','NIVEAU','NIVEAU','Niveau d’empilement du tronçon dans le franchissement. Par convention, 0 est le niveau le plus bas et n+1 le niveau immédiatement supérieur à n.'],
				
				ARRAY['ITINERAIRE','ID','ID','Identifiant de l’itinéraire. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['ITINERAIRE','NUMERO','NUMERO','Numéro de l’itinéraire routier.'],
				ARRAY['ITINERAIRE','NATURE','NATURE','Nature de l’itinéraire routier.'],
				ARRAY['ITINERAIRE','TOPONYME','TOPONYME','Toponyme éventuellement associé à l’itinéraire.'],
				
				ARRAY['LIAISON_MARITIME','ID','ID','Identifiant de la liaison maritime. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['LIAISON_MARITIME','OUVERTURE','OUVERTURE','Période d’ouverture de la liaison.'],
				ARRAY['LIAISON_MARITIME','VOCATION','VOCATION','Vocation de la liaison.'],
				ARRAY['LIAISON_MARITIME','DUREE','DUREE','Durée de la traversée en minutes.'],
				ARRAY['LIAISON_MARITIME','TOPONYME','TOPONYME','Toponyme éventuellement associé à la liaison.'],
				
				ARRAY['NOEUD_ROUTIER','ID','ID','Identifiant du nœud routier. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['NOEUD_ROUTIER','NATURE','NATURE','Nature du nœud routier.'],
				ARRAY['NOEUD_ROUTIER','TOPONYME','TOPONYME','Toponyme éventuel du nœud routier.'],
				ARRAY['NOEUD_ROUTIER','COTE','COTE','Altitude en mètre du nœud routier.'],
				ARRAY['NOEUD_ROUTIER','NUM_CARREF','NUM_CARREF','Numéro du carrefour complexe auquel peut appartenir le nœud routier. Cette numérotation concerne uniquement les échangeurs sur autoroute, voies express aux normes, et certaines routes nationales qui garantissent la continuité des liaisons vertes importantes, plusieurs carrefours complexes peuvent porter le même numéro.'],
				ARRAY['NOEUD_ROUTIER','NAT_CARREF','NAT_CARREF','Nature du carrefour complexe auquel peut appartenir le nœud routier.'],
				ARRAY['NOEUD_ROUTIER','TOP_CARREF','TOP_CARREF','Toponyme éventuel du carrefour complexe auquel peut appartenir le nœud routier.'],
				
				ARRAY['TRONCON_ROUTE','ID','ID','Identifiant du tronçon de route. Cet identifiant est unique. Il est stable d’une édition à l’autre.'],
				ARRAY['TRONCON_ROUTE','VOCATION','VOCATION','Vocation de la liaison. Ce champ matérialise une hiérarchisation du réseau routier basée, non pas sur un critère administratif, mais sur l’importance des tronçons de route pour le trafic routier.'],
				ARRAY['TRONCON_ROUTE','NB_CHAUSSE','NB_CHAUSSE','Nombre de chaussées du tronçon.'],
				ARRAY['TRONCON_ROUTE','NB_VOIES','NB_VOIES','Nombre total de voies du tronçon de route.'],
				ARRAY['TRONCON_ROUTE','ETAT','ETAT','Etat physique du tronçon.'],
				ARRAY['TRONCON_ROUTE','ACCES','ACCES','Accès au tronçon.'],
				ARRAY['TRONCON_ROUTE','POS_SOL','POS_SOL','Position du tronçon par rapport au sol.'],
				ARRAY['TRONCON_ROUTE','RES_VERT','RES_VERT','Appartenance du tronçon au réseau vert. Il s’agit du réseau vert de transit, le réseau vert de rabattement n’est pas géré.'],
				ARRAY['TRONCON_ROUTE','SENS','SENS','Sens de circulation sur le tronçon. Le sens de circulation est géré de façon obligatoire sur les tronçons composant les voies à chaussées séparées éloignées (valeur Sens unique ou Sens inverse) et sur les tronçons constituant un échangeur détaillé ; dans les autres cas, le sens est géré si l’information est connue.'],
				ARRAY['TRONCON_ROUTE','NB_VOIES_M','NB_VOIES_M','Nombre de voies de la chaussée montante. Ne concerne que les tronçons à deux chaussées. La chaussée montante est celle où la circulation s’effectue dans le sens des arcs composant le tronçon.'],
				ARRAY['TRONCON_ROUTE','NB_VOIES_D','NB_VOIES_D','Nombre de voies de la chaussée descendante. Ne concerne que les tronçons à deux chaussées. La chaussée descendante est celle où la circulation s’effectue dans le sens inverse des arcs composant le tronçon.'],
				ARRAY['TRONCON_ROUTE','TOPONYME','TOPONYME','Toponyme éventuellement associé au tronçon de route.'],
				ARRAY['TRONCON_ROUTE','USAGE','USAGE','Utilisation du tronçon de route. Ce champ permet de distinguer les tronçons en fonction de leur utilisation potentielle pour la description de la logique de communication et/ou d’une représentation cartographique.'],
				ARRAY['TRONCON_ROUTE','DATE','DATE','Date prévue de mise en service du tronçon.'],
				ARRAY['TRONCON_ROUTE','NUM_ROUTE','NUM_ROUTE','Numéro de la route contenant le tronçon.'],
				ARRAY['TRONCON_ROUTE','CLASS_ADM','CLASS_ADM','Classement administratif de la route contenant le tronçon.'],
				ARRAY['TRONCON_ROUTE','GEST_ROUTE','GEST_ROUTE','Département ou autre gestionnaire de la route contenant le tronçon.'],
				
				ARRAY['ETABLISSEMENT','ID','ID','Identifiant de l’établissement. Cet identifiant est unique. À partir de l’édition 2019, de nouveaux identifiants sont utilisés.'],
				ARRAY['ETABLISSEMENT','NATURE','NATURE','Nature de l’établissement.'],
				ARRAY['ETABLISSEMENT','TOPONYME','TOPONYME','Toponyme éventuellement associé à l’établissement.'],
				
				ARRAY['MASSIF_BOISE','ID','ID','Identifiant du massif boisé. Cet identifiant est unique. À partir de l’édition 2019, de nouveaux identifiants sont utilisés.'],
				ARRAY['MASSIF_BOISE','TOPONYME','TOPONYME','Toponyme éventuellement associé au massif boisé.'],
				
				ARRAY['POINT_REMARQUABLE_RELIEF','ID','ID','Identifiant du point remarquable. Cet identifiant est unique. À partir de l’édition 2019, de nouveaux identifiants sont utilisés.'],
				ARRAY['POINT_REMARQUABLE_RELIEF','NATURE','NATURE','Nature du point remarquable.'],
				ARRAY['POINT_REMARQUABLE_RELIEF','COTE','COTE','Altitude en mètre du point remarquable.'],
				ARRAY['POINT_REMARQUABLE_RELIEF','TOPONYME','TOPONYME','Toponyme éventuellement associé au point remarquable.'],
				
				ARRAY['ZONE_ACTIVITE','ID','ID','Identifiant de la zone d’activité. Cet identifiant est unique. À partir de l’édition 2019, de nouveaux identifiants sont utilisés.'],
				ARRAY['ZONE_ACTIVITE','TOPONYME','TOPONYME','Toponyme éventuellement associé à la zone d’activité.'],
				
				ARRAY['ZONE_HABITAT_MAIRIE','ID','ID','Identifiant de la zone d’habitat. Cet identifiant est unique. À partir de l’édition 2019, de nouveaux identifiants sont utilisés.'],
				ARRAY['ZONE_HABITAT_MAIRIE','IMPORTANCE','IMPORTANCE','Importance de la zone d’habitat.'],
				ARRAY['ZONE_HABITAT_MAIRIE','INSEE','INSEE','Numéro INSEE de la commune dont la zone d’habitat est chef-lieu.'],
				ARRAY['ZONE_HABITAT_MAIRIE','TOPONYME','TOPONYME','Toponyme éventuellement associé à la zone d’habitat.'],
				
				ARRAY['ZONE_REGLEMENTEE_TOURISTIQUE','ID','ID','Identifiant de la zone réglementée par l’administration et d’intérêt touristique. Cet identifiant est unique. À partir de l’édition 2019, de nouveaux identifiants sont utilisés.'],
				ARRAY['ZONE_REGLEMENTEE_TOURISTIQUE','NATURE','NATURE','Nature de la zone d’intérêt touristique.'],
				ARRAY['ZONE_REGLEMENTEE_TOURISTIQUE','TOPONYME','TOPONYME','Toponyme éventuellement associé à la zone réglementée.']
				
				];
nb_valeur := array_length(liste_valeur, 1);

FOR i_table IN 1..nb_valeur LOOP
---- Récupération des champs
---- Nom de la table
	select
		case
			when COVADIS is false then 
				lower(liste_valeur[i_table][1])
			else
				case
					when millesime is not null then
						'n_' || lower(liste_valeur[i_table][1]) || '_' || trigramecovadis || '_' || emprise || '_' || millesime
					else
						'n_' || lower(liste_valeur[i_table][1]) || '_' || trigramecovadis || '_' || emprise
				end
		end
		 into nom_table;
---- Nom du champs à commenter		
	SELECT 
		CASE
			WHEN livraison = 'sql' THEN lower(liste_valeur[i_table][2])
			ELSE lower(liste_valeur[i_table][3])
		END
		into champs;
---- Nom du commentaire	
	SELECT liste_valeur[i_table][4] into commentaires;
	/*-- debug
	RAISE NOTICE '%', i_table;
	RAISE NOTICE '%', nom_table;
	RAISE NOTICE '%', champs;
	RAISE NOTICE '%', commentaires;
	-- debug */

---- Execution de la requete
	IF EXISTS (SELECT relname FROM pg_class where relname='' || nom_table ) then
		req := '
				COMMENT ON COLUMN ' || nom_schema || '.' || nom_table || '.' || champs || ' IS ''' || commentaires || ''';
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
	else
		req := '
				La table ' || nom_schema || '.' || nom_table || ' n´est pas présente pour le champs ' || champs || '.
				';
		RAISE NOTICE '%', req;
	END IF;

END LOOP; 

RETURN current_time;
END; 

$function$
;

COMMENT ON FUNCTION w_adl_delegue.set_comment_bdcarto_v4(bpchar,bpchar,bpchar,bool) IS '[ADMIN - BDCARTO] - Mise en place des commentaires

Options :
- format de livraison de l''IGN :
	- ''shp'' = shapefile
	- ''sql'' = dump postgis
- emprise sur 3 caractères selon la COVADIS ddd : 
	- ''fra'' : France Entière
	- ''000'' : France Métropolitaine
	- ''rrr'' : Numéro INSEE de la Région : ''r84'' pour Avergne-Rhône-Alpes
	- ''ddd'' : Numéro INSEE du département : ''038'' pour l''Isère
				non pris en compte si COVADIS = false
- millesime selon COVADIS : aaaa pour l''année du millesime ou null si pas de millesime
				non pris en compte si COVADIS = false
- COVADIS : nommage des tble selon la COVADIS : oui : true / non false

Tables concernées :
					''acces_equipement'',
					''aerodrome'',
					''arrondissement_departemental'',
					''cimetiere'',
					''commune'',
					''communication_restreinte'',
					''construction_elevee'',
					''departement'',
					''digue'',
					''enceinte_militaire'',
					''equipement_routier'',
					''etablissement'',
					''franchissement'',
					''itineraire'',
					''laisse''
					''lien_franchissement'',
					''ligne_electrique'',
					''limite_administrative'',
					''massif_boise'',
					''metro_aerien'',
					''noeud_ferre'',
					''noeud_routier'',
					''piste_aerodrome'',
					''point_remarquable_relief'',
					''ponctuel_hydrographique'',
					''region'',
					''surface_hydrographique'',
					''transport_cable'',
					''troncon_hydrographique'',
					''troncon_route'',
					''troncon_voie_ferree'',
					''zone_activite'',
					''zone_habitat_mairie'',
					''zone_hydrographique_texture'',
					''zone_occupation_sol'',
					''zone_reglementee_touristique''

amélioration à faire :


dernière MAJ : 05/12/2021';

