---- B] Mise en place de la fonction :
---- B.1 ] w_adl_delegue.set_adm_bdc_etat_major_v1
CREATE OR REPLACE FUNCTION w_adl_delegue.set_adm_bdcarto_etat_major_1(emprise character varying, millesime character varying, projection integer DEFAULT 2154)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/*
[ADMIN - BDCARTO ETAT-MAJOR] - Administration d´un millésime de la BDCARTO© ETAT-MAJOR V.1 issue du ShapeFile; une fois son import réalisé

Taches réalisées :
---- C. Optimisation de toutes les tables
---- C.1 Suppression du champs gid créée et de la séquence correspondante
---- C.2 Vérification du nom du champs géométrique
---- C.3 Correction des erreurs sur la géométrie
---- C.4 Ajout des contraintes
---- C.4.1 Ajout des contraintes sur le champs géométrie
---- C.4.2 CHECK (geometrytype(geom)
---- C.5 Ajout de la clef primaire
---- C.5.1 Suppression de l´ancienne si existante
---- C.5.1 Création de la clé primaire selon IGN
---- C.6 Ajout des index spatiaux
---- C.7 Ajout des index attributaires non existants

---- Les commentaires sont renvoyés à une autre fonction

Tables concernées :
	a1_limite_administrative_bdc_em
	b1_troncon_de_route_bdc_em
	b2_troncon_de_cours_d_eau_bdc
	b2_troncon_de_voie_ferree_bdc
	c11_ocs_ancien_bdc_em
	c12_ocs_ancien_sans_bati_bdc_em
	c21_batiment_bdc_em
	c22_batiment_hors_zone_urbaine_bdc_em
	c3_zone_urbaine_bdc_em

amélioration à faire :
---- A Create Schema : vérification que le schéma n'existe pas et le créer

dernière MAJ : 16/08/2021
*/

declare
nom_schema 			character varying;		-- Schéma du référentiel en text
nom_table 			character varying;		-- nom de la table en text
req 				text;
veriftable 			character varying;
tb_toutestables		character varying[];	-- Toutes les tables
nb_toutestables 	integer;				-- Nombre de tables --> normalement XX
attribut 			character varying; 		-- Liste des attributs de la table
typegeometrie 		text; 					-- "GeometryType" de la table

BEGIN
nom_schema:='r_bdc_etat_major_n4';

---- Référencement des tables à traiter
--DEBUG tb_toutestables := array['erp'];
tb_toutestables := array[
	'a1_limite_administrative_bdc_em',
	'b1_troncon_de_route_bdc_em',
	'b2_troncon_de_cours_d_eau_bdc_em',
	'b2_troncon_de_voie_ferree_bdc_em',
	'c11_ocs_ancien_bdc_em',
	'c12_ocs_ancien_sans_bati_bdc_em',
	'c21_batiment_bdc_em',
	'c22_batiment_hors_zone_urbaine_bdc_em',
	'c3_zone_urbaine_bdc_em'
		];
nb_toutestables := array_length(tb_toutestables, 1);

---- C. Optimisation de toutes les tables
FOR i_table IN 1..nb_toutestables LOOP
	nom_table:='n_' || tb_toutestables[i_table] || '_' || emprise || '_' || millesime;
	SELECT tablename FROM pg_tables WHERE schemaname = nom_schema AND tablename = nom_table INTO veriftable;
	IF LEFT(veriftable,length (nom_table)) = nom_table
	then
---- C.1 Suppression du champs gid créée et de la séquence correspondante
	req := '
				ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP COLUMN IF EXISTS gid;
				ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP COLUMN IF EXISTS ogc_fid;
				ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP COLUMN IF EXISTS id_0;
		';
		RAISE NOTICE '%', req;
		EXECUTE(req);
---- C.2 Vérification du nom du champs géométrique
		SELECT f_geometry_column FROM public.geometry_columns WHERE f_table_schema = nom_schema AND f_table_name = nom_table AND (
		select count(f_geometry_column) FROM public.geometry_columns WHERE f_table_schema = nom_schema AND f_table_name = nom_table
		) = 1
		INTO attribut;
		IF attribut = 'geom'
		THEN
			req := '
				La table ' || nom_schema || '.' || nom_table || ' à un nom de géométrie conforme
			';
		RAISE NOTICE '%', req;
		ELSE
			req :='
				ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' RENAME ' || attribut  || ' TO geom;
			 ';
			RAISE NOTICE '%', req;
			EXECUTE(req);
		END IF;
---- C.3 Correction des erreurs sur la géométrie
---- selon cette méthode : http://www.geoinformations.developpement-durable.gouv.fr/verification-et-corrections-des-geometries-a3522.html
	req := '
				UPDATE ' || nom_schema || '.' || nom_table || ' SET geom=
					CASE 
						WHEN GeometryType(geom) = ''POLYGON'' 		OR GeometryType(geom) = ''MULTIPOLYGON'' THEN
								ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
						WHEN GeometryType(geom) = ''LINESTRING'' 	OR GeometryType(geom) = ''MULTILINESTRING'' THEN
								ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
						WHEN GeometryType(geom) = ''POINT'' 		OR GeometryType(geom) = ''MULTIPOINT'' THEN
								ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
						ELSE ST_MakeValid(geom)
					END
				WHERE NOT ST_Isvalid(geom);
				';
	RAISE NOTICE '%', req;
	EXECUTE(req);
---- C.4 Ajout des contraintes
---- C.4.1 Ajout des contraintes sur le champs géométrie
	req := '
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_dims_geom;
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_dims_geom CHECK (ST_NDims(geom)=2);
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_srid_geom;
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)=' || projection || ');
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_no_self_intersection_geom;
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_no_self_intersection_geom CHECK (ST_IsSimple(geom));
	';
	RAISE NOTICE '%', req;
	EXECUTE(req);
---- C.4.2 CHECK (geometrytype(geom)
---- C.4.2.1 Création de la table pour lister les géométries disponible
	req := '
		CREATE TABLE public.a_supprimer AS (
			SELECT GeometryType(geom) AS geomtype
			FROM ' || nom_schema || '.' || nom_table || ' group by geomtype
		);
	';
	RAISE NOTICE '%', req;
	EXECUTE(req);

---- C.4.2.2 CHECK (geometrytype(geom)
	SELECT type FROM public.geometry_columns WHERE f_table_schema = nom_schema AND f_table_name = nom_table INTO attribut;
		IF attribut = 'GEOMETRY' then SELECT geomtype FROM public.a_supprimer LIMIT 1 INTO typegeometrie;
					IF 	typegeometrie = 'POLYGON' 			THEN req := '
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''POLYGON''::text);-- OR geom IS NULL);
							';
					ELSEIF typegeometrie = 'MULTIPOLYGON' 	THEN req := '
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTIPOLYGON''::text);-- OR geom IS NULL);
							';
					ELSEIF typegeometrie = 'LINESTRING' 		THEN req := '
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''LINESTRING''::text);-- OR geom IS NULL);
							';
					ELSEIF typegeometrie = 'MULTILINESTRING' 	THEN req := '
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTILINESTRING''::text);-- OR geom IS NULL);
							';
					ELSEIF typegeometrie = 'POINT' 		THEN req := '
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''POINT''::text);-- OR geom IS NULL);
							';
					ELSEIF typegeometrie = 'MULTIPOINT' 		THEN req := '
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
								ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTIPOINT''::text);-- OR geom IS NULL);
							';
					else req := 'La valeur attribut est <<' || attribut || '>> et la valeur typegeometrie est <<' || typegeometrie || '>> ';
				END IF;	
			ELSIF attribut = 'POLYGON' 			THEN req := '
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''POLYGON''::text);-- OR geom IS NULL);
					';
			ELSIF attribut = 'MULTIPOLYGON' 	THEN req := '
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTIPOLYGON''::text);-- OR geom IS NULL);
					';
			ELSIF attribut = 'LINESTRING' 		THEN req := '
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''LINESTRING''::text);-- OR geom IS NULL);
					';
			ELSIF attribut = 'MULTILINESTRING' 	THEN req := '
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTILINESTRING''::text);-- OR geom IS NULL);
					';
			ELSIF attribut = 'POINT' 		THEN req := '
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''POINT''::text);-- OR geom IS NULL);
					';
			ELSIF attribut = 'MULTIPOINT' 		THEN req := '
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
						ALTER TABLE ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (geometrytype(geom) = ''MULTIPOINT''::text);-- OR geom IS NULL);
					';
		else req := 'La valeur attribut est <<' || attribut || '>> ';
		END IF;
		RAISE NOTICE '%', req;
		EXECUTE(req);
---- C.4.2.3
		req := '
			DROP TABLE public.a_supprimer;
		';
		RAISE NOTICE '%', req;
		EXECUTE(req);
---- C.5 Ajout de la clef primaire
---- C.5.1 Suppression de l'ancienne si existante
		select t1.conname from pg_constraint as t1, pg_class as t2
		where t2.relname = nom_table and t1.contype = 'p' and t1.conrelid = t2.oid
		into attribut;
		IF attribut is NULL
		THEN
			req := '
				La table ' || nom_schema || '.' || nom_table || ' n´a pas de clé primaire.
			';
		RAISE NOTICE '%', req;
		ELSE
			req :='
				ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS ' || attribut  || ';
			 ';
			RAISE NOTICE '%', req;
			EXECUTE(req);

		END IF;
---- C.5.1 Création de la clé primaire selon IGN
			select left(nom_table,12) into attribut;
			IF attribut != 'n_toponymie_'
			then
				req := '
					ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT ' || nom_table || '_pkey PRIMARY KEY (cleabs);
				';		
			else
				req := '
					ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT ' || nom_table || '_pkey PRIMARY KEY (cleabs);
					select current_time;
				';					
			end if;
			RAISE NOTICE '%', req;
			EXECUTE(req);
---- C.6 Ajout des index spatiaux
			req := '
				DROP INDEX IF EXISTS ' || nom_schema || '.' || nom_table || '_geom_gist;
				CREATE INDEX ' || nom_table || '_geom_gist ON ' || nom_schema || '.' || nom_table || ' USING gist (geom) TABLESPACE index;
        		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' CLUSTER ON ' || nom_table || '_geom_gist;
			';
			RAISE NOTICE '%', req;
			EXECUTE(req);
---- C.7 Ajout des index attributaires non existants
			FOR attribut IN
				SELECT COLUMN_NAME
					FROM INFORMATION_SCHEMA.COLUMNS
					WHERE TABLE_NAME = nom_table AND TABLE_SCHEMA = nom_schema
					AND COLUMN_NAME != 'geom' AND COLUMN_NAME != 'the_geom'
			LOOP
					req := '
						DROP INDEX IF EXISTS ' || nom_schema || '.' || nom_table || '_' || attribut || '_idx;
						CREATE INDEX ' || nom_table || '_' || attribut || '_idx ON ' || nom_schema || '.' || nom_table || ' USING btree (' || attribut || ') TABLESPACE index;
					';
					RAISE NOTICE '%', req;
					EXECUTE(req);
			END LOOP;				
---- C.99 Fin de la boucle
	ELSE
	req :='La table ' || nom_schema || '.' || nom_table || ' n est pas présente';
	RAISE NOTICE '%', req;

	END IF;
END LOOP; 	

RETURN current_time;
END; 
$function$
;

COMMENT ON FUNCTION w_adl_delegue.set_adm_bdcarto_etat_major_1(varchar,varchar,int4) IS '[ADMIN - BDCARTO ETAT-MAJOR] - Administration d´un millésime de la BDCARTO© ETAT-MAJOR V.1 issue du ShapeFile; une fois son import réalisé

Taches réalisées :
---- C. Optimisation de toutes les tables
---- C.1 Suppression du champs gid créée et de la séquence correspondante
---- C.2 Vérification du nom du champs géométrique
---- C.3 Correction des erreurs sur la géométrie
---- C.4 Ajout des contraintes
---- C.4.1 Ajout des contraintes sur le champs géométrie
---- C.4.2 CHECK (geometrytype(geom)
---- C.5 Ajout de la clef primaire
---- C.5.1 Suppression de l´ancienne si existante
---- C.5.1 Création de la clé primaire selon IGN
---- C.6 Ajout des index spatiaux
---- C.7 Ajout des index attributaires non existants

---- Les commentaires sont renvoyés à une autre fonction

Tables concernées :
	a1_limite_administrative_bdc_em
	b1_troncon_de_route_bdc_em
	b2_troncon_de_cours_d_eau_bdc
	b2_troncon_de_voie_ferree_bdc
	c11_ocs_ancien_bdc_em
	c12_ocs_ancien_sans_bati_bdc_em
	c21_batiment_bdc_em
	c22_batiment_hors_zone_urbaine_bdc_em
	c3_zone_urbaine_bdc_em

amélioration à faire :
---- A Create Schema : vérification que le schéma n''existe pas et le créer

dernière MAJ : 16/08/2021';

/*
---- D] Mise en place des commentaires : A FAIRE !
---- Récupération de la fonction ici : https://gitlab.cerema.fr/centre-est/admin_ceremabase/-/blob/master/referentiels.ign/set_comment_bdtopo_3.sql
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
req 						text;
BEGIN
liste_valeur := ARRAY  ['001','002','003','004','005','006','007','008','009','010',
						'011','012','013','014','015','016','017','018','019','02a','02b',
						'021','022','023','024','025','026','027','028','029','030',
						'031','032','033','034','035','036','037','038','039','040',
						'041','042','043','044','045','046','047','048','049','050',
						'051','052','053','054','055','056','057','058','059','060',
						'061','062','063','064','065','066','067','068','069','070',
						'071','072','073','074','075','076','077','078','079','080',
						'081','082','083','084','085','086','087','088','089','090',
						'091','092','093','094','095'
						];
FOR i_table IN 1..array_length(liste_valeur, 1) LOOP
		req := '
				SELECT w_adl_delegue.set_comment_bdtopo_3(''r_bdtopo_2021'',''shp'','''|| liste_valeur[i_table] ||''',''2021'',true);
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
END LOOP;
END $$;
/*
set_comment_bdtopo_3|
--------------------|
17:26:00.997236+02  |
 */
*/

---- D] Finalisation
---- D.0] Ouverture des droits à tous


---- D.1] On réorganise toutes les couches selon l'index géométrique
DO $$
DECLARE
	nom_schema text;
	cat_attr text;
	req text;
BEGIN
nom_schema := 'r_bdc_etat_major_n4';
FOR cat_attr IN
	SELECT tablename
	FROM pg_tables
	WHERE schemaname LIKE nom_schema
	ORDER BY tablename
LOOP
	req := 'CLUSTER '||nom_schema||'.'|| cat_attr ||';';
   	RAISE NOTICE '%', req;
	EXECUTE(req);
	COMMIT;
END LOOP;
END $$;
--> OK

---- D.2] On met à jour la vue des résultats
REFRESH MATERIALIZED VIEW r_bdc_etat_major_n4.synthese_geometrie_schema;

---- E] SAUVEGARDE finale
---- E.1] Sauvegarde
ALTER SYSTEM SET max_locks_per_transaction = 1024;
time /usr/lib/postgresql/13/bin/pg_dump --host=hp-geomatique --port=5433 --dbname="ceremabase" --schema="r_bdc_etat_major_n4" --username "postgres" --role "postgres" --format=plain --encoding UTF8 --inserts --column-inserts --verbose --quote-all-identifiers --file="/mnt/data/sauve_r_bdc_etat_major_n4_2021.sql"
/*
real	0m40,278s
user	0m2,893s
sys	0m2,326s
 */

---- E.2] Compression optimisée pour le texte
time 7z a -t7z '/mnt/travaux/add_bdtopo_2021/99_sauve_postgis/sauve_r_bdc_etat_major_n4_2021.7z' '/mnt/data/sauve_r_bdc_etat_major_n4_2021.sql'  -mmt2 -m0=PPMd -v1024m
/*
real	0m56,764s
user	0m56,301s
sys	0m0,248s
 */

---- F] Pour restaurer
pg_restore -p 5433 -c -U postgres -d ceremabase -v '/mnt/travaux/add_bdtopo_2021/99_sauve_postgis/SAUVE_parcellaire_express_partitionne_2021.sql'
