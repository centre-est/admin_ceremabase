CREATE OR REPLACE FUNCTION w_adl_delegue.set_adm_adresse_premium(emprise character varying, millesime character varying, projection integer DEFAULT 2154)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
/*
[ADMIN - ADRESSE PREMIUM] - Administration d´un millésime de ADRESSE PREMIUM® de l'IGN une fois son import réalisé
-----------------------------------
Rappel pour l'import :
-----------------------------------
@ECHO OFF
REM Variables d'environnement :
SET PGENCODING=UTF8
SET PGHOST=srv41-sigi
SET PGPORT=5444
SET PGDATABASE=ceremabase
SET PGUSER=postgres
SET PGPASSWORD=

REM Variable de travail
REM SET repertoire=D:\cadastre_data.gouv.fr_2017\
SET schematof=r_adresse_premium

REM Variable d'exécutables
SET cmd_psql="C:\Program Files\PostgreSQL\12\bin\psql.exe" -h srv41-map -p 5444 -U postgres -d ceremabase
SET cmd_shp2pgsql="C:\Program Files\PostgreSQL\12\bin\shp2pgsql.exe"
SET cmd_org2ogr="C:\OSGeo4W64\bin\ogr2ogr.exe" -lco SCHEMA=%schematof% -f "PostgreSQL" PG:"host=%PGHOST% port=%PGPORT% user=%PGUSER% dbname=%PGDATABASE%"  

REM SET liste= 971 972 973 974 975 976 
SET liste=002 003 004 005 006 007 008 009 010 011 012 013 014 015 016 017 018 019 02A 02B 021 022 023 024 025 026 027 028 029 030 031 032 033 034 035 036 037 038 039 040 041 042 043 044 045 046 047 048 049 050 051 052 053 054 055 056 057 058 059 060 061 062 063 064 065 066 067 068 069 070 071 072 073 074 075 076 077 078 079 080 081 081 082 083 084 085 086 087 088 089 090 091 092 093 094 095
REM SET liste=001
REM SET liste=01 03 07 15 21 23 25 26 38 39 42 43 58 63 69 70 71 73 74 87 89 90

(
ECHO Création du Schéma
REM %cmd_psql%"CREATE SCHEMA %schematof% AUTHORIZATION postgres;"
REM %cmd_psql% --command="GRANT USAGE ON SCHEMA %schematof% TO public;
REM %cmd_psql% --command="COMMENT ON SCHEMA %schematof% IS 'ADRESSE PERMIUM de l´IGN';"

for %%d in (%liste%) do (
ECHO Departement %%d
REM SET repertoire=D:\_TEMP\ADD_BDADRESSE_2018\ADRESSE_PREMIUM_3-0__HYB_LAMB93_D%%d_2020-06-19\ADRESSE_PREMIUM\1_DONNEES_LIVRAISON_2020-06-00378\AP_3-0_HYB_LAMB93_D%%d-ED202\
ECHO Import 
%cmd_shp2pgsql% -s [EPSG]:2154 -c -g geom -t 2D D:\_TEMP\ADD_BDADRESSE_2018\ADRESSE_PREMIUM_3-0__HYB_LAMB93_D%%d_2020-06-19\ADRESSE_PREMIUM\1_DONNEES_LIVRAISON_2020-06-00378\AP_3-0_HYB_LAMB93_D%%d-ED202\A_ADR-PARC\Lien_Adresse-Parcelle_D%%d-ED202.shp %schematof%.n_lien_adresse_parcelle_adp_%%d_2020 | %cmd_psql%
%cmd_shp2pgsql% -s [EPSG]:2154 -c -g geom -t 2D D:\_TEMP\ADD_BDADRESSE_2018\ADRESSE_PREMIUM_3-0__HYB_LAMB93_D%%d_2020-06-19\ADRESSE_PREMIUM\1_DONNEES_LIVRAISON_2020-06-00378\AP_3-0_HYB_LAMB93_D%%d-ED202\B_ADR-BATI\Lien_Adresse-Bati_D%%d-ED202.shp %schematof%.n_lien_adresse_bati_adp_%%d_2020 | %cmd_psql%
%cmd_shp2pgsql% -s [EPSG]:2154 -c -g geom -t 2D D:\_TEMP\ADD_BDADRESSE_2018\ADRESSE_PREMIUM_3-0__HYB_LAMB93_D%%d_2020-06-19\ADRESSE_PREMIUM\1_DONNEES_LIVRAISON_2020-06-00378\AP_3-0_HYB_LAMB93_D%%d-ED202\C_BATI-PARC\Lien_Bati-Parcelle_D%%d-ED202.shp %schematof%.n_lien_bati_parcelle_adp_%%d_2020 | %cmd_psql%
%cmd_shp2pgsql% -s [EPSG]:2154 -c -g geom -n -t 2D D:\_TEMP\ADD_BDADRESSE_2018\ADRESSE_PREMIUM_3-0__HYB_LAMB93_D%%d_2020-06-19\ADRESSE_PREMIUM\1_DONNEES_LIVRAISON_2020-06-00378\AP_3-0_HYB_LAMB93_D%%d-ED202\D_ADR-IRIS\Lien_Adresse-Iris_D%%d-ED202.dbf %schematof%.n_lien_adresse_iris_adp_%%d_2020 | %cmd_psql%
%cmd_shp2pgsql% -s [EPSG]:2154 -c -g geom -n -t 2D D:\_TEMP\ADD_BDADRESSE_2018\ADRESSE_PREMIUM_3-0__HYB_LAMB93_D%%d_2020-06-19\ADRESSE_PREMIUM\1_DONNEES_LIVRAISON_2020-06-00378\AP_3-0_HYB_LAMB93_D%%d-ED202\E_ADR-HEXA\Lien_Adresse-Hexa_D%%d-ED202.dbf %schematof%.n_lien_adresse_hexa_adp_%%d_2020 | %cmd_psql%

))>D:\_TEMP\ADD_BDADRESSE_2018\00_synthese_adresse_premium_2020_%date:~6,4%%date:~3,2%%date:~0,2%.log
-----------------------------------

Taches réalisées :
---- A. Déplacement et Renomage des tables
---- B. Optimisation sur toutes les tables
---- B.1 Suppression du champs gid créée et de la séquence correspondante
---- B.2 Ajout des index attributaires non existants
---- C. Travail sur les tables géographiques
---- C.1 Référencement des tables à  traiter
---- C.2 Correction des erreurs sur la géométrie
---- C.3 Ajout des index spatiaux
---- C.4 Ajout des contraintes
---- C.4.1 Ajout des contraintes sur le champs géométrie
---- D. Ajout des clés primaires

---- C.1 Suppression du champs gid créée et de la séquence correspondante
---- C.2 Vérification du nom du champs géomÃ©trique
---- C.3 Correction des erreurs sur la gÃ©omÃ©trie
---- C.4 Ajout des contraintes
---- C.4.1 Ajout des contraintes sur le champs gÃ©omÃ©trie
---- C.4.2 CHECK (geometrytype(geom)
---- C.5 Ajout de la clef primaire
---- C.5.1 Suppression de lÂ´ancienne si existante
---- C.5.1 CrÃ©ation de la clÃ© primaire selon IGN
---- C.6 Ajout des index spatiaux
---- C.7 Ajout des index attributaires non existants

---- Les commentaires sont renvoyés à une autre fonction

Tables concernées :
	'lien_adresse_parcelle',
	'lien_adresse_bati',
	'lien_bati_parcelle',
	'lien_adresse_iris',
	'lien_adresse_hexa'

---- EXECUTION SUR TOUTE LA FRANCE
DO $$
DECLARE
liste_valeur 				CHARACTER VARYING[];
req 						text;
BEGIN
liste_valeur := ARRAY  ['001','002','003','004','005','006','007','008','009','010',
						'011','012','013','014','015','016','017','018','019','02a','02b',
						'021','022','023','024','025','026','027','028','029','030',
						'031','032','033','034','035','036','037','038','039','040',
						'041','042','043','044','045','046','047','048','049','050',
						'051','052','053','054','055','056','057','058','059','060',
						'061','062','063','064','065','066','067','068','069','070',
						'071','072','073','074','075','076','077','078','079','080',
						'081','082','083','084','085','086','087','088','089','090',
						'091','092','093','094','095','971','972','973','974','976'
						];
FOR i_table IN 1..array_length(liste_valeur, 1) LOOP
		req := '
				 SELECT w_adl_delegue.set_adm_adresse_premium('''|| liste_valeur[i_table] ||''',''2020'');
				';
		RAISE NOTICE '%', req;
		EXECUTE(req);
		COMMIT;
END LOOP;
END $$;

dernière MAJ : 19/10/2020'
*/

declare
nom_schema 					character varying;		-- Schéma du référentiel en text
nom_table 					character varying;		-- nom de la table en text
req 						text;					-- requète à  faire passer
veriftable 					character varying;		-- nom de la table à vérifier
tb_toutestables				character varying[];	-- Toutes les tables
nb_toutestables 			integer;				-- Nombre de tables --> normalement XX
attribut 					character varying; 		-- Liste des attributs de la table
typegeometrie 				text; 					-- "GeometryType" de la table

BEGIN
nom_schema:='r_adresse_premium';

---- Référencement des tables à  traiter
-- DEBUG : tb_toutestables := array['condomium','construction_lineaire'];
tb_toutestables := array[
	'lien_adresse_parcelle',
	'lien_adresse_bati',
	'lien_bati_parcelle',
	'lien_adresse_iris',
	'lien_adresse_hexa'
		];
nb_toutestables := array_length(tb_toutestables, 1);

---- A. Déplacement et Renomage des tables
req := '
		CREATE SCHEMA ' || nom_schema || ';
';
RAISE NOTICE '%', req;
--EXECUTE(req);

FOR i_table IN 1..nb_toutestables LOOP
	nom_table:=tb_toutestables[i_table];
	SELECT tablename FROM pg_tables WHERE schemaname = 'public' AND tablename = nom_table INTO veriftable;
	IF LEFT(veriftable,length (nom_table)) = nom_table
	then
	req := '
		ALTER TABLE public.' || nom_table || ' RENAME TO n_' || nom_table || '_adp_' || emprise || '_' || millesime || ';
		ALTER TABLE public.n_' || nom_table || '_bdt_' || emprise || '_' || millesime || ' SET SCHEMA ' || nom_schema || ';	
	';
	RAISE NOTICE '%', req;
	EXECUTE(req);
	
	ELSE
	req :='La table ' || nom_schema || '.' || nom_table || ' n est pas présente';
	RAISE NOTICE '%', req;

	END IF;
END LOOP; 

---- B. Optimisation sur toutes les tables
FOR i_table IN 1..nb_toutestables LOOP
	nom_table:='n_' || tb_toutestables[i_table] || '_adp_' || emprise || '_' || millesime;
	SELECT tablename FROM pg_tables WHERE schemaname = nom_schema AND tablename = nom_table INTO veriftable;
	IF LEFT(veriftable,length (nom_table)) = nom_table
	then
---- B.1 Suppression du champs gid créée et de la séquence correspondante
	req := '
				ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP COLUMN IF EXISTS gid;
				ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP COLUMN IF EXISTS ogc_fid;
				ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP COLUMN IF EXISTS id_0;
		';
		RAISE NOTICE '%', req;
		EXECUTE(req);

---- B.2 Ajout des index attributaires non existants
			FOR attribut IN
				SELECT COLUMN_NAME
					FROM INFORMATION_SCHEMA.COLUMNS
					WHERE TABLE_NAME = nom_table
					AND COLUMN_NAME != 'geom' AND COLUMN_NAME != 'the_geom' AND COLUMN_NAME != 'geom3d'
			LOOP
					req := '
						DROP INDEX IF EXISTS ' || nom_schema || '.' || nom_table || '_' || attribut || '_idx;
						CREATE INDEX ' || nom_table || '_' || attribut || '_idx ON ' || nom_schema || '.' || nom_table || ' USING btree (' || attribut || ') TABLESPACE index;
					';
					RAISE NOTICE '%', req;
					EXECUTE(req);
			END LOOP;				
---- B.99 Fin de la boucle
	ELSE
	req :='La table ' || nom_schema || '.' || nom_table || ' n?est pas prÃ©sente';
	RAISE NOTICE '%', req;
	END IF;
END LOOP;	

---- C. Travail sur les tables géographiques
---- C.1 Référencement des tables à  traiter
-- DEBUG : tb_toutestables := array['condomium','construction_lineaire'];
tb_toutestables := array[
	'lien_adresse_parcelle',
	'lien_adresse_bati',
	'lien_bati_parcelle'
		];
nb_toutestables := array_length(tb_toutestables, 1);

FOR i_table IN 1..nb_toutestables LOOP
	nom_table:='n_' || tb_toutestables[i_table] || '_adp_' || emprise || '_' || millesime;
	SELECT tablename FROM pg_tables WHERE schemaname = nom_schema AND tablename = nom_table INTO veriftable;
	IF LEFT(veriftable,length (nom_table)) = nom_table
	then
---- C.2 Correction des erreurs sur la gÃ©omÃ©trie
---- selon cette mÃ©thode : http://www.geoinformations.developpement-durable.gouv.fr/verification-et-corrections-des-geometries-a3522.html
	req := '
				UPDATE ' || nom_schema || '.' || nom_table || ' SET geom=
					CASE 
						WHEN GeometryType(geom) = ''POLYGON'' 		OR GeometryType(geom) = ''MULTIPOLYGON'' THEN
								ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),3)),0))
						WHEN GeometryType(geom) = ''LINESTRING'' 	OR GeometryType(geom) = ''MULTILINESTRING'' THEN
								ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),2)),0))
						WHEN GeometryType(geom) = ''POINT'' 		OR GeometryType(geom) = ''MULTIPOINT'' THEN
								ST_Multi(ST_Simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(geom)),1)),0))
						ELSE ST_MakeValid(geom)
					END
				WHERE NOT ST_Isvalid(geom);
				';
	RAISE NOTICE '%', req;
	EXECUTE(req);

---- C.3 Ajout des index spatiaux
			req := '
				DROP INDEX IF EXISTS ' || nom_schema || '.' || nom_table || '_geom_gist;
				CREATE INDEX ' || nom_table || '_geom_gist ON ' || nom_schema || '.' || nom_table || ' USING gist (geom) TABLESPACE index;
        		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' CLUSTER ON ' || nom_table || '_geom_gist;
			';
			RAISE NOTICE '%', req;
			EXECUTE(req);

---- C.4 Ajout des contraintes
---- C.4.1 Ajout des contraintes sur le champs gÃ©omÃ©trie
	req := '
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_validite_geom;
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_validite_geom CHECK (ST_IsValid(geom));
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_dims_geom;
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_dims_geom CHECK (ST_NDims(geom)=2);
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_srid_geom;
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_srid_geom CHECK (ST_Srid(geom)=' || projection || ');
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_geotype_geom;
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_geotype_geom CHECK (GeometryType(geom)=''MULTILINESTRING''::text OR geom IS NULL);
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_no_self_intersection_geom;
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_no_self_intersection_geom CHECK (ST_IsSimple(geom));
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' DROP CONSTRAINT IF EXISTS enforce_no_zero_length_geom;
		ALTER TABLE IF EXISTS ' || nom_schema || '.' || nom_table || ' ADD CONSTRAINT enforce_no_zero_length_geom CHECK (ST_Length(geom) > 0);
';
	RAISE NOTICE '%', req;
	EXECUTE(req);
		
---- C.99 Fin de la boucle
	ELSE
	req :='La table ' || nom_schema || '.' || nom_table || ' n?est pas prÃ©sente';
	RAISE NOTICE '%', req;

	END IF;
END LOOP;


---- D Ajout de la clef primaire
	req := '
		ALTER TABLE IF EXISTS ' || nom_schema || '.n_lien_adresse_bati_adp_' || emprise || '_' || millesime || ' DROP CONSTRAINT IF EXISTS n_lien_adresse_bati_adp_' || emprise || '_' || millesime ||'_pkey;
		ALTER TABLE IF EXISTS ' || nom_schema || '.n_lien_adresse_bati_adp_' || emprise || '_' || millesime || ' ADD CONSTRAINT n_lien_adresse_bati_adp_' || emprise || '_' || millesime ||'_pkey PRIMARY KEY (id);

		ALTER TABLE IF EXISTS ' || nom_schema || '.n_lien_adresse_hexa_adp_' || emprise || '_' || millesime || ' DROP CONSTRAINT IF EXISTS n_lien_adresse_hexa_adp_' || emprise || '_' || millesime ||'_pkey;
		ALTER TABLE IF EXISTS ' || nom_schema || '.n_lien_adresse_hexa_adp_' || emprise || '_' || millesime || ' ADD CONSTRAINT n_lien_adresse_hexa_adp_' || emprise || '_' || millesime ||'_pkey PRIMARY KEY (id_adr);

		ALTER TABLE IF EXISTS ' || nom_schema || '.n_lien_adresse_iris_adp_' || emprise || '_' || millesime || ' DROP CONSTRAINT IF EXISTS n_lien_adresse_iris_adp_' || emprise || '_' || millesime ||'_pkey;
		ALTER TABLE IF EXISTS ' || nom_schema || '.n_lien_adresse_iris_adp_' || emprise || '_' || millesime || ' ADD CONSTRAINT n_lien_adresse_iris_adp_' || emprise || '_' || millesime ||'_pkey PRIMARY KEY (id_adr);

		ALTER TABLE IF EXISTS ' || nom_schema || '.n_lien_adresse_parcelle_adp_' || emprise || '_' || millesime || ' DROP CONSTRAINT IF EXISTS n_lien_adresse_parcelle_adp_' || emprise || '_' || millesime ||'_pkey;
		ALTER TABLE IF EXISTS ' || nom_schema || '.n_lien_adresse_parcelle_adp_' || emprise || '_' || millesime || ' ADD CONSTRAINT n_lien_adresse_parcelle_adp_' || emprise || '_' || millesime ||'_pkey PRIMARY KEY (id);

		ALTER TABLE IF EXISTS ' || nom_schema || '.n_lien_bati_parcelle_adp_' || emprise || '_' || millesime || ' DROP CONSTRAINT IF EXISTS n_lien_bati_parcelle_adp_' || emprise || '_' || millesime ||'_pkey;
		ALTER TABLE IF EXISTS ' || nom_schema || '.n_lien_bati_parcelle_adp_' || emprise || '_' || millesime || ' ADD CONSTRAINT n_lien_bati_parcelle_adp_' || emprise || '_' || millesime ||'_pkey PRIMARY KEY (id);
	';
RAISE NOTICE '%', req;
EXECUTE(req);

RETURN current_time;
END; 
$function$
;
