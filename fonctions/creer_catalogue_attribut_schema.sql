CREATE OR REPLACE FUNCTION w_fonctions.creer_catalogue_attribut_schema(nom_schema text)
RETURNS void
AS $$
/*
[CHARGE ETUDE] - Cette fonction permet de créer pour chaque table d´un schéma donné son catalogue d´attributs

Paramètres : 
- nom_schema = ´nomduschema´ pour le schéma d´origine qui doit exister

exemple :
SELECT w_fonctions.creer_catalogue_attribut_schema('m_mapbruit');

Taches réalisées :
- Suppression des tables de catalogue si existantes,
- Pour toute table présente dans le schéma
--> Création d'une nouvelle table avec le suffixe  : _cat_attr
	avec 3 champs : 
	
nom_table|nom_champs|description|
---------|----------|-----------|
action   |coutreel  |xour       |
action   |id        |identité   |

amélioration à faire :

dernière MAJ : 10/06/2020.
*/
DECLARE
	nom text;
	cat_attr text;
	req text;
		
BEGIN

FOR cat_attr IN
	SELECT tablename
	FROM pg_tables
	WHERE schemaname LIKE nom_schema AND tablename LIKE '%_cat_attr'

LOOP
	req := 'DROP TABLE IF EXISTS '||nom_schema||'.'|| cat_attr ||' CASCADE;';
	EXECUTE(req);
    RAISE NOTICE 'Requete executée : %', req;
END LOOP;

FOR nom IN SELECT tablename
	FROM pg_tables 	WHERE schemaname LIKE nom_schema 
	
LOOP

	req := '
		CREATE TABLE '||nom_schema||'.'||nom||'_cat_attr AS
			WITH mytables AS
			(
				SELECT tablename,schemaname 
				FROM pg_tables 
				WHERE schemaname LIKE '''||nom_schema||'''
			)
		SELECT
			tablename as nom_table, a.attname as nom_champs, d.description
		FROM
			mytables
		JOIN
			pg_class c
		ON
			tablename = c.relname
		JOIN
			pg_attribute a
		ON
			a.attrelid=c.oid
		JOIN
			pg_description d
		ON
			d.objoid=c.oid AND d.objsubid = a.attnum
		WHERE tablename LIKE '''||nom||'''
		ORDER BY schemaname, tablename, a.attname;
	';
	EXECUTE (req);
    RAISE NOTICE 'Requete executée : %', req;
END LOOP;

END;
$$
LANGUAGE plpgsql VOLATILE;
COMMENT ON FUNCTION w_fonctions.creer_catalogue_attribut_schema(nom_schema text) is '
[CHARGE ETUDE] - Cette fonction permet de créer pour chaque table d´un schéma donné son catalogue d´attributs

Paramètres : 
- nom_schema = ´nomduschema´ pour le schéma d´origine qui doit exister

exemple :
SELECT w_fonctions.creer_catalogue_attribut_schema(´m_mapbruit´);

Taches réalisées :
- Suppression des tables de catalogue si existantes,
- Pour toute table présente dans le schéma
--> Création d´une nouvelle table avec le suffixe  : _cat_attr
	avec 3 champs : 
	
nom_table|nom_champs|description|
---------|----------|-----------|
action   |coutreel  |xour       |
action   |id        |identité   |

amélioration à faire :

dernière MAJ : 10/06/2020.
';
