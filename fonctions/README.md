# admin_ceremabase

## Répertoire fonctions
Fonctions partagées
- exemple de conversion d'un champs texte vers un champs numérique
- cree_standard_covadis_te.sql : Crée les tables conformes au standard COVADIS V1 (http://www.geoinformations.developpement-durable.gouv.fr/geostandard-transports-exceptionnels-v1-0-a3493.html) dans un schéma passé en paramètre
- creer_catalogue_attribut_schema.sql : Cette fonction permet de créer pour chaque table d´un schéma donné son catalogue d´attributs
- creer_catalogue_attribut_table.sql : permet de créer pour chaque table d´un schéma donné son catalogue d´attributs
- creer_grille.sql : crée une grille qui contient l'ensemble de la couche en paramètre, selon un pas en paramètre aussi
- synthese_geometrie_schema : Extrait l'ensemble des informations géométriques de tous les champs géométriques présents dans un schéma passé en paramètre