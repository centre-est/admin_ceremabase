CREATE OR REPLACE FUNCTION w_fonctions.creer_catalogue_attribut_table(couche text, nom_schema text)
RETURNS void
AS $$
/*
[CHARGE ETUDE] - Cette fonction permet de cr�er pour une table d�un sch�ma donn� son catalogue d�attributs

Param�tres : 
- couche = �nondelatable� dans le sch�ma d�origine qui doit exister
- nom_schema = �nomduschema� pour le sch�ma d�origine qui doit exister

exemple :
SELECT w_fonctions.creer_catalogue_attribut_table('action','m_mapbruit');


Taches r�alis�es :
- Suppression de la tables de catalogue si existante,
- Cr�ation d'une nouvelle table avec le suffixe  : _cat_attr
	avec 3 champs : 
	
nom_table|nom_champs|description|
---------|----------|-----------|
action   |coutreel  |xour       |
action   |id        |identit�   |

am�lioration � faire :

derni�re MAJ : 10/06/2020.
*/
DECLARE
req text;

BEGIN
req :='
DROP TABLE IF EXISTS '||nom_schema||'.'||couche||'_cat_attr;

CREATE TABLE '||nom_schema||'.'||couche||'_cat_attr AS
	WITH mytables AS
		(
		SELECT tablename,schemaname 
		FROM pg_tables 
		WHERE schemaname LIKE '''||nom_schema||'''
		)
SELECT tablename as nom_table, a.attname, d.description
FROM mytables
JOIN pg_class c ON tablename = c.relname
JOIN pg_attribute a ON a.attrelid=c.oid
JOIN pg_description d ON d.objoid=c.oid AND d.objsubid = a.attnum
WHERE tablename LIKE '''||couche||'''
ORDER BY schemaname, tablename, a.attname;

COMMENT ON TABLE '||nom_schema||'.'||couche||'_cat_attr IS ''Catalogue attributaire de la couche '||couche||'.'';
';

EXECUTE (req);

END;
$$ LANGUAGE PLPGSQL VOLATILE;
COMMENT ON FUNCTION  w_fonctions.creer_catalogue_attribut_table(couche text, nom_schema text) IS '
[CHARGE ETUDE] - Cette fonction permet de cr�er pour une table d�un sch�ma donn� son catalogue d�attributs

Param�tres : 
- couche = �nondelatable� dans le sch�ma d�origine qui doit exister
- nom_schema = �nomduschema� pour le sch�ma d�origine qui doit exister

exemple :
SELECT w_fonctions.creer_catalogue_attribut_table(�action�,�m_mapbruit�);


Taches r�alis�es :
- Suppression de la tables de catalogue si existante,
- Cr�ation d�une nouvelle table avec le suffixe  : _cat_attr
	avec 3 champs : 
	
nom_table|nom_champs|description|
---------|----------|-----------|
action   |coutreel  |xour       |
action   |id        |identit�   |

am�lioration � faire :

derni�re MAJ : 10/06/2020.
';
